#delimit ;
clear all;
set linesize 180;
***************************************
* bea_income.DO -- read in per capita personal
* income from BEA spreadsheet.
***************************************;
clear all;

local states AL AK AZ AR CA 
             CO CT DE DC FL 
			 GA HI ID IL IN
			 IA KS KY LA ME 
			 MD MA MI MN MS 
			 MO MT NE NV NH 
			 NJ NM NY NC ND 
			 OH OK OR PA RI 
			 SC SD TN TX UT 
			 VT VA WA WV WI 
			 WY;

			 
***********************************
* Population from Census (via Fred, https://fred.stlouisfed.org/release/tables?rid=118&eid=259194);
import excel using ../raw/population.xls, sheet("Annual") clear firstrow;
gen  year = year(DATE);
drop if year <1950;
drop DATE;

foreach x of local states {; 
  rename `x'POP POP`x';
};

greshape long POP, i(year) j(fipstate) string;
replace POP = POP*1000;
rename POP pop_census;
label var pop_census "Population from Census (via Fred)";

rename fipstate state;

keep if year >= 1959 & year <= 2020;
state2fips;

sort fips state year;
order fips state year;

tempfile pop;
save `pop', replace;

* Per capita personal income from BEA spreadsheet, taken from https://apps.bea.gov/itable/iTable.cfm?ReqID=70&step=1, SAINC1;
import excel using ../raw/per_capita_personal_income.xls, cellrange(A7:CP66) clear;

ren A geofips;
ren B geoname;

local counter = 1929;
foreach v of varlist C-CP {;
  ren `v' ypp`counter';
  local counter = `counter' + 1;
};

drop ypp1929-ypp1958;

destring geofips, replace;
gen int fips = geofips/1000;
keep if inrange(fips,1,56);

replace geoname = "Alaska" if geoname == "Alaska *";
replace geoname = "Hawaii" if geoname == "Hawaii *";

reshape long ypp, i(fips geoname) j(year);
sort year fips geoname;

save ../mid/temp, replace;

* Price index (from FRED at St. Louis Fed, downloaded 8/5/2021), https://fred.stlouisfed.org/series/PCEPI;
import excel using ../raw/PCEPI.xls, cellrange(A11:B761) clear firstrow;

gen  year = year(observation_date);

collapse (mean) PCEPI, by(year);
drop if year == 2021;

sort year;

merge 1:m year using ../mid/temp;
tab _merge;
drop _merge;
erase ../mid/temp.dta;

gen ypph = ypp/(PCEPI/100);
format ypph %20.4f;

sort fips year;
order fips year geoname ypp ypph;
rename geoname state_name;
drop geofips;
merge 1:1 fips year using `pop';
keep fips year ypph;
la var ypph    "Real Per Capital Personal Income ($2012 BEA)";
save ../mid/bea_income.dta, replace;
