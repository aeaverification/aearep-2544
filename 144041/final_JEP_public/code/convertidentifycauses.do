clear all
pause on
set rmsg on 
set scheme s1color

*in this do file we construct several intermediate data files in which deaths
*are categorized by their cause. 

*We have four strategies for obtaining consistent definitions of causes of death over time.
*one, refer to the code system itself. e.g., atherosclerosis is the section 
*titled atherosclerosis in both classifications. 
*two, use the broad icd chapters. these are sufficient for cancer and cicrculatory
*deaths in general, although some chapters have large jumps over 1998-1999 and 
*these are not appropriate for fine classifications like deaths of despair
*three, refer to icd10 code system and icd10-icd9 crosswalk. however, icd10 is more 
*granular and hence we may overcount if we use the crosswalk. and, there are situations
*where an icd10 code is equivalent to the presence of multiple icd9 codes, which 
*at this point we ignore due to the high burden of implementation.
*four, refer to icd9 code system and icd9-icd10 crosswalk. this leverages the 
*granularity of the icd10 codes better, although we still ignore situations
*where the presence of multiple icd10 codes in one death are together equivalent
*to an icd9 code. 

*at this point we are able to construct deaths of despair consistently using the 
*first method. we can supplement these with broad cancer and circulatory classifications
*using the relevant chapters (fourth method).
*for finer-grained conditions like diabetes, hypertension, ischemic/coronary 
*heart disease, and atherosclerosis, we use a mix of methods one, two, and three. 

* Note that, in the end, we use only method one.  We have experimented with methods two, three, and four, however, which have not significantly affected our results.
*****************
*store death codes in globals 
global causes9 circulatory cirrhosis suicide diabetes cerebrovasc lowerresp cancer flupneum
global causes10 circulatory cirrhosis suicide diabetes cerebrovasc lowerresp cancer flupneum

*globals of definitions for method one, to be put into icd9 and icd10 utility functions
*icd9 
global cirrhosis9 571*
global suicide9 E95*
global poison9 E850.0/E858.9 E860.0/E860.9 E980.0/E982.9

*ICD 9 main causes
global circulatory9 390.0/398.9 402.0/402.9 404.0/404.9 410.0/429.9
global lowerresp9 490.0/494.9 496.0/496.9
global flupneum9 480.0/487.9
global cerebrovasc9 430.0/434.9 436.0/438.9
global diabetes9 250*
global cancer9 140.0/208.9

*icd10 
global cirrhosis10 k70* k73/k74.9
global suicide10 x60/x84.9 y87.0
global poison10 x40/x45.9 y10/y15.9

global circulatory10 I00/I09.9 I11* I13* I20/I51.9
global diabetes10 E10/E14.9
global cerebrovasc10 I60/I69.9
global cancer10 C00/C97.9
global lowerresp10 J40/J47.9
global flupneum10 J10/J18.9

*****************
*import icd9 to icd10 crosswalk, taken from icd general equivalence mappings, downloaded from https://www.cdc.gov/nchs/icd/icd10cm.htm for 2018
infix str icd9 1-6 str icd10 7-14 byte approxflag 15 byte nomapflag 16 ///
byte combinationflag 17 byte scenarioflag 18 byte choicelistflag 19 ///
using ../raw/icd_gem_crosswalks/2018_I9gem.txt, clear 

*codes to detailed to make use of detailed flags, so we will truncate them.
*however these flags are mainly for use by healthcare professionals focused on 
*specific diagnoses. we want to take ALL icd10 codes associated with a group of 
*icd9 codes. 
*we also must ignore combinations, since if we did not we would have to look at 
*the record-axis conditions, which is infeasible. 
*combinations are a small share anyway
tab combinationflag
drop *flag

replace icd10 = substr(icd10, 1, 3) + "." + substr(icd10, 4, 1) if strlen(icd10) > 3
icd10 check icd10, gen(t)
tab t
*icd10 check utility is not perfect, as each of these codes passed a spot check
list if inlist(icd10, "A05.5", "C4A.0", "G40.A")
drop t

gen l9 = strlen(icd9)
*if icd9 non-numeric, it is bc it has E or V
assert strpos(icd9, "E") | strpos(icd9, "V") if mi(real(icd9))
*and, the non-numeric character is in the first position
assert !mi(real(substr(icd9, 2, 10))) if mi(real(icd9))
*so the length of the numeric portion is equal to the length of the whole minus 1
gen l9num = cond(!mi(real(icd9)), strlen(icd9), strlen(icd9)-1)

gen t = ""
*insert . in the 4th position if 4+ digits and no E or V
replace t = substr(icd9, 1, 3) + "." + substr(icd9, 4, 1) if l9num > 3 & !strpos(icd9, "E") & !strpos(icd9, "V")
*insert . in the 5th position if 4+ digits and there is E 
replace t = substr(icd9, 1, 4) + "." + substr(icd9, 5, 1) if l9num > 3 & strpos(icd9, "E")
*insert . in the 4th position if 3+ digits and there is V
replace t = substr(icd9, 1, 3) + "." + substr(icd9, 4, 2) if l9num > 3 & strpos(icd9, "V")
*take icd9 as it is if 3- digits
replace t = icd9 if l9num <= 3
assert !mi(t)

icd9 check t, gen(s)
assert !s
drop s l*

drop icd9
rename t icd9 
order icd9, after(icd10)

duplicates drop 
compress
label data "cleaned crosswalk of icd9 to icd10. returns all icd10 codes associated with an icd9 code"
save ../mid/icd9toicd10, replace 

*****************
*obtain equivalent icd10 codes for method 3, store in globals 
*for this to work, we need to attach asterisks onto the end of short codes,
*so that they can be properly read when the globals are fed back into icd gen later on
*gen ev = max(strpos(icd9, "E"), strpos(icd9, "V"))
*replace icd9 = icd9 + "*" if strlen(icd9) < 5
replace icd10 = icd10 + "*" if strlen(icd10) == 3 

foreach c of global causes9 {
	if "`c'" == "suicide" continue
	di "`c'"
	icd9 gen `c'9 = icd9, range(${`c'9})
	glevelsof icd10 if `c'9, local(`c'10_3)
	global `c'10_3 ``c'10_3'
	global `c'10_3 : list clean `c'10_3
	drop `c'
}

foreach c of global causes9 {
	*di `"${`c'10_3}"'
	foreach e of global `c'10_3 {
		di `"`e'"'
		if strlen(`"`e'"') < 5 assert strpos(`"`e'"', "*") 
	}
}

*****************
*import icd10 to icd9 crosswalk,  taken from icd general equivalence mappings, downloaded from https://www.cdc.gov/nchs/icd/icd10cm.htm for 2018
infix str icd10 1-8 str icd9 9-14 byte approxflag 15 byte nomapflag 16 ///
byte combinationflag 17 byte scenarioflag 18 byte choicelistflag 19 ///
using ../raw/icd_gem_crosswalks/2018_I10gem.txt, clear 

*codes to detailed to make use of detailed flags, so we will truncate them.
*however these flags are mainly for use by healthcare professionals focused on 
*specific diagnoses. we want to take ALL icd9 codes associated with a group of 
*icd10 codes. 
*we also must ignore combinations, since if we did not we would have to look at 
*the record-axis conditions, which is infeasible. 
*combinations are a small share anyway
tab combinationflag
drop *flag

replace icd10 = substr(icd10, 1, 3) + "." + substr(icd10, 4, 1) if strlen(icd10) > 3
icd10 check icd10, gen(t)
tab t
*icd10 check utility is not perfect, as each of these codes passed a spot check
list if inlist(icd10, "A05.5", "C4A.0", "G40.A")
drop t

drop if icd9 == "NoDx"
gen l9 = strlen(icd9)
*fix one row that has lower case v when it should be upper case
replace icd9 = "V5889" if icd9 == "v5889"
*f icd9 non-numeric, it is bc it has E or V
assert strpos(icd9, "E") | strpos(icd9, "V") if mi(real(icd9))
*and, the non-numeric character is in the first position
assert !mi(real(substr(icd9, 2, 10))) if mi(real(icd9))
*so the length of the numeric portion is equal to the length of the whole minus 1
gen l9num = cond(!mi(real(icd9)), strlen(icd9), strlen(icd9)-1)

gen t = ""
*insert . in the 4th position if 4+ digits and no E or V
replace t = substr(icd9, 1, 3) + "." + substr(icd9, 4, 1) if l9num > 3 & !strpos(icd9, "E") & !strpos(icd9, "V")
*insert . in the 5th position if 4+ digits and there is E 
replace t = substr(icd9, 1, 4) + "." + substr(icd9, 5, 1) if l9num > 3 & strpos(icd9, "E")
*insert . in the 4th position if 3+ digits and there is V
replace t = substr(icd9, 1, 3) + "." + substr(icd9, 4, 2) if l9num > 3 & strpos(icd9, "V")
*take icd9 as it is if 3- digits
replace t = icd9 if l9num <= 3
assert !mi(t)

icd9 check t, gen(s)
assert !s
drop s l*

drop icd9
rename t icd9 
order icd9, after(icd10)

duplicates drop 
compress
label data "cleaned crosswalk of icd10 to icd9. returns all icd9 codes associated with an icd10 code"
save ../mid/icd10toicd9, replace 

*****************
*obtain icd9 codes equivalent to and icd10 code and store in globals 
*for this to work, we need to attach asterisks onto the end of short codes,
*so that they can be properly read when the globals are fed back into icd gen later on
*gen ev = max(strpos(icd9, "E"), strpos(icd9, "V"))
replace icd9 = icd9 + "*" if strlen(icd9) < 5

foreach c of global causes10 {
	if "`c'" == "suicide" continue
	icd10 gen `c'10 = icd10, range(${`c'10})
	glevelsof icd9 if `c'10, local(`c'9_2)
	global `c'9_2 "``c'9_2'"
	drop `c'
}

foreach c of global causes10 {
	*di `"${`c'10_2}"'
	foreach e of global `c'10_2 {
		di `"`e'"'
		if strlen(`"`e'"') < 5 assert strpos(`"`e'"', "*") 
	}
}

*************************************************************************************
*create files of just icd9 and icd10 codes, and use the above globals in 
*stata's icd utilities to determine which codes which which broad categories,
*using a number of different methods. then we separately merge the results onto 
*icd9 and icd10 microdata, and append the eras together. 
use ../mid/micromaster, clear 
gcollapse (first) id, by(icd9 icd10) fast 
drop id
preserve
keep icd9
keep if !mi(icd9)
save ../mid/icd9codes, replace 
restore 
keep icd10 
keep if !mi(icd10)
save ../mid/icd10codes, replace 

use ../mid/icd9codes, clear 
foreach c of global causes9 {
	icd9 gen `c' = icd9, range(${`c'9})
	recast byte `c'
	glevelsof icd9 if `c'
}

foreach c of global causes10 {
	if "`c'" != "suicide" {
		icd9 gen `c'_2 = icd9, range(${`c'9_2})
		recast byte `c'_2
		glevelsof icd9 if `c'_2
		}
		}
		
icd9 gen poison = icd9, range(${poison9})

save ../mid/icd9causes, replace

use ../mid/icd10codes, clear 
foreach c of global causes10 {
	di "`c'"
	
	icd10 gen `c' = icd10, range(${`c'10})
	recast byte `c'
	glevelsof icd10 if `c'
}

foreach c of global causes9 {
	if "`c'" != "suicide" {
		icd10 gen `c'_3 = icd10, range(${`c'10_3})
		recast byte `c'_3
		glevelsof icd10 if `c'_3
		}
	}
icd10 gen poison = icd10, range(${poison10})

save ../mid/icd10causes, replace 

******************************
*icd codes to chapters
use ../mid/icd9codes, clear 

local chapters  1            2          3           4           5           6           7           8           9           10          11          12          13          14          15          16          19
local ranges `" "001/139.9" "140/239.9" "240/279.9" "280/289.9" "290/319.9" "320/389.9" "390/459.9" "460/519.9" "520/579.9" "580/629.9" "630/679.9" "680/709.9" "710/739.9" "740/759.9" "760/779.9" "780/799.9" "E*" "'
forvalues i = 1 / 17 {
	local c : word `i' of `chapters'
	local r : word `i' of `ranges'
	icd9 gen icd9chap`c' = icd9, range(`r')
}

egen t = rowtotal(*chap*)
assert t == 1, fast  
drop t

compress
save ../mid/icd9chapters, replace 

use ../mid/icd10codes, clear 

local chapters 1	2	3	4	5	6	7	8	9	10	11	12	13	14	15	16	17	18	19	20	21
local ranges   A00/B99	C00/D49	D50/D89	E00/E89	F01/F99	G00/G99	H00/H59	H60/H95	I00/I99	J00/J99	K00/K95	L00/L99	M00/M99	N00/N99	O00/O9A	P00/P96	Q00/Q99	R00/R99	S00/T99	V00/Y99	Z00/Z99
forvalues i = 1 / 21 {
	local c : word `i' of `chapters'
	local r : word `i' of `ranges'
	icd10 gen icd10chap`c' = icd10, range(`r'.9)
}

*U codes - death from terrorism 
gen byte icd10chap22 = cond(strpos(icd10, "U"), 1, 0)

egen t = rowtotal(*chap*)
assert t == 1, fast  
drop t

compress
save ../mid/icd10chapters, replace 
