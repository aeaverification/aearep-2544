clear all
pause on
set rmsg on 
set scheme s1color


*NVSR reference population weights (intercensal 2000 based on 1990 census, unpublished), taken from https://www.cdc.gov/nchs/data/statnt/statnt20.pdf
infix a1 1-2 a2 4-5 str num 7-13 wt 15-21 using ../raw/2000popwts.txt, clear 
replace num = subinstr(num, ",", "", .)
destring num, replace
replace num = num * (274633642 / 1000000)
*list
drop num
qui sum wt 
replace wt = wt / r(sum)
gen age3 = _n - 1
list
collapse (sum) wt, by(age3)
save ../mid/agestd_weights_nvsr, replace

*NVSR reference population weights (intercensal 2000 based on 1990 census, unpublished)
*for education mortality rates - only use 4 10-yr groups 25-64 (like NVSR)
infix a1 1-2 a2 4-5 str num 7-13 wt 15-21 using ../raw/2000popwts.txt, clear 
replace num = subinstr(num, ",", "", .)
destring num, replace
replace num = num * (274633642 / 1000000)
keep if a1 >= 25 & a2 <= 64
drop num
qui sum wt 
replace wt = wt / r(sum)
gen age4 = _n 
save ../mid/agestd_weights_nvsr_educ, replace


