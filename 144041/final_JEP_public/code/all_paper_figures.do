#delimit ;
set scheme s1color;
pause on;

local file = "../final_dataset_redacted";

local prg JEP_main_figures_final;
cap log close;

log using ../logs/`prg'.log, replace;

scalar createstartingpoint = 1;
scalar figure1 = 1;
scalar figure2 = 1;
scalar figure3 = 1;
scalar figure4 = 1;
scalar figure5 = 1;
scalar figure6 = 1;
scalar figure7 = 1;

* the below creates a starting point dataset that is used to generate a number of our figures;
if(createstartingpoint==1){;
use `file', clear;
cap drop _merge;

* keep only midlife mortality;
keep if age_group == 3; 
drop age_group;

ren fipstate fips;
sort fips year;
gen lnmr_all       = ln(mr_all);
gen lnmr_NC        = ln(mr_all_NC);
gen lnmr_CD      = ln(mr_all_CD);
gen lnNC_CD_diff = lnmr_NC - lnmr_CD;
cap rename realpcincome ypph;
gen lnypph = ln(ypph);
gen lnpop = ln(pop);
sort fips year;
tsset fips year, y;
cap drop *diff*;

la var smoke "Smoke every day";
la var lnypph "Ln Real Per Capita Income";

gen lnpop_NC_all = ln(pop_NC);
gen lnpop_CD_all = ln(pop_CD);

* keep the relevant years and states;
keep if year >= 1992 & year <= 2016;
drop if inlist(fips, 2, 11, 15) | fips > 56;
drop if inlist(fips, 13, 40, 44, 46);

hashsort fips year;
tempfile startingpoint;
save `startingpoint', replace;
};


if(figure1==1){;
use ../mid/le_berkeley_mean_sds, clear;

la var USLE     "US Life Expectancy at Birth (Years, left scale)";
la var sdle_wtd "Std. Dev. of State-Level Life Expectancy (right scale)";
gen cv_wtd = sdle_wtd/USLE;
la var cv_wtd   "Coef. of Variation of State-Level Life Exp. (right scale)";

gr tw (line USLE     year if year >= 1959, lc(navy))
      (line cv_wtd year if year >= 1959, lc(cranberry) lp(dash) yaxis(2)), 
  xlabel(1960(5)2020)
  xti("")
  yscale(range(70 80.5))
  yscale(range(.012 .0245) axis(2))
  yti("")
  yti("", axis(2) orientation(rvertical))
  ylab(.012(.002).024, axis(2) angle(rvertical)) 
  leg(ring(0) pos(11) cols(1) region(lstyle(none)));

gr export ../paper_figures/figure1.pdf, as(pdf) replace;
};

if(figure2==1){;

**********************************************
* Means and Std Deviations by Age Group;

*****************
* Population weighted, including AK/DC/HI;

use ../mid/readin_WONDER, clear;
* drop if fips == 2 | fips == 11 | fips == 15;
tab state, m;

collapse (sd)    sdcrude = cruderate  sdageadj = ageadjustedrate
        (mean) meancrude = cruderate meanageadj = ageadjustedrate [aw=population], by(age_group year);

gen cvcrude  =  sdcrude/meancrude;
gen cvageadj = sdageadj/meanageadj;
		
drop sd*;

reshape wide cv* mean*, i(year) j(age_group);
tsset year;

foreach x of numlist 1 2 3 4 {;
  la var meanageadj`x' "Mean (left scale)";
  la var cvageadj`x'   "Coefficient of Variation (right scale)";
  la var meancrude`x'  "Mean (left scale)";
  la var cvcrude`x'    "Coefficient of Variation (right scale)";
};

gr tw (line meanageadj1 year, lp(solid) lc(navy)) 
      (line cvageadj1 year, yaxis(2) lp(dash) lc(cranberry)), 
  ti("Child (Ages 0-4)")    ylabel(.10(.05).25, axis(2) angle(rvertical)) ylabel(#3) yti("") yti("", axis(2)) name(g1, replace) xti("");
  
gr tw (line meanageadj2 year, lp(solid) lc(navy)) 
      (line cvageadj2 year, yaxis(2) lp(dash) lc(cranberry)), 
  ti("Young Person (Ages 5-24)") ylabel(.10(.05).25, axis(2) angle(rvertical))  ylabel(#3)  yti("") yti("", axis(2)) name(g2, replace) xti("");
  
gr tw (line meanageadj3 year, lp(solid) lc(navy)) 
      (line cvageadj3 year, yaxis(2) lp(dash) lc(cranberry)), 
  ti("Midlife (Ages 25-64)") ylabel(.10(.05).25, axis(2) angle(rvertical))  ylabel(#3)  yti("") yti("", axis(2)) name(g3, replace) xti("");
 
gr tw (line meanageadj4 year, lp(solid) lc(navy)) 
      (line cvageadj4 year, yaxis(2) lp(dash) lc(cranberry)), 
  ti("Older (Ages 65+)")     ylabel(0(.05).15, axis(2) angle(rvertical))  ylabel(#3)  yti("") yti("", axis(2)) name(g4, replace) xti("");
    

grc1leg g1 g2 g3 g4, /* ti("Age-Adjusted, Population Weighted, includes AK/DC/HI") */ name(f_popwtd_ageadj, replace);
gr export ../paper_figures/figure2.pdf, as(pdf) replace;
};

if(figure3==1){;
use `startingpoint', clear;
rename state_abbrev state;
sort year mr_all;
by year : gen rank = _n;
gen invrank = -rank;

sort year mr_all_NC;
by year : gen rankNC = _n;
gen invrankNC = -rankNC;

sort year mr_all_CD;
by year : gen rankCD = _n;
gen invrankCD = -rankCD;

gen odd = rank/2 - floor(rank/2) > 0;
sort year mr_all;
summ mr_all if year == 1992, detail;
local sd_1992 = r(sd);
local mean_1992 = r(mean);
local cv_1992 = round(`sd_1992' / `mean_1992', 0.001);
corr mr_all lnypph if year == 1992;
local corr_with_income_1992 = round(r(rho), 0.01);

*** GENERATE GAP GRAPHS FOR ALL, HIGHLIGHTING OHIO AND CALIFORNIA;
gr tw (scatter mr_all invrank if year == 1992 & odd == 0 & !inlist(fips, 6, 39), mlab(state) msize(vsmall) mlabsize(vsmall) mlabpos(12) mcol(dkgreen) mlabcol(dkgreen)) 
      (scatter mr_all invrank if year == 1992 & odd == 1 & !inlist(fips, 6, 39), mlab(state) mlabsize(vsmall) msize(vsmall) mlabpos(6) mcol(dkgreen) mlabcol(dkgreen)) 
      (scatter mr_all_NC invrank if year == 1992, msize(vsmall) mcol(dkgreen) connect(l) lc(dkgreen)) 
      (scatter mr_all_CD invrank if year == 1992, msize(vsmall) mcol(dkgreen) connect(l) lc(dkgreen))
      (line mr_all invrank if year == 1992, lp(solid) lc(dkgreen))
      (scatter mr_all invrank if year == 1992 & fips==6, mlab(state) mlabsize(vsmall) msize(vsmall) mlabpos(6) mcol(red) mlabcol(red)) 
      (scatter mr_all invrank if year == 1992 & fips==39, mlab(state) mlabsize(vsmall) msize(vsmall) mlabpos(12) mcol(red) mlabcol(red)),

	     xti("Overall Mortality Rank in 1992")
		 ti("1992")
		 ylab(0(100)700)
		 yti("Age-Adjusted Mortality Rate")
		 text(550 -12 "Coefficient of Variation: `cv_1992'", size(small))
		 /*text(550 -13 "{&rho}{subscript: income}: `corr_with_income_1992'", size(small))*/
		 xlabel(none) xsc(r(-50 -1))
		 leg(off)
		 name(overall1992, replace);
gr play grec1.grec;
gr save ../paper_figures/figure_3_1992.gph, replace;
gr export ../paper_figures/figure_3_1992.pdf, replace;


summ mr_all if year == 2000, detail;
local sd_2000 = r(sd);
local mean_2000 = r(mean);
local cv_2000 = round(`sd_2000' / `mean_2000', 0.001);
corr mr_all lnypph if year == 2000;
local corr_with_income_2000 = round(r(rho), 0.01);
gr tw (scatter mr_all invrank if year == 2000 & odd == 0 & !inlist(fips, 6, 39), mlab(state) msize(vsmall) mlabsize(vsmall) mlabpos(12) mcol(dkgreen) mlabcol(dkgreen)) 
      (scatter mr_all invrank if year == 2000 & odd == 1 & !inlist(fips, 6, 39), mlab(state) mlabsize(vsmall) msize(vsmall) mlabpos(6) mcol(dkgreen) mlabcol(dkgreen)) 
      (scatter mr_all_NC invrank if year == 2000, msize(vsmall) mcol(dkgreen) connect(l) lc(dkgreen)) 
      (scatter mr_all_CD invrank if year == 2000, msize(vsmall) mcol(dkgreen) connect(l) lc(dkgreen)) 
      (line mr_all invrank if year == 2000, lp(solid) lc(dkgreen))
      (scatter mr_all invrank if year == 2000 & fips==6, mlab(state) mlabsize(vsmall) msize(vsmall) mlabpos(6) mcol(red) mlabcol(red)) 
      (scatter mr_all invrank if year == 2000 & fips==39, mlab(state) mlabsize(vsmall) msize(vsmall) mlabpos(6) mcol(red) mlabcol(red)) ,

	     xti("Overall Mortality Rank in 2000")
		 ti("2000")
		 ylab(0(100)700)
		 yti("Age-Adjusted Mortality Rate")
		 xlabel(none)
		 text(550 -12 "Coefficient of Variation: `cv_2000'", size(small))
		 /*text(550 -13 "{&rho}{subscript: income}: `corr_with_income_2000'", size(small))*/
		 leg(off) xsc(r(-50 -1))
		 name(overall2000, replace);

gr export ../paper_figures/figure_3_2000.pdf, replace;

summ mr_all if year == 2016, detail;
local sd_2016 = r(sd);
local mean_2016 = r(mean);
local cv_2016 = round(`sd_2016' / `mean_2016', 0.001);
corr mr_all lnypph if year == 2016;
local corr_with_income_2016 = round(r(rho), 0.01);
gr tw (scatter mr_all invrank if year == 2016 & odd == 0 & !inlist(fips, 6, 39), mlab(state) msize(vsmall) mlabsize(vsmall) mlabpos(12) mcol(dkgreen) mlabcol(dkgreen)) 
      (scatter mr_all invrank if year == 2016 & odd == 1 & !inlist(fips, 6, 39), mlab(state) mlabsize(vsmall) msize(vsmall) mlabpos(6) mcol(dkgreen) mlabcol(dkgreen)) 
      (scatter mr_all_NC invrank if year == 2016, msize(vsmall) mcol(dkgreen) connect(l) lc(dkgreen)) 
      (scatter mr_all_CD invrank if year == 2016, msize(vsmall) mcol(dkgreen) connect(l) lc(dkgreen))
      (line mr_all invrank if year == 2016, lp(solid) lc(dkgreen))
      (scatter mr_all invrank if year == 2016 & fips==6, mlab(state) mlabsize(vsmall) msize(vsmall) mlabpos(12) mcol(red) mlabcol(red)) 
      (scatter mr_all invrank if year == 2016 & fips==39, mlab(state) mlabsize(vsmall) msize(vsmall) mlabpos(6) mcol(red) mlabcol(red)),

	     xti("Overall Mortality Rank in 2016")
		 ti("2016")
		 ylab(0(100)700)
		 yti("Age-Adjusted Mortality Rate")
		 xlabel(none)
		 text(575 -12 "Coefficient of Variation: `cv_2016'", size(small))
		 leg(off) xsc(r(-50 -1))
		 name(overall2016, replace);
		 gr export ../paper_figures/figure_3_2016.pdf, replace;

};


if(figure4==1){;
use `startingpoint', clear;

* considering two equation system
* mr_it^(C) = phi_t + u_(it)^{C}
* mr_it^(H) = phi_t + phi_et + u_(it)^(H);

* to estimate phi_t, make data long, and then demean by year
* we will do this later
* we first estimate phi_et;

gen lnmr_diff = lnmr_NC - lnmr_CD;
gcollapse (mean) phi_et = lnmr_diff, by(year) fast merge;

*******************************************************************
* Now estimate college shares that force everything to be exact. 
* We do this by finding the CD and NC shares that will generate 
* the state-level mortality rate when these college shares are applied to the 
* education-specific rates for that state. Call this new share variable COLLSHARE2,
* to distinguish it from the existing COLLSHARE
* Note that the results / this graph are NOT significantly affected whichever choice is made;

gen double collshare2 = (mr_all - mr_all_NC)/(mr_all_CD - mr_all_NC);

gen double mycheck = collshare2*mr_all_CD + (1-collshare2)*mr_all_NC;
assert abs(mycheck - mr_all) < .000000000001 if !missing(mycheck) & !missing(mr_all);
drop mycheck;

*****************************************************
* Reshape the data into long form; 
keep fips year lnmr_CD lnmr_NC phi_et pop collshare2;
reshape long lnmr_, i(fips year) j(class) string;

replace phi_et = 0 if class == "CD";
gcollapse (mean) phi_t = lnmr_, by(year) fast merge;
gen myerror_ = lnmr_ - phi_t - phi_et;

gen thisshare = 0;
replace thisshare = collshare2   if class == "CD";
replace thisshare = 1-collshare2 if class == "NC";

save ../mid/`prg'_longdata, replace;

use ../mid/`prg'_longdata;

sort fips year class;
gen share92   = thisshare if year == 1992;
gen phi_t92  = phi_t    if year == 1992;
gen phi_et92  = phi_et    if year == 1992;
gen error92   = myerror_  if year == 1992;
gen error92CDonly = myerror_ if year == 1992 | class == "NC";
gen error92NConly = myerror_ if year == 1992 | class == "CD";

sort fips class year;
by fips class: carryforward share92 phi_t92 phi_et92 error92 error92CDonly error92NConly, replace;

* COUNTER1: Change college error before high school error;
gen counter1a = exp(phi_t92 + phi_et92 + error92);
gen counter1x = counter1a;
gen counter1b = exp(phi_t    + phi_et92   + error92);
gen counter1c = exp(phi_t    + phi_et   + error92);
gen counter1d = exp(phi_t    + phi_et   + error92NConly);
gen counter1e = exp(phi_t   + phi_et   + myerror_);

sort fips year class;
save ../mid/`prg'_counters_classes, replace;

***********************************
* Now figure state-level means of the counterfactuals 
* by collapsing education-specific classes
* using the appropriate college shares (either 1992 or actual);

use  ../mid/`prg'_counters_classes, clear;
collapse (mean) counter1x [aw=share92], by(fips year);
gen lncounter1x = ln(counter1x);
save ../mid/temp1, replace;

use ../mid/`prg'_counters_classes, clear;
collapse (mean) counter1a counter1b counter1c counter1d counter1e [aw=thisshare], by(fips year);
gen lncounter1a = ln(counter1a);
gen lncounter1b = ln(counter1b);
gen lncounter1c = ln(counter1c);
gen lncounter1d = ln(counter1d);
gen lncounter1e = ln(counter1e);

merge 1:1 fips year using ../mid/temp1, assert(3);
drop _merge;
erase ../mid/temp1.dta;

* Merge in selected state-wide variables from TWID2 so that we can check our work;
sort fips year;
merge 1:1 fips year using `startingpoint', keepusing(fips year lnypph mr_all lnmr_all mr_all_CD mr_all_NC pop);
keep if _merge == 3;
drop _merge;

save ../mid/`prg'_counters_states, replace;

*****************************
* Check class-specific and state-wide rates against data from TWID2;

use ../mid/`prg'_counters_classes, clear;
keep fips year counter1e class;
reshape wide counter1e, i(fips year) j(class) string;

order fips year *CD* *NC*;
merge 1:1 fips year using ../mid/`prg'_counters_states;
keep if _merge == 3;

* note that COUNTER1d and COUNTER2d use actual data throughout, so they should both
* match MR_ALL -- the state-wide mortality rate;
tsline mr_all counter1e if fips == 36, lp(solid dash) leg(cols(1)) name(ratecheck36, replace);
tsline mr_all counter1e if fips == 10, lp(solid dash) leg(cols(1)) name(ratecheck10, replace);

assert abs(mr_all - counter1e) < .001 if !missing(mr_all) & !missing(counter1e);


* same for the education-class specific rates;
assert abs(mr_all_NC - counter1eNC) < .001 if !missing(mr_all_NC) & !missing(counter1eNC);

assert abs(mr_all_CD - counter1eCD) < .001 if !missing(mr_all_CD) & !missing(counter1eCD);

save ../mid/`prg'_counters_checks, replace;

*************************************
* Find standard deviations of the state-wide data and the various counterfactuals;

use ../mid/`prg'_counters_states, clear;
collapse (sd) lncounter1x lncounter1a lncounter1b lncounter1c lncounter1d lncounter1e
			  lnmr_all [aw = pop], by(year);

* education only model, decomposition of variance
la var lncounter1x "1992 Variance";			
la var lncounter1a "Use Actual College Shares";
la var lncounter1b "Add Actual Year Effect";
la var lncounter1c "Add Actual Education Effect";
la var lncounter1d "Add College Error Term";
la var lncounter1e "Add High School Error Term";
la var lnmr_all    "Actual State-Level Data";

tsline lncounter1x lncounter1a lncounter1b lncounter1c lncounter1d lncounter1e lnmr_all,
  lc(navy cranberry dkgreen brown cyan khaki pink)
  lp(solid dash long solid solid dash dash_dot) 
  ti("Variance Decomposition of Education-Only Model")
  xlabel(1992(3)2016)
  ylabel(.10(.02).2)
  name(counter1, replace) xti("") leg(ring(0) pos(11) cols(1) region(lstyle(none)));
  
gr play decomp1.grec;
gr play decomp2.grec;
  
gr export ../paper_figures/figure4.pdf, as(pdf) replace;
gr save ../paper_figures/figure4.gph, replace;
clear;

};


if(figure5==1){;
* data downloaded from Wonder for this figure;
use ../mid/readin_WONDER, clear;
keep fips year cruderate ageadjustedrate population age_group;
reshape wide cruderate ageadjustedrate population, i(fips year) j(age_group);

sort fips year;
merge 1:1 fips year using ../mid/bea_income;


tab _merge;
keep if year >= 1968 & year <= 2019;
assert _merge == 3;
drop _merge;
fips2state;

gen ypph_1968x = ypph if year == 1968;
egen ypph_1968 = max(ypph_1968x), by(fips);
drop ypph_1968x;

* 4-panel plots;

drop if fips == 2 | fips == 11 | fips == 15;
drop if inlist(fips, 13, 40, 44, 46);

la var ageadjustedrate3 "Age-Adjusted Mortality Rate";

la var ypph      "Real Income per Person ($2012)";
la var ypph_1968 "Real Income per Person in 1968 ($2012)";

* Midlife;
corr ageadjustedrate3 ypph if year == 1968;
local this_corr = round(r(rho), 0.01);
local this_corr : di %3.2f `this_corr';
gr tw (scatter ageadjustedrate3 ypph      if year == 1968 & inlist(fips, 6, 36, 5, 39), msymbol(circle) msize(medsmall) mlab(state) mlabpos(12) mcolor(dkgreen)) (scatter ageadjustedrate3 ypph      if year == 1968 & !inlist(fips, 6, 36, 5, 35), msymbol(circle) msize(medsmall) mcolor(dkgreen)), ti("1968 Mortality vs. 1968 Income") name(ga, replace) text(800 22500 "{&rho}:  `this_corr'") ylabel(#3) leg(off);
corr ageadjustedrate3 ypph if year == 1980;
local this_corr = round(r(rho), 0.01);
local this_corr : di %3.2f `this_corr';
gr tw (scatter ageadjustedrate3 ypph      if year == 1980 & inlist(fips, 6, 36, 5, 39), msymbol(circle) msize(medsmall) mlab(state) mlabpos(12) mcolor(dkgreen)) (scatter ageadjustedrate3 ypph      if year == 1980 & !inlist(fips, 6, 36, 5, 35), msymbol(circle) msize(medsmall) mcolor(dkgreen)),   ti("1980 Mortality vs. 1980 Income") name(gb, replace) text(580 27500 "{&rho}:  `this_corr'") ylabel(#3) leg(off);
corr ageadjustedrate3 ypph if year == 2019;
local this_corr = round(r(rho), 0.01);
local this_corr : di %3.2f `this_corr';
gr tw (scatter ageadjustedrate3 ypph      if year == 2019 & inlist(fips, 6, 36), msymbol(circle) msize(medsmall) mlab(state) mlabpos(6) mcolor(dkgreen)) (scatter ageadjustedrate3 ypph      if year == 2019 & inlist(fips, 39), msymbol(circle) msize(medsmall) mlab(state) mlabpos(12) mcolor(dkgreen) mlabcol(dkgreen)) (scatter ageadjustedrate3 ypph      if year == 2019 & inlist(fips, 5), msymbol(circle) msize(medsmall) mlab(state) mlabpos(7) mcolor(dkgreen) mlabcol(dkgreen)) (scatter ageadjustedrate3 ypph      if year == 2019 & !inlist(fips, 6, 36, 5, 35), msymbol(circle) msize(medsmall) mcolor(dkgreen)),  ti("2019 Mortality vs. 2019 Income") name(gc, replace) text(570 63333 "{&rho}:  `this_corr'") ylabel(#3) leg(off);
corr ageadjustedrate3 ypph_1968 if year == 2019;
local this_corr = round(r(rho), 0.01);
local this_corr : di %3.2f `this_corr';
gr tw (scatter ageadjustedrate3 ypph_1968     if year == 2019 & inlist(fips, 6, 5, 39), msymbol(circle) msize(medsmall) mlab(state) mlabpos(6) mcolor(dkgreen)) (scatter ageadjustedrate3 ypph_1968     if year == 2019 & inlist(fips, 36), msymbol(circle) msize(medsmall) mlab(state) mlabpos(3) mlabcol(dkgreen) mcolor(dkgreen)) (scatter ageadjustedrate3 ypph_1968      if year == 2019 & !inlist(fips, 6, 36, 5, 35), msymbol(circle) msize(medsmall) mcolor(dkgreen)),   ti("2019 Mortality vs. 1968 Income") name(gd, replace) text(570 22500 "{&rho}:  `this_corr'") ylabel(#3) leg(off);
gr combine ga gb gc gd;
gr export ../paper_figures/figure5.pdf, as(pdf) replace;
};

if(figure6==1){;
use `startingpoint', clear;
collapse (mean) mr_all mr_other mr_despair (sd) sd_mr_all = mr_all sd_mr_other = mr_other [aw = pop], by(year);
gen cv_mr_all = sd_mr_all/mr_all;
gen cv_mr_other = sd_mr_other/mr_other;
la var cv_mr_all   "All-Cause Mortality Rate";
la var cv_mr_other     "Omitting Deaths of Despair";

tsline cv_mr_all cv_mr_other, 
  lp(solid dash)
  lc(navy cranberry)
  yti("Coefficient of Variation")
  leg(ring(0) pos(11) cols(1) region(lstyle(none)))
  xlabel(1992(3)2016)
  xti("")
  name(components_all, replace);
gr export ../paper_figures/figure6.pdf, as(pdf) replace;

};

if(figure7==1){;
use `startingpoint', clear;
collapse (mean) mr_all mr_despair mr_circulatory mr_cerebrovasc mr_other mr_lowerresp mr_cancer [aw = pop], by(year);
keep if inlist(year, 1992, 2000, 2008, 2016);
order year, first;
drop mr_all mr_other;
reshape long mr_, i(year) j(cause) string;
reshape wide mr_, i(cause) j(year);
replace cause = "Diseases of heart" if cause == "circulatory";
replace cause = "Malignant neoplasms" if cause == "cancer";
replace cause = "Despair" if cause == "despair";
replace cause = "Cerebrovascular diseases" if cause == "cerebrovasc";
replace cause = "Chronic lower respiratory" if cause == "lowerresp";
graph dot mr_1992 mr_2000  mr_2008 mr_2016, ti("Means") over(cause, sort(mr_1992) descending relabel(2 `" "Chronic lower"   "respiratory" "' 1 `" "Cerebrovascular" "diseases" "' 5 `" "Malignant"   "neoplasms (cancer)" "')) linegap(40) name(correlations, replace) marker(1, msymbol(circle) mcolor(navy)) marker(2, msymbol(diamond) mcolor(cranberry)) marker(3, msymbol(square) mcolor(dkgreen)) marker(4, msymbol(triangle) mcolor(brown))  /*leg(order(1 "1992" 2 "2000" 3 "2008" 4 "2016") row(1))*/ legend(off);
gr export ../paper_figures/figure7a.pdf, as(pdf) replace;

use `startingpoint', clear;
hashsort year;

gen   corr_all = .;
gen   corr_despair = .;
gen   corr_circulatory = .;
gen   corr_cancer = .;
gen   corr_cerebrovasc = .;
gen   corr_other = .;
gen   corr_lowerresp = .;

gen wtcorr_all = .;
gen wtcorr_despair = .;
gen wtcorr_circulatory = .;
gen wtcorr_cancer = .;
gen wtcorr_cerebrovasc = .;
gen wtcorr_other = .;
gen wtcorr_lowerresp = .;


foreach v in all cancer circulatory despair cerebrovasc other lowerresp {;
foreach y of numlist 1992 2000 2008 2016 {;

	corr mr_`v' ypph            if year == `y';
	replace   corr_`v' = r(rho) if year == `y';
    corr mr_`v' ypph [aw=pop]   if year == `y';
	replace wtcorr_`v' = r(rho) if year == `y';

};
};

sort year fips;
by year: keep if _n == 1;		 
drop mr_*;

keep if inlist(year, 1992, 2000, 2008, 2016);
order year, first;

drop  corr_all   corr_other 
      wtcorr_all wtcorr_other;

keep year corr* wtcorr*;

reshape long corr_ wtcorr_ , i(year) j(cause) string;
reshape wide corr_ wtcorr_ , i(cause) j(year);

gen sortorder = .;
replace sortorder = 1 if cause == "cancer";
replace sortorder = 2 if cause == "circulatory";
replace sortorder = 3 if cause == "despair";
replace sortorder = 4 if cause == "cerebrovasc";
replace sortorder = 5 if cause == "lowerresp";

replace cause = "Diseases of heart" if cause == "circulatory";
replace cause = "Malignant neoplasms" if cause == "cancer";
replace cause = "Despair" if cause == "despair";
replace cause = "Cerebrovascular diseases" if cause == "cerebrovasc";
replace cause = "Chronic lower respiratory" if cause == "lowerresp";

graph dot corr_1992 corr_2000 corr_2008 corr_2016, ti("Correlations with Income") over(cause, sort(sortorder))
  linegap(40) 
  name(correlations, replace) 
  marker(1, msymbol(circle) mcolor(navy)) 
  marker(2, msymbol(diamond) mcolor(cranberry))
  marker(3, msymbol(square) mcolor(dkgreen)) 
  marker(4, msymbol(triangle) mcolor(brown))
  ylabel(-1(0.25)1) 
  yline(0, lp(dash) lc(gray)) 
  leg(order(1 "1992" 2 "2000" 3 "2008" 4 "2016") row(1));
gr export ../paper_figures/figure7b.pdf, as(pdf) replace;

/*;
graph dot corr_1992 corr_2000 corr_2008 corr_2016, ti("Correlations with Income") over(cause, sort(sortorder)  
                                                                                              relabel(2 `" "Chronic lower"   "respiratory" "' 1 `" "Cerebrovascular" "diseases" "' 5 `" "Malignant"   "neoplasms" "') )
  linegap(40) 
  name(correlations, replace) 
  marker(1, msymbol(circle) mcolor(navy)) 
  marker(2, msymbol(diamond) mcolor(cranberry))
  marker(3, msymbol(square) mcolor(dkgreen)) 
  marker(4, msymbol(triangle) mcolor(brown))
  ylabel(-1(0.25)1) 
  yline(0, lp(dash) lc(gray)) 
  leg(order(1 "1992" 2 "2000" 3 "2008" 4 "2016") row(1));
gr export ./paper_figures/figure7b_unwtd.pdf, as(pdf) replace;

graph dot wtcorr_1992 wtcorr_2000 wtcorr_2008 wtcorr_2016, ti("Correlations with Income (Weighted by Population)", span) over(cause, sort(sortorder)  
                                                                                                                         relabel(2 `" "Chronic lower"   "respiratory" "' 1 `" "Cerebrovascular" "diseases" "' 5 `" "Malignant"   "neoplasms" "'))
  linegap(40) 
  name(correlations, replace) 
  marker(1, msymbol(circle) mcolor(navy)) 
  marker(2, msymbol(diamond) mcolor(cranberry))
  marker(3, msymbol(square) mcolor(dkgreen)) 
  marker(4, msymbol(triangle) mcolor(brown))
  ylabel(-1(0.25)1) 
  yline(0, lp(dash) lc(gray)) 
  leg(order(1 "1992" 2 "2000" 3 "2008" 4 "2016") row(1));
gr export ../paper_figures/figure7b_wgtd.pdf, as(pdf) replace;*/;
};




gr close _all;
log close;
clear;
