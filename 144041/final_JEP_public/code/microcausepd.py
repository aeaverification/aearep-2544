import numpy as np
import pandas as pd
import time
import random
import re
import gc
# run garbage collection to try to perhaps clean out memory
gc.collect()
import sys
import os

# OVERALL PURPOSE OF FILE: take cleaned micromaster file, and mappings of icd9/icd10 codes to causes and chapters.
# Associate all of the deaths in micromaster with a cause and chapter, specifically using binary column variables 
# for each cause and chapter.  Add on opioid, unspecified drug, and non-opioid deaths by looking specifically at 
# the record axis column.  Make sure all is consistent across codes.

# all of the columns, and their optimal data type
all_cols = ['year', 'month','dow','pod','fipstateocc','fipstateres','sex','age','marstat','fipstatebirth','ind','ind2','occ','occ2','educ89','educr',\
                                                   'icd9','aut','work', 'entaxnum','entaxcon','recaxnum','recaxcon','race','hisp','manner','act','icd10','educ03','preg','tob','disp',\
                                                   'confipsctyres','confipsctyocc','poi','datem','educf','education','id']
all_dtype = {'year': 'int16', 'month': 'int8', 'dow': 'int8', 'pod': 'int8', 'fipstateocc': 'int8', 'fipstateres': 'int8', 'sex':'int8','age': 'float16', \
                                                 'marstat': 'int8', 'fipstatebirth': 'int8', 'ind': 'float16', 'ind2': 'float16', 'occ': 'float16', 'occ2': 'float16','educ89': 'int8', \
                                                 'educr': 'float16', 'icd9': 'object', 'icd10': 'object', 'aut':'float16','work':'float16', 'entaxnum':'int8','entaxcon':'object',\
                                                 'recaxnum':'int8','recaxcon':'object','race':'int8','hisp':'int8','manner': 'float16', 'act': 'float16', 'educ03': 'int8','preg': 'float16',\
                                                 'tob': 'float16', 'disp': 'float16', 'confipsctyres': 'int16', 'confipsctyocc': 'int16','poi': 'float16','datem': 'object',
                                                 'educf': 'int8','education':'int8','id':'float64'}
# the columns we are going to read in, and their datatypes
real_cols = ['year', 'month','dow','fipstateocc','fipstateres','sex','age','educ89','educr','marstat',\
                                                   'icd9','recaxcon','race','hisp','icd10','educ03',\
                                                   'confipsctyres','confipsctyocc','educf','education', 'id']
real_dtype = {var: all_dtype[var] for var in real_cols}
# only read in real_cols -- this really speeds up the process
df = pd.read_csv("../mid/micromaster.csv", usecols = real_cols, \
                                        dtype = real_dtype)

# restrict to less than 1999, and merge on the causes based on the icd9 code
icd9_causespd = pd.read_stata("../mid/icd9causes.dta")
df_icd9 = df[df.year<1999]
df_icd9 = df_icd9.merge(icd9_causespd, on = 'icd9', how='inner')

# generate a new column, opioid poisonings
df_icd9["poison_op"] = 0
df_icd9["poison_op"] = df_icd9["poison_op"].astype('int8')
opcodes = ["85000", "85010", "85020", "96501"]
intcodes = [85000, 85010, 85020, 96501]
temp_arrays = []
# look at the record axis codes; if any of them are equal to the above codes, this is an opioid poisoning
for i in range(1, 21):
    j = ((i-1)*5)
    temp = df_icd9["recaxcon"].str.slice(j,j+5)
    temp_arrays.append(temp)
    for code in opcodes:
        df_icd9["poison_op"] = df_icd9["poison_op"].mask(temp==code, 1)
    #df_icd9 = df_icd9.persist()  

# generate a new column for unspecified drug poisonings
df_icd9["poison_unspec"] = 0
df_icd9["poison_unspec"] = df_icd9["poison_unspec"].astype('int8')
opcode = "85890"
intcode = 85890
# again, classify these based on the record axis codes (note that this may also be in the actual icd9 code)
# must also be a poisoning and not an opioid poisoning
for i in range(1, 21):
    j = ((i-1)*5)
    #temp = df_icd9["recaxcon"].str.slice(j,j+5)
    temp = temp_arrays[i-1]
    df_icd9["poison_unspec"] = df_icd9["poison_unspec"].mask((temp==opcode) & (df_icd9["poison"]==1) & (df_icd9["poison_op"]==0), 1)
    #df_icd9 = df_icd9.persist()  
    
# all codes that are poisonings but *not* opioid poisonings
df_icd9["poison_notop"] = 0
df_icd9["poison_notop"] = df_icd9["poison_notop"].astype('int8')
df_icd9["poison_notop"] = df_icd9["poison_notop"].mask((df_icd9["poison"]>0) & (df_icd9["poison_op"]==0), 1)

del temp_arrays
icd10_causespd = pd.read_stata("../mid/icd10causes.dta")
# restrict to less than 1999, and merge on the causes based on the icd10 code
df_icd10 = df[df.year>=1999]
df_icd10 = df_icd10.merge(icd10_causespd, on = 'icd10', how='inner')

# generate a new column, opioid poisonings
df_icd10["poison_op"] = 0
opcodes = ["T400", "T401", "T402", "T403", "T404", "T406"]
df_icd10["poison_op"] = df_icd10["poison_op"].astype('int8')
# look at the record axis codes; if any of them are equal to the above codes, this is an opioid poisoning
for code in opcodes:
    df_icd10["poison_op"] =df_icd10["poison_op"].mask((df_icd10["poison"]==1)&(df_icd10["recaxcon"].str.contains(code)), 1)
    #df_icd10 = df_icd10.persist()  
    
# again, classify unspecified poisonings based on the record axis codes
df_icd10["poison_unspec"] = 0
df_icd10["poison_unspec"] = df_icd10["poison_unspec"].astype('int8')
df_icd10["poison_unspec"] = df_icd10["poison_unspec"].mask((df_icd10["recaxcon"].str.contains("T509")) & (df_icd10["poison"]==1) & (df_icd10["poison_op"]==0), 1)  

# all codes that are poisonings but *not* opioid poisonings
df_icd10["poison_notop"] = 0
df_icd10["poison_notop"] = df_icd10["poison_notop"].astype('int8')
df_icd10["poison_notop"] = df_icd10["poison_notop"].mask((df_icd10["poison"]>0) & (df_icd10["poison_op"]==0), 1)

# add the relevant columns to each separate dataframe
# this speeds up pd.concat, and also forces them to be float32, not float64
icd10cols = df_icd10.columns.values.tolist()
icd9cols = df_icd9.columns.values.tolist()
icd10notin9 = [col for col in icd10cols if col not in icd9cols]
icd9notin10 = [col for col in icd9cols if col not in icd10cols]
all_columns = []
all_columns.extend(icd10notin9)
all_columns.extend(icd9notin10)
for c in icd9notin10:
    df_icd10[c] = np.nan
    df_icd10[c] = df_icd10[c].astype('float32')
    df_icd9[c] = df_icd9[c].astype('float32')
for c in icd10notin9:
    df_icd9[c] = np.nan
    df_icd10[c]= df_icd10[c].astype('float32')
    df_icd9[c] = df_icd9[c].astype('float32')

# concatenate the two dataframes together
df_fin = pd.concat([df_icd9, df_icd10], axis = 0)
set_of_columns = df_fin.columns.values.tolist()

# set method 2 and method 3 according to method 1 in the years not covered
underscoretwo = [i for i in set_of_columns if i[-2:] =="_2"]
underscoretwosubstrings = [j[:-2] for j in underscoretwo]
underscorethree = [i for i in set_of_columns if i[-2:] =="_3"]
underscorethreesubstrings = [j[:-2] for j in underscorethree]

for i in range(len(underscoretwo)):
    df_fin[underscoretwo[i]] = df_fin[underscoretwo[i]].mask(df_fin["year"]>1998, df_fin[underscoretwosubstrings[i]])
    
for i in range(len(underscorethree)):
    df_fin[underscorethree[i]] = df_fin[underscorethree[i]].mask(df_fin["year"]<=1998, df_fin[underscorethreesubstrings[i]])

# merge on icd9 and icd10 chapters based on icd9 and icd10 codes
icd9chapters = pd.read_stata("../mid/icd9chapters.dta")
df_fin = df_fin.merge(icd9chapters, on = 'icd9', how='left')
icd10chapters = pd.read_stata("../mid/icd10chapters.dta")
df_fin = df_fin.merge(icd10chapters, on = 'icd10', how='left')

# fill in which chapter is nonzero for icd9_chapter and icd10_chapter
set_of_columnstwo = df_fin.columns.values.tolist()
icd9chaps = [i for i in set_of_columnstwo if i[:8] =="icd9chap"]
icd10chaps = [i for i in set_of_columnstwo if i[:9] =="icd10chap"]
df_fin["icd9_chapter"] = np.nan
print("I am here 5")
for chap in icd9chaps:
    chap_num = int(chap[8:])
    df_fin["icd9_chapter"] = df_fin["icd9_chapter"].mask(df_fin[chap]==1, chap_num)
    del df_fin[chap]

df_fin["icd10_chapter"] = np.nan
for chap in icd10chaps:
    chap_num = int(chap[9:])
    df_fin["icd10_chapter"] = df_fin["icd10_chapter"].mask(df_fin[chap]==1, chap_num)
    del df_fin[chap]


# based on which chapter, generate binary variables for each chapter
df_fin["chap1"] = (df_fin["icd9_chapter"]==1)|(df_fin["icd10_chapter"]==1)
df_fin["chap1"] = df_fin["chap1"].astype('int8')
#df_fin = df_fin.persist()
df_fin["chap2"] = (df_fin["icd9_chapter"]==2)|(df_fin["icd10_chapter"]==2)
df_fin["chap2"] = df_fin["chap2"].astype('int8')
#df_fin = df_fin.persist()
df_fin["chap3"] = (df_fin["icd9_chapter"]==4)|(df_fin["icd10_chapter"]==3)
df_fin["chap3"] = df_fin["chap3"].astype('int8')
#df_fin = df_fin.persist()
df_fin["chap4"] = (df_fin["icd9_chapter"]==3)|(df_fin["icd10_chapter"]==4)
df_fin["chap4"] = df_fin["chap4"].astype('int8')
#df_fin = df_fin.persist()
df_fin["chap5"] = (df_fin["icd9_chapter"]==5)|(df_fin["icd10_chapter"]==5)
df_fin["chap5"] = df_fin["chap5"].astype('int8')
#df_fin = df_fin.persist()
df_fin["chap6"] = (df_fin["icd9_chapter"]==6)|(df_fin["icd10_chapter"]==6)|(df_fin["icd10_chapter"]==7)|(df_fin["icd10_chapter"]==8)
df_fin["chap6"] = df_fin["chap6"].astype('int8')
#df_fin = df_fin.persist()
df_fin["chap7"] = (df_fin["icd9_chapter"]==7)|(df_fin["icd10_chapter"]==9)
df_fin["chap7"] = df_fin["chap7"].astype('int8')
#df_fin = df_fin.persist()
df_fin["chap8"] = (df_fin["icd9_chapter"]==8)|(df_fin["icd10_chapter"]==10)
df_fin["chap8"] = df_fin["chap8"].astype('int8')
#df_fin = df_fin.persist()
df_fin["chap9"] = (df_fin["icd9_chapter"]==9)|(df_fin["icd10_chapter"]==11)
df_fin["chap9"] = df_fin["chap9"].astype('int8')
#df_fin = df_fin.persist()
df_fin["chap10"] = (df_fin["icd9_chapter"]==10)|(df_fin["icd10_chapter"]==14)
df_fin["chap10"] = df_fin["chap10"].astype('int8')
#df_fin = df_fin.persist()
df_fin["chap11"] = (df_fin["icd9_chapter"]==11)|(df_fin["icd10_chapter"]==15)
df_fin["chap11"] = df_fin["chap11"].astype('int8')
#df_fin = df_fin.persist()
df_fin["chap12"] = (df_fin["icd9_chapter"]==12)|(df_fin["icd10_chapter"]==12)
df_fin["chap12"] = df_fin["chap12"].astype('int8')
#df_finb = df_fin.persist()
df_fin["chap13"] = (df_fin["icd9_chapter"]==13)|(df_fin["icd10_chapter"]==13)
df_fin["chap13"] = df_fin["chap13"].astype('int8')
#df_fin = df_fin.persist()
df_fin["chap14"] = (df_fin["icd9_chapter"]==14)|(df_fin["icd10_chapter"]==17)
df_fin["chap14"] = df_fin["chap14"].astype('int8')
#df_fin = df_fin.persist()
df_fin["chap15"] = (df_fin["icd9_chapter"]==15)|(df_fin["icd10_chapter"]==16)
df_fin["chap15"] = df_fin["chap15"].astype('int8')
#df_fin = df_fin.persist()
df_fin["chap16"] = (df_fin["icd9_chapter"]==16)|(df_fin["icd10_chapter"]==18)
df_fin["chap16"] = df_fin["chap16"].astype('int8')
#df_fin = df_fin.persist()
df_fin["chap17"] = (df_fin["icd9_chapter"]==19)|(df_fin["icd10_chapter"]==19)|(df_fin["icd10_chapter"]==20)
df_fin["chap17"] = df_fin["chap17"].astype('int8')
#df_fin = df_fin.persist()
df_fin["chap18"] = (df_fin["icd10_chapter"]==22)
df_fin["chap18"] = df_fin["chap18"].astype('int8')

# delete old dataframes to free up some space, since to_stata will copy the dataframe
del df
del df_icd9
del df_icd10
gc.collect()

# this section just checks the size of what we're currently working with
print(df_fin.memory_usage().sum()/1e9)
localvars = list(locals().items())
for var, obj in localvars:
    if(sys.getsizeof(obj)/1e9 > 0.5):
        print(var, sys.getsizeof(obj)/1e9)

# reassign these two variables, because to_stata doesn't allowing float16
df_fin["age"] = df_fin["age"].astype('float32')
df_fin["educr"] = df_fin["educr"].astype('float32')
df_fin.to_stata("../mid/microcausepre.dta")