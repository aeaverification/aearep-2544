#delimit cr
clear all

* import decennial data for educational attainment over time
* downloaded from https://www.census.gov/data/tables/2000/dec/phc-t-41.html
import excel using ../raw/table06a.xls, cellrange(A10:H72) firstrow allstring clear
rename (B C D E F G H) (collegeshare1940 collegeshare1950 collegeshare1960 collegeshare1970 collegeshare1980 collegeshare1990 collegeshare2000)

* there is some data that we drop unrelated to states
gen id = _n
drop if id<=11

* use statastates to get fips codes
rename Geographicarea StateName
replace StateName =subinstr(StateName, ".", "",.)
statastates, name(StateName)
assert _merge==3
drop _merge
order state_abbrev state_fips, after(StateName)
drop state_abbrev StateName id

* drop Alaska and Hawaii
drop if inlist(state_fips, 2, 15)

* turn numeric, and then reshape so that year is a variable
destring _all, replace
reshape long collegeshare, i(state_fips) j(year)
tempfile before2010
save `before2010', replace

* NOTE RE BELOW; I know using a for loop is exponentially more efficient
* however, a number of the Excel files are slightly different, to the point that it's worth just loading in one by one
* Note that these tables are downloaded manually from data.census.gov, table S1501
import delimited using ../raw/EducationalAttainment2010-2018/ACSST1Y2010.S1501_data_with_overlays_2020-08-18T165800.csv, varnames(2) rowrange(2:53) colrange(2:32) stringcols(_all) clear
* exclude the first row and the first column, turn into all strings so that no data gets parsed weirdly
rename (geographicareaname totalestimatepercentbachelorsdeg) (StateName collegeshare)
* there are a lot of columns but we just need this one
keep StateName collegeshare
gen year = 2010
* pull out fips codes using statename
statastates, name(StateName)
assert _merge == 3
drop _merge state_abbrev StateName
order state_fips, before(collegeshare)
order year, after(state_fips)
* drop alaska and hawaii
drop if inlist(state_fips, 2, 15)
* turn into numeric
destring _all, replace
tempfile 2010
save `2010', replace

* below is very similar as the above, with small changes noted
import delimited using ../raw/EducationalAttainment2010-2018/ACSST1Y2011.S1501_data_with_overlays_2020-08-18T165800.csv, rowrange(2:53) colrange(2:32) stringcols(_all) varnames(2) clear
rename (geographicareaname totalestimatepercentbachelorsdeg) (StateName collegeshare)
keep StateName collegeshare
gen year = 2011
statastates, name(StateName)
assert _merge == 3
drop _merge state_abbrev StateName
order state_fips, before(collegeshare)
order year, after(state_fips)
drop if inlist(state_fips, 2, 15)
destring _all, replace
tempfile 2011
save `2011', replace

import delimited using ../raw/EducationalAttainment2010-2018/ACSST1Y2012.S1501_data_with_overlays_2020-08-18T165800.csv, rowrange(2:53) colrange(2:32) stringcols(_all) varnames(2) clear
rename (geographicareaname totalestimatepercentbachelorsdeg) (StateName collegeshare)
keep StateName collegeshare
gen year = 2012
statastates, name(StateName)
assert _merge == 3
drop _merge state_abbrev StateName
order state_fips, before(collegeshare)
order year, after(state_fips)
drop if inlist(state_fips, 2, 15)
destring _all, replace
tempfile 2012
save `2012', replace

import delimited using ../raw/EducationalAttainment2010-2018/ACSST1Y2013.S1501_data_with_overlays_2020-08-18T165800.csv, rowrange(2:53) colrange(2:32) stringcols(_all) varnames(2) clear
rename (geographicareaname totalestimatepercentbachelorsdeg) (StateName collegeshare)
keep StateName collegeshare
gen year = 2013
statastates, name(StateName)
assert _merge == 3
drop _merge state_abbrev StateName
order state_fips, before(collegeshare)
order year, after(state_fips)
drop if inlist(state_fips, 2, 15)
destring _all, replace
tempfile 2013
save `2013', replace

import delimited using ../raw/EducationalAttainment2010-2018/ACSST1Y2014.S1501_data_with_overlays_2020-08-18T165800.csv, rowrange(2:53) colrange(2:32) stringcols(_all) varnames(2) clear
rename (geographicareaname totalestimatepercentbachelorsdeg) (StateName collegeshare)
keep StateName collegeshare
gen year = 2014
statastates, name(StateName)
assert _merge == 3
drop _merge state_abbrev StateName
order state_fips, before(collegeshare)
order year, after(state_fips)
drop if inlist(state_fips, 2, 15)
destring _all, replace
tempfile 2014
save `2014', replace

import delimited using ../raw/EducationalAttainment2010-2018/ACSST1Y2015.S1501_data_with_overlays_2020-08-18T165800.csv, rowrange(2:53) colrange(2:32) stringcols(_all) varnames(2) clear
* need to specifically calculate collegeshare here
destring v24 v26 totalestimatepopulation25yearsan, replace
gen collegeshare = 100*(v24 + v26)/(totalestimatepopulation25yearsan)
rename (geographicareaname) (StateName)
keep StateName collegeshare
gen year = 2015
statastates, name(StateName)
assert _merge == 3
drop _merge state_abbrev StateName
order state_fips, before(collegeshare)
order year, after(state_fips)
drop if inlist(state_fips, 2, 15)
destring _all, replace
tempfile 2015
save `2015', replace

import delimited using ../raw/EducationalAttainment2010-2018/ACSST1Y2016.S1501_data_with_overlays_2020-08-18T165800.csv, rowrange(2:53) colrange(2:32) stringcols(_all) varnames(2) clear
* need to specifically calculate college share
destring v24 v26 totalestimatepopulation25yearsan, replace
gen collegeshare = 100*(v24 + v26)/(totalestimatepopulation25yearsan)
rename (geographicareaname) (StateName)
keep StateName collegeshare
gen year = 2016
statastates, name(StateName)
assert _merge == 3
drop _merge state_abbrev StateName
order state_fips, before(collegeshare)
order year, after(state_fips)
drop if inlist(state_fips, 2, 15)
destring _all, replace
tempfile 2016
save `2016', replace

import delimited using ../raw/EducationalAttainment2010-2018/ACSST1Y2017.S1501_data_with_overlays_2020-08-18T165800.csv, rowrange(2:53) colrange(2:32) stringcols(_all) varnames(2) clear
* need to specifically calculate collegeshare
destring v24 v26 totalestimatepopulation25yearsan, replace
gen collegeshare = 100*(v24 + v26)/(totalestimatepopulation25yearsan)
rename (geographicareaname) (StateName)
keep StateName collegeshare
gen year = 2017
statastates, name(StateName)
assert _merge == 3
drop _merge state_abbrev StateName
order state_fips, before(collegeshare)
order year, after(state_fips)
drop if inlist(state_fips, 2, 15)
destring _all, replace
tempfile 2017
save `2017', replace

*merge all of the files together
use `before2010', clear
append using `2010'
append using `2011'
append using `2012'
append using `2013'
append using `2014'
append using `2015'
append using `2016'
append using `2017'

* first, save a version without interpolation
rename state_fips fipstate
tempfile withoutip
save `withoutip', replace
save ../mid/statecollegesharenotinterpolated, replace
keep fipstate
duplicates drop fipstate, force
* expand out and interpolate the college share
expand 78
bysort fipstate: gen year = 1939 + _n
sort fipstate year
merge 1:1 fipstate year using `withoutip', assert(1 3) nogenerate
by fipstate: mipolate collegeshare year, gen(collegeshareip)
drop collegeshare
rename collegeshareip collegeshare
label var collegeshare "college share, census + acs"
save ../mid/statecollegeshareinterpolated, replace
