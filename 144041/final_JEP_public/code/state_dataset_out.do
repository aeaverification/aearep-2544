clear all
pause on
set rmsg on 
set scheme s1color

* population, broken down by education, 25-64
use ../mid/educstaterace, clear
*keep if year >=1992
drop if inlist(fipstate, 2, 15) 
gen educ2 = 1
replace educ2 = 2 if educ == 3
drop educ
rename educ2 educ
gcollapse (sum) pop, by(fipstate year educ age4)
tempfile collegehspops
save `collegehspops', replace

* pooulation, 25-64, flat
use ../mid/educstaterace, clear
*keep if year >=1992
drop if inlist(fipstate, 2, 15) 
gen educ2 = 1
replace educ2 = 2 if educ == 3
drop educ
rename educ2 educ
gcollapse (sum) pop, by(fipstate year age4)
tempfile pops
save `pops', replace

* population, all age groups
use ../mid/seerconfips, clear 
*keep if year >=1992
gen fipstate = int(confips / 1000)
drop if inlist(fipstate, 2, 15) 
gen agecat = 0
replace agecat = 1 if ageseer == 1
replace agecat = 2 if inlist(ageseer, 2, 3)
replace agecat = 3 if inlist(ageseer, 4, 5)
replace agecat = 4 if inlist(ageseer, 6, 7)
replace agecat = 5 if inlist(ageseer, 8, 9)
replace agecat = 6 if inlist(ageseer, 10, 11)
replace agecat = 7 if inlist(ageseer, 12, 13)
replace agecat = 8 if inlist(ageseer, 14, 15)
replace agecat = 9 if inlist(ageseer, 16, 17)
replace agecat = 10 if ageseer == 18
drop if year > 2017
preserve
gcollapse (sum) pop, by(fipstate year agecat)
tempfile popallages
save `popallages', replace
restore
preserve
* population, black, all age groups (imported this way for ease of code later)
keep if race == 1
gcollapse (sum) pop, by(fipstate year agecat)
tempfile popallages_b
save `popallages_b', replace
restore
preserve
* population, hisp, all age groups (imported this way for ease of code later)
keep if hisp == 1
gcollapse (sum) pop, by(fipstate year agecat)
tempfile popallages_h
save `popallages_h', replace
restore
preserve
* population, wnh, all age groups (imported this way for ease of code later)
keep if race == 0 & hisp == 0
gcollapse (sum) pop, by(fipstate year agecat)
tempfile popallages_wnh
save `popallages_wnh', replace
restore


* Comparability ratios for crossing 1998, for non all-cause mortality
import excel using ../raw/comparability_ratios_top_causes_age, clear firstrow
rename A agecat
tempfile comparability_ratios
save `comparability_ratios', replace

use ../mid/imputeeducation,  clear

replace age4 = age4 + 3
rename age4 agecat
gcollapse (sum) deaths poison suicide cirrhosis circulatory diabetes cerebrovasc lowerresp cancer flupneum, by(year fipstate agecat educ)

* adjust causes of death based on comparability ratios
merge m:1 agecat using `comparability_ratios', assert(2 3) keep(3) nogenerate
foreach var in deaths poison suicide cirrhosis circulatory diabetes cerebrovasc lowerresp cancer flupneum{
replace `var' = `var' * CR_`var' if year <=1998
}

* create our final causes of death
gen despair = poison + suicide + cirrhosis
gen other = deaths - despair
drop poison suicide cirrhosis

replace agecat = agecat - 3
rename agecat age4

tempfile withcomparabilityratios
save `withcomparabilityratios', replace

gcollapse (sum) deaths circulatory despair other diabetes cerebrovasc lowerresp cancer flupneum, by(year fipstate age4 educ)

* merge on population, check our merge, add 0 deaths for smaller cells
fmerge 1:1 year fipstate age4 educ using `collegehspops', assert(2 3) keep(2 3)
keep if year >=1992
drop if inlist(fipstate, 13, 40, 44, 46)
replace pop = 0 if _merge == 1
foreach v of varlist deaths circulatory despair other diabetes cerebrovasc lowerresp cancer flupneum  {
	replace `v' = 0 if _merge == 2 
}
foreach v of varlist * {
	di "`v'"
	assert !mi(`v')
}

* merge on population weights for age-adjustment
fmerge m:1 age4 using ../mid/agestd_weights_nvsr_educ, assert(3) noreport nogenerate 

preserve
keep if educ == 1

* create raw mortality rates for age groups
gen mr_all = (deaths/ (pop / 100000)) 
replace mr_all = 100000 if deaths>pop 
replace mr_all = 0 if deaths==0 & pop == 0
foreach var of varlist circulatory despair other diabetes cerebrovasc lowerresp cancer flupneum {
	gen mr_`var' = (`var'/ (pop / 100000)) 
	replace mr_`var' = 100000 if `var'>pop 
	replace mr_`var' = 0 if `var'==0 & pop == 0
}

* collapse, taking means and weighting by wt to age adjust the mortality rates.  Keep the raw death counts 
gcollapse (rawsum) pop deaths circulatory despair other diabetes cerebrovasc lowerresp cancer flupneum (mean) mr_* [aw=wt], by(year fipstate) fast
hashsort fipstate year
		
gen mr_all_crude = (deaths/ (pop / 100000)) 
	replace mr_all_crude = 100000 if deaths>pop 
	replace mr_all_crude = 0 if deaths==0 & pop == 0
foreach var of varlist circulatory despair other diabetes cerebrovasc lowerresp cancer flupneum {
	gen mr_`var'_crude = (`var'/ (pop / 100000)) 
	replace mr_`var'_crude = 100000 if `var'>pop 
	replace mr_`var'_crude = 0 if `var'==0 & pop == 0
}

rename (pop deaths mr_all mr_all_crude) (pop_NC deaths_NC mr_all_NC mr_all_crude_NC)
foreach var in circulatory despair other diabetes cerebrovasc lowerresp cancer flupneum {
	rename (`var' mr_`var' mr_`var'_crude) (`var'_NC mr_`var'_NC mr_`var'_crude_NC)
}
hashsort fipstate year
tempfile noncollege
save `noncollege'
restore

keep if educ == 2
gen mr_all = (deaths/ (pop / 100000)) 
replace mr_all = 100000 if deaths>pop 
replace mr_all = 0 if deaths==0 & pop == 0
foreach var of varlist circulatory despair other diabetes cerebrovasc lowerresp cancer flupneum {
	gen mr_`var' = (`var'/ (pop / 100000)) 
	replace mr_`var' = 100000 if `var'>pop 
	replace mr_`var' = 0 if `var'==0 & pop == 0
}

gcollapse (rawsum) pop deaths circulatory despair other diabetes cerebrovasc lowerresp cancer flupneum (mean) mr_* [aw=wt], by(year fipstate) fast
hashsort fipstate year

		
gen mr_all_crude = (deaths/ (pop / 100000)) 
	replace mr_all_crude = 100000 if deaths>pop 
	replace mr_all_crude = 0 if deaths==0 & pop == 0
foreach var of varlist circulatory despair other diabetes cerebrovasc lowerresp cancer flupneum {
	gen mr_`var'_crude = (`var'/ (pop / 100000)) 
	replace mr_`var'_crude = 100000 if `var'>pop 
	replace mr_`var'_crude = 0 if `var'==0 & pop == 0
}

rename (pop deaths mr_all mr_all_crude) (pop_CD deaths_CD mr_all_CD mr_all_crude_CD)
foreach var in circulatory despair other diabetes cerebrovasc lowerresp cancer flupneum {
	rename (`var' mr_`var' mr_`var'_crude) (`var'_CD mr_`var'_CD mr_`var'_crude_CD)
}
tempfile college
save `college'

use `noncollege', clear
hashsort fipstate year
merge 1:1 fipstate year using `college', assert(3) keep(3) nogenerate
gen age_group = 3
tempfile educationmortalities
save `educationmortalities', replace

** mortality rates, all age groups, with race, by cause, WITHOUT education
foreach race in "black" "hispanic" "wnh" "allrace"{
use ../mid/priorstateimpute, clear
if("`race'" == "black"){
    keep if race == 1
    local stub = "_b"
}
if("`race'" == "wnh"){
    keep if race == 0 & hisp == 0
    local stub = "_wnh"
}
if("`race'" == "hispanic"){
    keep if hisp == 1
    local stub = "_h"
}
if("`race'" == "allrace"){
   local stub = ""
}
* create ten age categories, will be using all
gen count = 1
gen byte agecat = . 
replace agecat = 0 if age==0
replace agecat = 1 if inrange(age, 1, 4)
replace agecat = 2 if inrange(age, 5, 14)
replace agecat = 3 if inrange(age, 15, 24)
replace agecat = 4 if inrange(age, 25, 34)
replace agecat = 5 if inrange(age, 35, 44)
replace agecat = 6 if inrange(age, 45, 54)
replace agecat = 7 if inrange(age, 55, 64)
replace agecat = 8 if inrange(age, 65, 74)
replace agecat = 9 if inrange(age, 75, 84)
replace agecat = 10 if age>=85
drop if mi(agecat)
drop age
cap drop death count
gen deaths = 1
tempfile dataset
save `dataset', replace

* adjust non all-cause deaths by comparaibility ratios
use `dataset', clear
gcollapse (sum) deaths poison suicide cirrhosis circulatory diabetes cerebrovasc lowerresp cancer flupneum, by(year fipstate agecat)
merge m:1 agecat using `comparability_ratios', assert(3) nogenerate
foreach var in deaths poison suicide cirrhosis circulatory diabetes cerebrovasc lowerresp cancer flupneum{
replace `var' = `var' * CR_`var' if year <=1998
}

* define the rest of our causes of death
gen despair = poison + suicide + cirrhosis
gen other = deaths - despair
drop poison suicide cirrhosis

drop CR_*

* merge on relevant populations, do any small corrections in case of 0 population or 0 death
fmerge 1:1 year fipstate agecat using `popallages`stub'', assert(1 2 3)
keep if year >=1990
replace pop = 0 if _merge == 1
foreach v of varlist deaths circulatory despair other diabetes cerebrovasc lowerresp cancer flupneum  {
	replace `v' = 0 if _merge == 2 
}
foreach v of varlist * {
	di "`v'"
	assert !mi(`v')
}

rename agecat age3
* merge on weights to age adjust
fmerge m:1 age3 using ../mid/agestd_weights_nvsr, noreport nogenerate 
rename age3 agecat
tempfile beforeagegroups
save `beforeagegroups', replace
* loop over the different age groups we want to consider
foreach agegroup in "infant" "young" "mid" "old" "all"{
if("`agegroup'"=="infant"){
	local lower_lim = 0
	local upper_lim = 1
	local age_group_num = 1
}
if("`agegroup'"=="young"){
	local lower_lim = 2
	local upper_lim = 3
	local age_group_num = 2
}
if("`agegroup'"=="mid"){
	local lower_lim = 4
	local upper_lim = 7
	local age_group_num = 3
}
if("`agegroup'"=="old"){
	local lower_lim = 8
	local upper_lim = 10
	local age_group_num = 4
}
if("`agegroup'"=="all"){
	local lower_lim = 0
	local upper_lim = 10
	local age_group_num = 5
}
use `beforeagegroups', clear
* restrict to just the relevant age groups
keep if agecat>=`lower_lim' & agecat<=`upper_lim'

* calculate crude age-group mortalities
gen mr_all = (deaths/ (pop / 100000)) 
replace mr_all = 100000 if deaths>pop 
replace mr_all = 0 if deaths==0 & pop == 0
foreach var of varlist circulatory despair other diabetes cerebrovasc lowerresp cancer flupneum {
	gen mr_`var' = (`var'/ (pop / 100000)) 
	replace mr_`var' = 100000 if `var'>pop 
	replace mr_`var' = 0 if `var'==0 & pop == 0
}

* collapse, producing the age-adjusted mortality rates based on wt, and also preserving the number of deaths
gcollapse (rawsum) pop deaths circulatory despair other diabetes cerebrovasc lowerresp cancer flupneum (mean) mr_* [aw=wt], by(year fipstate) fast
hashsort fipstate year

gen age_group = `age_group_num'
	
gen mr_all_crude = (deaths/ (pop / 100000)) 
	replace mr_all_crude = 100000 if deaths>pop 
	replace mr_all_crude = 0 if deaths==0 & pop == 0
foreach var of varlist circulatory despair other diabetes cerebrovasc lowerresp cancer flupneum {
	gen mr_`var'_crude = (`var'/ (pop / 100000)) 
	replace mr_`var'_crude = 100000 if `var'>pop 
	replace mr_`var'_crude = 0 if `var'==0 & pop == 0
}
tempfile `agegroup'
save ``agegroup'', replace
}

* append the age group datasets together
use `infant', clear
append using `young'
append using `mid'
append using `old'
append using `all'

* rename variables based on race
rename pop pop`stub'
rename deaths deaths`stub'
rename mr_all mr_all`stub'
rename mr_all_crude mr_all_crude`stub'
foreach cause in circulatory despair other diabetes cerebrovasc lowerresp cancer flupneum{
	rename `cause' `cause'`stub'
	rename mr_`cause' mr_`cause'`stub'
	rename mr_`cause'_crude mr_`cause'_crude`stub'
}
tempfile `race'
save ``race'', replace

}

* start with all race, cause, age group, no education mortalities
use `allrace', clear
label define age_group 1 "Child (Ages 0-4)" 2 "Young Person (Ages 5-24)" 3 "Midlife (Ages 25-64)" 4 "Older (Age 65+)" 5 "All Ages"
label values age_group age_group

* merge on black, all cause, age group, no education mortalities
merge 1:1 fipstate year age_group using `black', assert(1 3) nogenerate
* merge on hispanic, all cause, age group, no education mortalities
merge 1:1 fipstate year age_group using `hispanic', assert(1 3) nogenerate
* merge on wnh, all cause, age group, no education mortalities
merge 1:1 fipstate year age_group using `wnh', assert(1 3) nogenerate
* merge on all race, all cause, midlife, education mortalities
merge 1:1 fipstate year age_group using `educationmortalities', assert(1 3) nogenerate
* merge on covariates
merge m:1 fipstate year using ../mid/state_covariates, assert(2 3) keep(3) nogenerate
* make a few small corrections
replace state_abbrev = "AR" if fipstate == 5
replace state_abbrev = "WY" if fipstate==56
drop if fipstate == 11

order age_group, after(fipstate)
order pop_b pop_h pop_wnh pop_NC pop_CD, after(pop)
order deaths_NC-flupneum_NC, after(flupneum)
order deaths_CD-flupneum_CD, after(flupneum)
order deaths_wnh-flupneum_wnh, after(flupneum)
order deaths_h-flupneum_h, after(flupneum)
order deaths_b-flupneum_b, after(flupneum)
label var age_group "age group"
label var pop "year-state-agegroup population"
label var pop_b "year-state-agegroup black population"
label var pop_h "year-state-agegroup hispanic population"
label var pop_wnh "year-state-agegroup white non-hispanic population"
label var pop_NC "year-state-agegroup population without a college degree"
label var pop_CD "year-state-agegroup population with a college degree"

foreach str in "" "_b" "_h" "_wnh" "_NC" "_CD"{
if("`str'"==""){
local addon = ""
}
if("`str'"=="_b"){
local addon = ", black population"
}
if("`str'"=="_h"){
local addon = ", hispanic population"
}
if("`str'"=="_wnh"){
local addon = ", white non-hispanic population"
}
if("`str'"=="_CD"){
local addon = ", adults with a college degree"
}
if("`str'"=="_NC"){
local addon = ", adults without a college degree"
}

label var deaths`str' "deaths due to all cause`addon'"
label var circulatory`str' "deaths due to diseases of the heart`addon'"
label var despair`str' "deaths of despair`addon'"
label var other`str'   "deaths due to causes other than deaths of despair`addon'"
label var diabetes`str' "deaths due to diabetes mellitus`addon'"
label var cerebrovasc`str' "deaths due to cerebrovascular diseases`addon'"
label var lowerresp`str' "deaths due to chronic lower respiratory diseases`addon'"
label var flupneum`str' "deaths due to influenza or pneumonia`addon'"
label var cancer`str' "deaths due to cancer`addon'"
}

foreach str in "" "_b" "_h" "_wnh" "_NC" "_CD"{
foreach strtwo in "_crude" ""{
if("`str'"==""){
local addon = ""
}
if("`str'"=="_b"){
local addon = ", black population"
}
if("`str'"=="_h"){
local addon = ", hispanic population"
}
if("`str'"=="_wnh"){
local addon = ", white non-hispanic population"
}
if("`str'"=="_CD"){
local addon = ", adults with a college degree"
}
if("`str'"=="_NC"){
local addon = ", adults without a college degree"
}
if("`strtwo'"=="_crude"){
local addontype = "crude"
}
if("`strtwo'"==""){
local addontype = "age-adjusted"
}

label var mr_all`strtwo'`str' "all-cause `addontype' mortality rate`addon'"
label var mr_despair`strtwo'`str' "despair `addontype' mortality rate`addon'"
label var mr_other`strtwo'`str' "other `addontype' mortality rate`addon'"
label var mr_diabetes`strtwo'`str' "diabetes `addontype' mortality rate`addon'"
label var mr_cerebrovasc`strtwo'`str' "cerebrovascular `addontype' mortality rate`addon'"
label var mr_lowerresp`strtwo'`str' "chronic lower respiratory `addontype' mortality rate`addon'"
label var mr_cancer`strtwo'`str' "cancer `addontype' mortality rate`addon'"
label var mr_circulatory`strtwo'`str' "heart disease `addontype' mortality rate`addon'"
label var mr_flupneum`strtwo'`str' "influenza and pneumonia `addontype' mortality rate`addon'"
}
}


save ../final_dataset, replace

use ../final_dataset, clear
* redact the final dataset
foreach str in "" "_b" "_h" "_wnh" "_NC" "_CD"{
foreach cause in circulatory despair other diabetes cerebrovasc lowerresp cancer flupneum{
disp "`cause'"
disp "`str'"
replace mr_`cause'_crude`str' = . if `cause'`str' < 10
replace mr_`cause'`str' = . if `cause'`str' < 10
replace `cause'`str' = . if `cause'`str' < 10
}
}

foreach str in "" "_b" "_h" "_wnh" "_NC" "_CD"{
disp "`str'"
replace mr_all_crude`str' = . if deaths`str' < 10
replace mr_all`str' = . if deaths`str' < 10
replace deaths`str' = . if deaths`str' < 10
}

keep if age_group == 3

drop *_b
drop *_wnh
drop *_h
drop circulatory_CD            -flupneum_CD
drop circulatory_NC            -flupneum_NC
drop mr_circulatory_CD         -mr_flupneum_CD
drop mr_circulatory_NC         -mr_flupneum_NC
drop mr_circulatory_crude_CD   -mr_flupneum_crude_CD
drop mr_circulatory_crude_NC   -mr_flupneum_crude_NC
drop mr_circulatory_crude mr_despair_crude mr_other_crude mr_cerebrovasc_crude mr_lowerresp_crude mr_cancer_crude

drop *diabetes*
drop *flupneum*
drop *overweight*
drop *inactive*
drop *binge*
cap drop per_capita_medicaid-weightedpopdensitytract Gini


save ../final_dataset_redacted, replace
