clear all
pause on
set rmsg on 
set scheme s1color

**************************
*read in pop data 1990-2018; downloaded https://seer.cancer.gov/popdata/download.html
*this time we will keep the data long and use the same varnames and categories
*as the death data 
infix int year 1-4 byte fipstate 7-8 int fipscty 9-11 byte race 14 byte hisp 15 ///
byte sex 16 byte age 17-18 pop 19-26 using ../raw/us.1990_2019.singleages.adjusted.txt, clear 
assert inlist(hisp, 0, 1), fast 
*puts categories exactly same as in death data: 0=white, 1=black, 2=native, 3=asian/pac.islander
replace race = race - 1 
assert inlist(race, 0, 1, 2, 3), fast 
*puts genders same as in death data: 0=male, 1=female
replace sex = sex - 1
assert inlist(sex, 0, 1), fast 
assert inrange(age, 0, 85), fast 
drop if inlist(fipstate, 2, 15, 99)

*i show which 9xx county codes correspond to pre-alteration counties
preserve
keep if fipstate == 8 & inlist(fipscty, 1, 13, 14, 59, 123, 911, 912, 913, 914)
keep year fipstate fipscty pop
gcollapse (sum) pop, by(year fipstate fipscty) fast 
reshape wide pop, i(year) j(fipscty)
line pop* year, lc(cranberry orange purple green navy cranberry orange green navy) lp(solid solid solid solid solid dash dash dash dash)
restore
preserve
keep if fipstate == 4 & inlist(fipscty, 12, 27, 910)
keep year fipstate fipscty pop
gcollapse (sum) pop, by(year fipstate fipscty) fast 
reshape wide pop, i(year) j(fipscty)
line pop* year, lc(cranberry orange purple green navy cranberry orange green navy) lp(solid solid solid solid solid dash dash dash dash)
restore
*and restore those codes 
replace fipscty = 1 if fipstate == 8 & fipscty == 911
replace fipscty = 13 if fipstate == 8 & fipscty == 912
replace fipscty = 59 if fipstate == 8 & fipscty == 913
replace fipscty = 123 if fipstate == 8 & fipscty == 914
replace fipscty = 27 if fipstate == 4 & fipscty == 910
replace fipscty = 102 if fipstate == 46 & fipscty == 113

*I show that the only official 2019 county that is not found in the seer data
*is bedford county 51019,
*and that there is only one county in the seer data that is not found in the 
*official county list 
preserve
gcollapse (sum) pop, by(year fipstate fipscty) fast 
fmerge m:1 fipstate fipscty using ../mid/counties2019, keepusing(name) 
tab fipstate if _merge == 2
list if _merge == 2 & fipstate == 51 
assert fipscty == 19 if _merge == 2 & fipstate == 51
assert fipscty == 917 if _merge == 1 & fipstate == 51 
restore
*I restore this code 
replace fipscty = 19 if fipstate == 51 & fipscty == 917

*now I can convert to confips 
rename fipscty oldfipscty
fmerge m:1 fipstate oldfipscty using ../confips/confips, ///
assert(2 3) keep(3) noreport nogenerate keepusing(confips)
gcollapse (sum) pop, by(year confips race hisp sex age) fast 

label define race 0 "white" 1 "black" 2 "native" 3 "asian / pacific islander" 
label values race race
label define hisp 0 "not hispanic" 1 "hispanic" 
label values hisp hisp
label define sex 0 "male" 1 "female"
label values sex sex 
label define age 85 "85+"
label values age age 
label variable age "85=85+"
label variable pop ""
label data "single-age SEER population data by confips/year/race/hisp/age"
compress
save ../mid/seerconfips_singleage, replace 

gen ageseer = .
replace ageseer = 0 if age == 0
local groups 1   2   3     4     5     6     7     8     9     10    11    12    13    14    15    16    17    18
local ranges 1-4 5-9 10-14 15-19 20-24 25-29 30-34 35-39 40-44 45-49 50-54 55-59 60-64 65-69 70-74 75-79 80-84 85+ 
forvalues g = 1 / 17 {
	local r : word `g' of `ranges'
	local d `=strpos("`r'", "-")'
	local r1 `=substr("`r'", 1, `d'-1)'
	local r2 `=substr("`r'", `d'+1, 2)'
	di "`g' ; `r1' ; `r2'"
	replace ageseer = `g' if inrange(age, `r1', `r2')
	label define ageseer `g' "`r'", add
}
replace ageseer = 18 if age == 85
label define ageseer 18 "85+", add
assert !mi(ageseer), fast 
label variable ageseer "categorical age"
label values ageseer ageseer 

gcollapse (sum) pop, by(year confips race hisp sex ageseer) fast 
label variable pop ""
label variable age ""
label data "categorical-age SEER population data by confips/year/race/hisp/age"
compress
save ../mid/seerconfips, replace 

*now we organize ages by cohort 
use ../mid/seerconfips_singleage, clear
drop if age == 85 // 85+ assigned 85, cannot determine year of birth
gen int birthyear = year - age
save ../mid/seer_cohort, replace 
