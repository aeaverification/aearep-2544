import xlrd, openpyxl, gc
# run garbage collection to try to perhaps clean out memory
gc.collect()

# read in comparability ratios, reformat
workbook = xlrd.open_workbook("../raw/Comparability_Ratio_tables.xls")
age_crs = workbook.sheet_by_name("Table 4")
CRs = {}
CRs["deaths"] = [1 for i in range(11)]
CRs["poison"] = [1 for i in range(11)]
stubs = ["circulatory", "cancer", "cerebrovasc", "lowerresp", "accidents", "flupneum", "diabetes",  "hiv", "suicide", "cirrhosis", "nephritis", "septicemia", "alzheimers", "assault", "atherosclerosis"]
columns = [5, 8, 11, 14, 17, 20, 23, 26, 29, 32, 35]
for i in stubs:
    CRs[i] = []
    
for cur_row in range(6, 21):
    for cur_col in columns:
        cell = age_crs.cell(cur_row, cur_col).value
        if cell == "*" or cell == ". . .":
            final_value = 1
        else:
            final_value = float(cell)
        CRs[stubs[cur_row - 6]].append(final_value)
stubsmanual = ["deaths", "poison"]
stubsmanual.extend(stubs)

# put into new Excel file to be easily readable into Stata
book = openpyxl.Workbook()
mysheet = book.active
mycell= mysheet.cell(row=1, column=1)  
mycell = "Age_comp"
for row in range(2, 13):
    mysheet.cell(row=row, column=1, value = row-2)
    
for col in range(len(stubsmanual)):
    mysheet.cell(row=1, column=col+2, value = "CR_" + stubsmanual[col]) 

for row in range(2, 13):
    for col in range(len(stubsmanual)):
        mysheet.cell(row=row, column=col+2, value = CRs[stubsmanual[col]][row - 2]) 

book.save("../raw/comparability_ratios_top_causes_age.xlsx")