clear all
pause on
set rmsg on 
set scheme s1color

cd ../raw/

* download share of poverty
copy "https://www2.census.gov/programs-surveys/cps/tables/time-series/historical-poverty-people/hstpov21.xls" hstpov21.xls, replace

* download 2019 counties
copy "https://www2.census.gov/geo/docs/maps-data/data/gazetteer/2019_Gazetteer/2019_Gaz_counties_national.zip" 2019_Gaz_counties_national.zip, replace
unzipfile 2019_Gaz_counties_national.zip, replace

* download Comparability Ratio Tables
copy "https://ftp.cdc.gov/pub/health_statistics/NCHS/Datasets/Comparability/icd9_icd10/Comparability_Ratio_tables.xls" Comparability_Ratio_tables.xls, replace

cd icd_gem_crosswalks

* download icd general equivalence mappings
copy "https://www.cms.gov/Medicare/Coding/ICD10/Downloads/2018-ICD-10-CM-General-Equivalence-Mappings.zip" gem.zip, replace
unzipfile gem.zip, replace

cd ..

cd brfss

forvalues y = 2011 / 2019 {
	copy "https://www.cdc.gov/brfss/annual_data/`y'/files/LLCP`y'XPT.zip" LLCP`y'XPT.zip, replace
	unzipfile LLCP`y'XPT.zip, replace
}

forvalues yy = 0 / 10 {
	if `yy' < 10 local yy 0`yy'
	copy "https://www.cdc.gov/brfss/annual_data/20`yy'/files/CDBRFS`yy'XPT.zip" CDBRFS`yy'XPT.zip, replace
	unzipfile CDBRFS`yy'XPT.zip, replace
}

forvalues yy = 90 / 99 {
	copy "https://www.cdc.gov/brfss/annual_data/19`yy'/files/CDBRFS`yy'XPT.zip" CDBRFS`yy'XPT.zip, replace
	unzipfile CDBRFS`yy'XPT.zip, replace
}

forvalues yy = 84 / 89 {
	copy "https://www.cdc.gov/brfss/annual_data/19`yy'/files/CDBRFS`yy'_XPT.zip" CDBRFS`yy'XPT.zip, replace
	unzipfile CDBRFS`yy'XPT.zip, replace
}

cd ..

* download new seer data
copy "https://seer.cancer.gov/popdata/yr1990_2019.19ages/us.1990_2019.19ages.adjusted.txt.gz" us.1990_2019.19ages.adjusted.txt.gz, replace
shell gunzip us.1990_2019.19ages.adjusted.txt.gz
copy "https://seer.cancer.gov/popdata/yr1990_2019.singleages/us.1990_2019.singleages.adjusted.txt.gz" us.1990_2019.singleages.adjusted.txt.gz, replace
shell gunzip us.1990_2019.singleages.adjusted.txt.gz

cd ../code
