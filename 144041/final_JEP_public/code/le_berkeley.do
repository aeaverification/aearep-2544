#delimit ;
clear all;
set linesize 180;
*******************************************
* LE_BERKELEY.DO -- Life expectancy US Mortality Project at Berkeley
*******************************************;

set more off;
set linesize 120;
clear all;

local prg le_berkeley;
cap log close;
log using ../logs/`prg'.log, replace;

local lc1 ""0 71 171"";
local lc2 cranberry;
local lc3 dkgreen;
local lc4 brown; 


***** Download Haver National Life Expectancy dataset;
*haver use USLE USLEWX USLEBX USLEH USLEB USLEW using C:\Haver\USECON.DAT, tvar(year) clear;
*d, f;
*tsset year, y;

*save ../mid/haver_national_le.dta, replace;
*clear;

***********************************
* ;

local states AL AK AZ AR CA 
             CO CT DE DC FL 
			 GA HI ID IL IN
			 IA KS KY LA ME 
			 MD MA MI MN MS 
			 MO MT NE NV NH 
			 NJ NM NY NC ND 
			 OH OK OR PA RI 
			 SC SD TN TX UT 
			 VT VA WA WV WI 
			 WY;
			 

* import raw data, all age groups;
* note that data is from US Mortality Data Base, https://usa.mortality.org/; 
* cannot download non-manually because of password requirement;
foreach x of local states {; 
  import delimited using "../raw/le_berkeley_states/States/`x'/`x'_bltper_1x1.txt", clear rowrange(3:) delimiter(tab) varnames(3);
  gen state = "`x'";
  keep if age == "0";
    
  ren ex le_berkeley;
  keep state year le_berkeley;
  
  save ../mid/`x'.dta, replace;
};
clear all;

foreach x of local states {; 
  append using ../mid/`x';
  erase ../mid/`x'.dta;
};

state2fips;

order fips state year;
sort fips state year;

save ../mid/temp, replace;


***********************************
* Population from Census (via Fred);
import excel using ../raw/population.xls, sheet("Annual") clear firstrow;
gen  year = year(DATE);
drop if year <1950;
drop DATE;

foreach x of local states {; 
  rename `x'POP POP`x';
};

greshape long POP, i(year) j(fipstate) string;
replace POP = POP*1000;
rename POP pop_census;
label var pop_census "Population from Census (via Fred)";

rename fipstate state;

keep if year >= 1959 & year <= 2020;
state2fips;

sort fips state year;
order fips state year;

merge 1:1 fips state year using ../mid/temp;
tab _merge;
drop _merge;

sort fips state year;

save ../mid/`prg', replace;



*****************************************
* Standard deviations of life expectancy;

use ../mid/`prg', clear;

summ le_berkeley ;
summ le_berkeley [aw=pop_census];

* Population weighted;
use ../mid/`prg'.dta, clear;
collapse (sd) sdle_wtd = le_berkeley [aw=pop_census], by(year);

tsset year;
save ../mid/sd_wtd, replace;
clear;

* Unweighted;
use ../mid/`prg'.dta, clear;
collapse (sd) sdle_unwtd = le_berkeley /* [aw=pop_census] */, by(year);

tsset year;
merge 1:1 year using ../mid/sd_wtd, assert(3);
drop _merge;
erase ../mid/sd_wtd.dta;
save ../mid/sd_two, replace;

* Unweighted without DC;
use ../mid/`prg'.dta, clear;
collapse (sd) sdle_unwtd_noDC = le_berkeley /* [aw=pop_census] */ if state ~= "DC", by(year);

tsset year;
merge 1:1 year using ../mid/sd_two, assert(3);
drop _merge;
erase ../mid/sd_two.dta;

la var sdle_wtd          "Weighted by Pop.";
la var sdle_unwtd        "Unweighted";
la var sdle_unwtd_noDC   "Unweighted w/o DC";

tsset year, y;

save ../mid/temp, replace;

* Add national life expectancy and save a year-by-year dataset;

use ../mid/haver_national_le.dta, clear;
merge 1:1 year using ../mid/temp;
keep if year >= 1959 & year <= 2020;
assert _merge == 3;
drop _merge;

order year sd* USLE;
save ../mid/`prg'_mean_sds, replace;

erase ../mid/temp.dta;

log close;
exit, clear;
