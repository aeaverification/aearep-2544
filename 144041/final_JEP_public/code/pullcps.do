clear all
pause on
set rmsg on 
set scheme s1color

** note that cps_00012 is a .dat file pulled from ipms, cps_00012.cbk summarizes the download
global import = 1
if ${import} == 1 {
	cd ../cps
	do cps_00012
	save cpsraw, replace
	cd ../code
}

use ../cps/cpsraw, clear 
drop if year > 2017

* generate new race, age, sex, hispan variables to match definitions in seer
gen byte race2 = .
replace race2 = 0 if race == 100
replace race2 = 1 if race == 200
replace race2 = 2 if race == 300
replace race2 = 3 if inlist(race, 650, 651, 652) 
replace race2 = 9 if inrange(race, 700, 830)
assert !mi(race2), fast 
drop race 
rename race2 race
cap label drop race
label define race 0 "white" 1 "black" 3 "native" 4 "asian/pac islander" 9 "mixed/other"
label values race race 
label variable race "race, 4 levels"

assert !mi(age), fast 
replace age = 80 if age > 80 
cap label drop age 
label define age 80 "80+", modify
label values age age 

replace sex = sex - 1
assert !mi(sex), fast 
cap label drop sex
label define sex 0 "male" 1 "female"
label values sex sex 

gen byte hisp = hispan > 0
replace hisp = 9 if inlist(hispan, 901, 902)
assert !mi(hisp), fast 
drop hispan
cap label drop hisp
label define hisp 0 "no" 1 "yes" 9 "missing"
label values hisp hisp
label variable hisp "indicates hispanic"


tab educ
tab educ, nolabel


* educ3 is a 3-level educational variable
gen byte educ3 = .
replace educ3 = 1 if inrange(educ, 2, 73)
replace educ3 = 2 if inrange(educ, 80, 100) 
replace educ3 = 3 if inrange(educ, 110, 125)
replace educ3 = 9 if educ == 1 
assert !mi(educ3), fast 
drop educ
rename educ3 educ 
label define educ3 1 "HS or less" 2 "some college" 3 "bachelor's or more"
label values educ educ3
label variable educ "education, 3 levels"

compress
rename statefip fipstate
label data "CPS demographic microdata, 1990-2017"
save ../cps/cpsmid, replace 

* generate college share for those over the age of 25
use ../cps/cpsmid, clear
drop if educ == 9
drop if inlist(fipstate, 2, 15)
drop if age < 25
gen collegedegree = (educ == 3)
collapse (mean) collshare_cps = collegedegree [aw=wtfinl], by(year fipstate)
replace collshare_cps = 100*collshare_cps
sort year fipstate
label var collshare_cps "college share, cps"
save ../mid/statecollegeshare, replace

use ../cps/cpsmid, clear
drop if educ == 9 
drop if inlist(fipstate, 2, 15)
keep if inrange(age, 25, 64)
replace hisp = 0 if hisp == 9

glevelsof educ, local(educ)
foreach e of local educ {
	gen byte educ`e' = educ == `e'
}
drop educ

*case-deaton age groups 
gen byte age4 = .
replace age4 = 1 if inrange(age, 25, 34)
replace age4 = 2 if inrange(age, 35, 44)
replace age4 = 3 if inrange(age, 45, 54)
replace age4 = 4 if inrange(age, 55, 64) 
assert !mi(age4), fast
drop age 

gen byte race3 = .
replace race3 = 0 if !race & !hisp     // white non hispanic
replace race3 = 1 if race == 1 & !hisp // black non hispanic
replace race3 = 2 if !race & hisp      // white hispanic
replace race3 = 3 if race > 1 | race == 1 & hisp // other 
assert !mi(race3), fast
drop race
rename race3 race 
cap label drop race
label define race 0 "white nonhispanic" 1 "black non hispanic" 2 "white hispanic" 3 "other"

*gen year2 = int((year - 1990) / 4)

gcollapse (count) cellsize = educ1 (mean) educ* [aw=wtfinl], by(year fipstate race age4) fast 

* use seer data as reference population; cps is used to split it into educational group, based on cells above
preserve
use ../mid/seerconfips, clear 
drop if year > 2017
drop if ageseer < 6
gen fipstate = int(confips / 1000)

/*
replace ageseer = 17 if ageseer == 18 
label values ageseer ageseer
*/

*case-deaton age groups 
keep if inrange(ageseer, 6, 13)
gen byte age4 = . 
replace age4 = 1 if inlist(ageseer, 6, 7)
replace age4 = 2 if inlist(ageseer, 8, 9)
replace age4 = 3 if inlist(ageseer, 10, 11)
replace age4 = 4 if inlist(ageseer, 12, 13)
assert !mi(age4)
drop ageseer 

gen byte race3 = .
replace race3 = 0 if !race & !hisp     // white non hispanic
replace race3 = 1 if race == 1 & !hisp // black non hispanic
replace race3 = 2 if !race & hisp      // white hispanic
replace race3 = 3 if race > 1 | race == 1 & hisp // other 
assert !mi(race3), fast
drop race
rename race3 race 
cap label drop race
label define race 0 "white nonhispanic" 1 "black non hispanic" 2 "white hispanic" 3 "other"

gcollapse (sum) pop, by(year fipstate race age4) fast 

*gen year2 = int((year - 1990) / 4)
*gcollapse (mean) pop, by(year2 fipstate race agecd) fast 

tempfile denom
save `denom'
restore

fmerge 1:1 year fipstate race age4 using `denom'

foreach v of varlist year fipstate race age4 {
	di "`v'"
	tab `v' _merge
}

count if _merge == 2
gen mi = cellsize < 20 | _merge == 2
count if mi
hashsort fipstate age4 race year
by fipstate age4 race : ereplace mi = max(mi)
count if mi
drop mi _merge  

hashsort fipstate race age4 year
egen group = group(fipstate race age4)
tsset group year 

* smooths over the smallest window possible
foreach i of local educ {
	forvalues j = 1 / 5 {
		tssmooth ma maeduc`i'_`j' = educ`i', window(`j' 1 `j')	
	}
	forvalues j = 4(-1)1 {
		local k = `j'+1 
		assert !mi(maeduc`i'_`k')
		replace maeduc`i'_`j' = maeduc`i'_`k' if mi(maeduc`i'_`j') & !mi(maeduc`i'_`k') 
		drop maeduc`i'_`k'
	}
}

* makes sure that everything adds to 1
egen total = rowtotal(maeduc*)
foreach v of varlist maeduc* {
	replace `v' = `v' / total
}
drop total group 

* replaces educ with population counts
drop educ*
foreach i of local educ {
	gen educ`i' = maeduc`i' * pop
	drop maeduc`i'
}

gcollapse (sum) totalcellsize = cellsize, by(fipstate race age4) fast merge 
sum totalcellsize if year == 1990, detail
drop *cellsize

drop pop
renvars educ*, sub("educ" "pop")
reshape long pop, i(year fipstate race age4) j(educ)
label values educ educ 

drop if !pop
compress
hashsort year fipstat race age4 educ
label data "population by education and race, from CPS and SEER"
save ../mid/educstaterace, replace 

gcollapse (sum) pop, by(year fipstate age4 educ)
compress
label data "population by education, from CPS and SEER"
hashsort year fipstate age4 educ
save ../mid/educstate, replace 

