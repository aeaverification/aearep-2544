clear all

pause on
set rmsg on 
set scheme s1color

* source: confidential cdc microdata

********************************************************************************
*1989-1998 have largely the same data structure 
local dict1 byte month 55-56 dow 83 byte rectype 19 byte restat 20 byte pod 75 ///
byte fipstateocc 119-120 int fipsctyocc 121-123 byte fipstateres 124-125 int fipsctyres 126-128 ///
byte sex 59 byte raced 60-61 byte ageu 64 byte age 65-66 byte marstat 77 byte fipstatebirth 78-79 byte hisp 80-81 ///
int ind 85-87 int ind2 94-97 int occ 88-90 int occ2 98-101 byte educ89 52-53 educr 54 ///
str4 icd9 142-145 long icd9_282 146-150 int icd9_72 151-153 int icd9_61 154-156 int icd9_52 91-93 ///
int icd9_34 157-159 byte aut 84 byte poi 141 byte work 136 ///
byte entaxnum 160-161 str119 entaxcon 162-301 byte recaxnum 338-339 str recaxcon 341-440

*test file 
infix `dict1' using ../raw/MULT1990.AllCnty.txt, clear 

forvalues y = 1989 / 1998 {
	infix `dict1' using ../raw/MULT`y'.AllCnty.txt, clear
	tempfile `y'
	save ``y''
}

clear
gen int year = .
forvalues y = 1989 / 1998 {
	append using ``y''
	replace year = `y' if mi(year)
}

replace sex = sex - 1 // 0 = m, 1 = f

replace age = 0 if ageu > 1 & age != 9
replace age = 100 + age if ageu == 1
replace age = 999 if ageu == 9
assert inrange(age, 0, 199) | age == 999
drop ageu

gen byte race = .
replace race = 0 if raced == 1 // white
replace race = 1 if raced == 2 // black
replace race = 2 if raced == 3 // native american 
*asian = chinese, japanese, filipino, indian, korean, vietnamese
replace race = 3 if inlist(raced, 4, 5, 7, 18, 28, 48) 
*pacific islander = hawaiian, "other asian/pac.islander", samoan, guamanian, "other adian/pac.islander" for some areas, "other adian/pac.islander" for other areas 
replace race = 4 if inlist(raced, 6, 8, 38, 58, 68, 78) 
replace race = 9 if raced == 9 // other
assert !mi(race), fast 
drop raced 

gen byte hisp2 = .
replace hisp2 = 0 if !hisp
replace hisp2 = 1 if inrange(hisp, 1, 5)
replace hisp2 = 9 if hisp == 99
assert !mi(hisp2), fast 
drop hisp
rename hisp2 hisp

replace aut = 0 if aut == 2 
replace aut = 9 if aut == 8

replace fipstateres = 99 if !fipstateres

*We refer to the documentation (eg 1989, p.35) for the icd9 codes now - 
*all codes 800.0 - 999.9 are actually E codes with the E omitted. 
*Additionally, refer to the 1999 technical appendix for the 1999 Vital Statistics 
*(https://data.nber.org/mortality/2002/docs/techap99.pdf)
*on page 16, last paragraph: "For underlying cause of death, where both an 
*E code and another code were applicable, the E code was still used when the 
*other code was from [accidents, poisonings, and violence, 800.0-999.9].
*This tendency is also described in Paul Buecher's 2003 article in the 
*State Center for Health Statistics' Statistical Primer (no.16)
*(https://schs.dph.ncdhhs.gov/schs/pdf/16Web.pdf): 
*"To complicate things a little further, the “E” is omitted
*from the cause of death codes in the mortality data files
*produced by the SCHS for the years 1979 through 1998,
*when the ICD-9 was used for cause-of-death coding.
*Thus, whenever a code between 800.0 and 999.9 is
*encountered in the underlying cause of death field of
*these data files, it is always an E code. (The four-digit
*data values will be from 8000 to 9999.) In the more
*detailed 1979-1998 mortality files maintained at the
*SCHS, a “nature of injury flag” field is used to tell
*whether a code in the 800-999 range in the multiple
*cause-of-death portion of the record is an E-code or an
*N-code."
*While here he refers specifically to the state of North Carolina, the NVSS
*is state-reported and he describes perfectly what we believe to be the 
*case about the data.
*Lastly, the data documentation comes with control tables to check basic 
*tabulations against to ensure accuracy of the data. Looking at the part of 
*control table 1 shown on page 31 of the control table section (directly after
*the geography section) we see that in 1989 there were 5 deaths with E800.0 ad
*the underlying cause, 479 with E805.2, 352 with E810.0, and 9244 with E812.0. 
*we confirm that our data comes very close to these benchmarks:
foreach i in 8000 8052 8100 8120 {
	di "`i'"
	count if year == 1989 & icd9 == "`i'"
}
*Meanwhile, no nature of death codes ("N" codes) are ever the underlying cause, 
*according to page 39. 
*case closed!

*from docs: For those causes that do not have a 4th digit, locatlon 145 is blank.
replace icd9 = icd9 + "0" if strlen(icd9) == 3 
assert strlen(icd9) == 4, fast 

*insert implied decimal place
replace icd9 = substr(icd9, 1, 3) + "." + substr(icd9, 4, 1)

*and insert E's into E codes 
replace icd9 = "E" + icd9 if strpos(icd9, "8") == 1 | strpos(icd9, "9") == 1

*compress, nocoalesce 
save ../mid/temp1989_1998, replace 

********************************************************************************
*1999-2002 
local dict2 byte rectype 19 byte restat 20 byte pod 75 byte dow 83 ///
byte manner 139 byte act 140 byte poi 141 ///
byte fipstateocc 119-120 int fipsctyocc 121-123 byte fipstateres 124-125 int fipsctyres 126-128 ///
byte month 55-56 byte sex 59 byte raced 60-61 byte ageu 64 byte age 65-66 byte marstat 77 byte fipstatebirth 78-79 ///
byte hisp 80-81 int ind 85-87 int occ 88-90 byte educ89 52-53 byte educr 54 ///
str4 icd10 142-145 int icd10_358 146-148 int icd10_113 151-153 int icd10_130 154-156 byte icd10_39 157-158 ///
byte work 136 byte entaxnum 160-161 str132 entaxcon 162-301 byte recaxnum 338-339 str89 recaxcon 341-440 

forvalues y = 1999 / 2002 {
	*1999 file has a name that doesn't fit the same pattern as the others 
	if `y' == 1999 infix `dict2' using ../raw/MULT`y'.AllCnty.txt, clear
	else infix `dict2' using ../raw/MULT`y'.USAllCnty.txt, clear
	tempfile `y'
	save ``y''
}

clear
gen int year = .
forvalues y = 1999 / 2002 {	
	append using ``y''
	replace year = `y' if mi(year)
}

replace sex = sex - 1 // 0 = m, 1 = f

replace age = 0 if ageu > 1 & age != 9
replace age = 100 + age if ageu == 1
replace age = 999 if ageu == 9
assert inrange(age, 0, 199) | age == 999
drop ageu

gen byte race = .
replace race = 0 if raced == 1 // white
replace race = 1 if raced == 2 // black
replace race = 2 if raced == 3 // native american 
*asian = chinese, japanese, filipino, indian, korean, vietnamese
replace race = 3 if inlist(raced, 4, 5, 7, 18, 28, 48) 
*pacific islander = hawaiian, samoan, guamanian, "other adian/pac.islander" for some areas, "other adian/pac.islander" for other areas 
replace race = 4 if inlist(raced, 6, 38, 58, 68, 78) 
replace race = 9 if raced == 9 // other
assert !mi(race), fast 
drop raced 

gen byte hisp2 = .
replace hisp2 = 0 if !hisp
replace hisp2 = 1 if inrange(hisp, 1, 5)
replace hisp2 = 9 if hisp == 99
assert !mi(hisp2), fast 
drop hisp
rename hisp2 hisp

replace fipstateres = 99 if !fipstateres

compress, nocoalesce 
save ../mid/temp1999_2002, replace 

********************************************************************************
*2003-2004
local dict3 byte rectype 19 byte restat 20 byte pod 83 byte dow 85 byte manner 107 ///
str2 stateocc 21-22 int fipsctyocc 23-25 str2 stateres 29-30 int fipsctyres 35-37 ///
byte month 65-66 str1 sex 69 byte raced 445-446 byte ageu 70 int age 71-73 str1 marstat 84 ///
str2 statebirth 59-60 int hisp 484-486 byte educ89 61-62 byte educ03 63 byte educf 64 ///
str4 icd10 146-149 int icd10_358 150-152 int icd10_113 154-156 int icd10_130 157-159 byte icd10_39 160-161 ///
byte entaxnum 163-164 str132 entaxcon 165-304 byte recaxnum 341-342 str89 recaxcon 344-443 ///
str1 tob 142 byte poi 145 str1 work 106 str1 disp 108 str1 aut 109 str1 preg 143 byte act 144 

forvalues y = 2003 / 2004 {	
	infix `dict3' using ../raw/MULT`y'.USAllCnty.txt, clear
	tempfile `y'
	save ``y''
}

clear
gen int year = .
forvalues y = 2003 / 2004 {	
	append using ``y''
	replace year = `y' if mi(year)
}

gen byte sex2 = .
replace sex2 = 0 if sex == "M"
replace sex2 = 1 if sex == "F"
assert !mi(sex2), fast 
drop sex
rename sex2 sex 

gen byte marstat2 = .
replace marstat2 = 1 if marstat == "S" 
replace marstat2 = 2 if marstat == "M"
replace marstat2 = 3 if marstat == "W"
replace marstat2 = 4 if marstat == "D"
replace marstat2 = 9 if marstat == "U"
assert !mi(marstat2), fast 
drop marstat 
rename marstat2 marstat

gen byte work2 = .
replace work2 = 1 if work == "Y"
replace work2 = 2 if work == "N"
replace work2 = 9 if work == "U"
assert !mi(work2), fast 
drop work
rename work2 work

gen byte aut2 = .
replace aut2 = 1 if aut == "Y" 
replace aut2 = 0 if aut == "N"
replace aut2 = 9 if aut == "U"
assert !mi(aut2), fast
drop aut
rename aut2 aut 

replace age = 0 if ageu > 1
assert inrange(age, 0, 199) | age == 999
drop ageu 

gen byte race = .
replace race = 0 if raced == 1 // white
replace race = 1 if raced == 2 // black
replace race = 2 if raced == 3 // native american 
*asian = chinese, japanese, filipino, indian, korean, vietnamese
replace race = 3 if inlist(raced, 4, 5, 7, 18, 28, 48) 
*pacific islander = hawaiian, samoan, guamanian, "other adian/pac.islander" for some areas, "other adian/pac.islander" for other areas 
replace race = 4 if inlist(raced, 6, 38, 58, 68, 78) 
replace race = 9 if raced == 9 // other
assert !mi(race), fast 
drop raced 

gen byte hisp2 = .
replace hisp2 = 0 if inrange(hisp, 100, 199)
replace hisp2 = 1 if inrange(hisp, 200, 299)
replace hisp2 = 9 if inrange(hisp, 996, 999)
assert !mi(hisp2), fast 
drop hisp
rename hisp2 hisp

rename stateocc abbrev
fmerge m:1 abbrev using ../mid/fipstate, keepusing(fipstate) assert(3) noreport nogenerate 
rename fipstate fipstateocc
drop abbrev 

rename stateres abbrev
fmerge m:1 abbrev using ../mid/fipstate, keepusing(fipstate) assert(1 3) noreport nogenerate 
replace fipstate = 99 if mi(fipstate)
rename fipstate fipstateres
drop abbrev 

rename statebirth abbrev
fmerge m:1 abbrev using ../mid/fipstate, keepusing(fipstate) assert(1 3) noreport nogenerate 
replace fipstate = 99 if mi(fipstate)
rename fipstate fipstatebirth
drop abbrev 

gen byte preg2 = real(preg)
replace preg2 = 9 if inlist(preg, "N", "") | inlist(preg2, 7, 8)
assert !mi(preg2), fast 
drop preg
rename preg2 preg

gen byte tob2 = .
replace tob2 = 0 if inlist(tob, "n", "N")
replace tob2 = 1 if inlist(tob, "y", "Y")
replace tob2 = 2 if inlist(tob, "p", "P")
replace tob2 = 9 if tob == "U" | tob == "u" | tob == ""
assert !mi(tob2), fast 
drop tob 
rename tob2 tob

gen byte disp2 = .
replace disp2 = 0 if disp == "B"
replace disp2 = 1 if disp == "C"
replace disp2 = 9 if inlist(disp, "O", "U", "D", "E", "R")
assert !mi(disp2), fast 
drop disp
rename disp2 disp

compress, nocoalesce
save ../mid/temp2003_2004, replace 

********************************************************************************
*2005-2017
local dict4 byte rectype 19 byte restat 20 byte pod 83 byte dow 85 byte manner 107 ///
str2 stateocc 21-22 int fipsctyocc 23-25 str2 stateres 29-30 int fipsctyres 35-37 ///
byte month 65-66 str1 sex 69 byte raced 445-446 byte ageu 70 int age 71-73 str1 marstat 84 ///
str2 statebirth 59-60 int hisp 484-486 byte educ89 61-62 byte educ03 63 byte educf 64 ///
str4 icd10 146-149 int icd10_358 150-152 int icd10_113 154-156 int icd10_130 157-159 byte icd10_39 160-161 ///
byte entaxnum 163-164 str138 entaxcon 165-304 byte recaxnum 341-342 str94 recaxcon 344-443 ///
byte poi 145 str1 work 106 str1 disp 108 str1 aut 109 str1 tob 142 str1 preg 143 byte act 144

forvalues y = 2005 / 2017 {	
	infix `dict4' using ../raw/MULT`y'.USAllCnty.txt, clear
	tempfile `y'
	save ``y''
}

clear
gen int year = .
forvalues y = 2005 / 2017 {
	append using ``y''
	replace year = `y' if mi(year)
}

gen byte sex2 = .
replace sex2 = 0 if sex == "M"
replace sex2 = 1 if sex == "F"
assert !mi(sex2), fast 
drop sex
rename sex2 sex 

gen byte marstat2 = .
replace marstat2 = 1 if marstat == "S" 
replace marstat2 = 2 if marstat == "M"
replace marstat2 = 3 if marstat == "W"
replace marstat2 = 4 if marstat == "D"
replace marstat2 = 9 if marstat == "U"
assert !mi(marstat2)
drop marstat 
rename marstat2 marstat

gen byte work2 = .
replace work2 = 1 if work == "Y" | work == "y"
replace work2 = 2 if work == "N" | work == "n"
replace work2 = 9 if work == "U" | work == "u"
assert !mi(work2), fast 
drop work
rename work2 work

gen byte aut2 = .
replace aut2 = 1 if aut == "Y" | aut == "y"
replace aut2 = 0 if aut == "N" | aut == "n"
replace aut2 = 9 if aut == "U" | aut == "u"
assert !mi(aut2), fast
drop aut
rename aut2 aut 

replace age = 0 if ageu > 1
assert inrange(age, 0, 199) | age == 999
drop ageu 

gen byte race = .
replace race = 0 if raced == 1 // white
replace race = 1 if raced == 2 // black
replace race = 2 if raced == 3 // native american 
*asian = chinese, japanese, filipino, indian, korean, vietnamese
replace race = 3 if inlist(raced, 4, 5, 7, 18, 28, 48) 
*pacific islander = hawaiian, samoan, guamanian, "other adian/pac.islander" for some areas, "other adian/pac.islander" for other areas 
replace race = 4 if inlist(raced, 6, 38, 58, 68, 78) 
replace race = 9 if raced == 9 // other
assert !mi(race), fast 
drop raced 

gen byte hisp2 = .
replace hisp2 = 0 if inrange(hisp, 100, 199)
replace hisp2 = 1 if inrange(hisp, 200, 299)
replace hisp2 = 9 if inrange(hisp, 996, 999)
assert !mi(hisp2), fast 
drop hisp
rename hisp2 hisp

rename stateocc abbrev
fmerge m:1 abbrev using ../mid/fipstate, keepusing(fipstate) assert(3) noreport nogenerate 
rename fipstate fipstateocc
drop abbrev 

rename stateres abbrev
fmerge m:1 abbrev using ../mid/fipstate, keepusing(fipstate) assert(1 3) noreport nogenerate 
replace fipstate = 99 if mi(fipstate)
rename fipstate fipstateres
drop abbrev 

rename statebirth abbrev
fmerge m:1 abbrev using ../mid/fipstate, keepusing(fipstate) assert(1 3) noreport nogenerate 
replace fipstate = 99 if mi(fipstate)
rename fipstate fipstatebirth
drop abbrev 

gen byte preg2 = real(preg)
replace preg2 = 9 if inlist(preg, "N", "") | inlist(preg2, 7, 8)
assert !mi(preg2), fast 
drop preg
rename preg2 preg

gen byte tob2 = .
replace tob2 = 0 if inlist(tob, "n", "N")
replace tob2 = 1 if inlist(tob, "y", "Y")
replace tob2 = 2 if inlist(tob, "p", "P")
replace tob2 = 9 if tob == "U" | tob == "u" | tob == ""
assert !mi(tob2), fast 
drop tob 
rename tob2 tob

gen byte disp2 = .
replace disp2 = 0 if disp == "B"
replace disp2 = 1 if disp == "C"
replace disp2 = 9 if inlist(disp, "O", "U", "D", "E", "R")
assert !mi(disp2), fast 
drop disp
rename disp2 disp

*compress, nocoalesce 
save ../mid/temp2005_2017, replace 

********************************************************************************
********************************************************************************
********************************************************************************
*now we combine the separate periods
clear
cd ..
local files : dir mid files "temp*.dta" 
foreach f of local files {
	append using mid/`f'
}
cd code


*we drop ppl who do not reside in the US, or whose state of residence is unknown
drop if fipstateres == 99
assert inrange(fipstateres, 1, 56), fast
assert inrange(fipstateocc, 1, 56), fast 

*we really do not need variables describing relationship between state of occurence 
*and state/country of residence
drop rectype restat 

*HIV deaths in GA in the late 80s/early 90s were recorded as county 999
*if there were fewer than 3 cases in the county (see 1989 docs, p. 3 in pdf)
assert fipstateres == 13 & inrange(year, 1989, 1991) if fipsctyres == 999, fast 
assert fipstateocc == 13 & inrange(year, 1989, 1991) if fipsctyocc == 999, fast 
*we drop them 
drop if fipsctyres == 999 | fipsctyocc == 999
drop if fipsctyres == 999 | fipsctyocc == 999
*and AK and HI 
drop if inlist(fipstateres, 2, 15)
drop if inlist(fipstateocc, 2, 15)

******
*merge our consolidated fips codes 
rename (fipstateres fipsctyres) (fipstate oldfipscty)
fmerge m:1 fipstate oldfipscty using ///
../confips/confips, ///
assert(2 3) keep(3) noreport nogenerate keepusing(confipscty)
rename (fipstate confipscty) (fipstateres confipsctyres)
drop oldfipscty

rename (fipstateocc fipsctyocc) (fipstate oldfipscty)
fmerge m:1 fipstate oldfipscty using ///
../confips/confips, ///
assert(2 3) keep(3) noreport nogenerate keepusing(confipscty)
rename (fipstate confipscty) (fipstateocc confipsctyocc)
drop oldfipscty

*categories slightly different in 1989-2002 and 2003-2017
replace pod = 9 if pod == 4 & year <= 2002 // hospital-unknown -> unknown
*nursing / longterm care combined w hospice in same code used for nursing home pre-2003 
replace pod = 5 if pod == 6 & year >= 2003 
*change code for residence pre-2003 to line up w code post-2003
replace pod = 4 if pod == 6 & year <= 2002  
replace pod = 9 if pod == 7 // other -> missing 
assert inrange(pod, 1, 9), fast 

*standardize - other vars we use 9 for unknown
replace educr = 9 if educr == 6 

*in order to harmonize with seer data, we combine hte asian and pacific islander race categories
*(and we do it here bc it is easier)
replace race = 3 if race == 4 

*combined place of injury variable
gen poi2 = .
replace poi2 = 0 if poi == 0
replace poi2 = 1 if (poi == 7 & year <= 1998) | (poi == 1 & year >= 1999)
replace poi2 = 2 if (poi == 5 & year <= 1998) | (poi == 4 & year >= 1999)
replace poi2 = 3 if (poi == 4 & year <= 1998) | (poi == 3 & year >= 1999)
replace poi2 = 4 if (year <= 1998 & inlist(poi, 2, 3)) | (year >= 1999 & inlist(poi, 5, 6))
replace poi2 = 5 if (poi == 1 & year <= 1998) | (poi == 7 & year >= 1999)
replace poi2 = 6 if (poi == 6 & year <= 1998) | (poi == 2 & year >= 1999)
replace poi2 = 9 if inlist(poi, 8, 9)
assert !mi(poi2) if !mi(poi), fast
drop poi 
rename poi2 poi

*manner of death: recode could not determine as unknown
replace manner = 9 if manner == 5

*activity code: combine other / unknown
replace act = 9 if act == 8

*work-related injury: set no to 0
replace work = 0 if work == 2

*autopsy performed: set no to 0
replace aut = 0 if aut == 2

*year-month of death 
gen int datem = ym(year, month)
format datem %tm

********************************************************************************
*label variables and values 
label variable year "year of death"
label variable month "month of death"
label variable dow "day of week of death"
label variable pod "place of death"
label variable fipstateocc "state of death fips code"
label variable confipsctyocc "county of death consolidated fips code"
label variable fipstateres "state of residence code"
label variable confipsctyres "county of death consolidated fips code"
label variable sex "sex of decedent"
label variable age "age of decedent"
label variable marstat "marital status of decedent"
label variable fipstatebirth "state of decedent's birth fips code"
label variable ind "industry of decedent; coverage variables by year and state; classification varies by year"
label variable occ "occupation of decedent; coverage variables by year and state; classification varies by year"
label variable educ89 "years of education of decedent;17=17+;99=unknown"
label variable educr "education of decedent, 1989 recode schema"
label variable icd9 "icd9 code for death, used 1989-1998"
label variable icd9_282 "icd9, recoded to 282 categories"
label variable icd9_72 "icd9, recoded to 72 categories"
label variable icd9_61 "icd9, recoded to 61 categories"
label variable icd9_52 "icd9, recoded to 52 categories"
label variable icd9_34 "icd9, recoded to 34 categories"
label variable aut "whether an autopsy was performed"
label variable poi "place of injury"
label variable entaxnum "number of entity-axis conditions"
label variable entaxcon "entity-axis conditions"
label variable recaxnum "number of record-axis conditions"
label variable recaxcon "record-axis conditions"
label variable race "race of decedent"
label variable hisp "indicates whether decedent is hispanic"
label variable manner "manner of death"
label variable act "activity code"
label variable icd10 "icd10 code for death, used 1999-2017"
label variable icd10_358 "icd10, recoded to 358 categories"
label variable icd10_113 "icd10, recoded to 113 categories"
label variable icd10_130 "icd10, recoded to 130 categories"
label variable icd10_39 "icd10, recoded to 39 categories"
label variable work "work-related death"
label variable educ03 "education of decedent, 2003 schema"
label variable educf "education schema on certificate"
label variable tob "tobacco usage of decedent"
label variable disp "method of disposition of decedent"
label variable preg "pregnancy status of decedent"
label variable datem "year-month of death"

label define month 1 "jan" 2 "feb" 3 "march" 4 "april" 5 "may" 6 "june" 7 "july" 8 "aug" 9 "sep" 10 "oct" 11 "nov" 12 "dec"
label define dow 1 "sun" 2 "mon" 3 "tues" 4 "wed" 5 "thurs" 6 "fri" 7 "sat" 9 "unknown"
label define pod 1 "hospital-inpatient" 2 "hospital-outpatient/ER" 3 "hospital-dead on arrival" 4 "decedent's residence" 5 "hospice/nursing home/long term care" 9 "other/unknown"
label define sex 0 "male" 1 "female"
label define marstat 1 "single" 2 "married" 3 "widowed" 4 "divorced" 9 "unknown"
label define educr 1 "0-8 years" 2 "9-11 years" 3 "12 years" 4 "13-15 years" 5 "16 years or more" 9 "unkown"
label define aut 0 "no" 1 "yes" 9 "unknown"
label define poi 0 "home" 1 "residential institution" 2 "street" 3 "sporting area" 4 "mine/industrial/construction/trade/service area" 5 "farm" 6 "public building" 9 "other/unknown"
*label define race 0 "white" 1 "black" 2 "native" 3 "asian" 4 "pacific islander" 9 "unknown/other"
label define race 0 "white" 1 "black" 2 "native" 3 "asian / pacific islander" 9 "unknown/other"
label define hisp 0 "not hispanic" 1 "hispanic" 9 "unknown"
label define manner 1 "accident" 2 "suicide" 3 "homicde" 4 "pending investigation" 6 "self-inflicted" 7 "natural" 9 "unknown"
label define act 0 "sports" 1 "leisure" 2 "working for income" 3 "other work" 4 "resting/eating/sleeping" 9 "other/unknown"
label define work 0 "no" 1 "yes" 9 "unknown"
label define educ03 1 "8th grade or less" 2 "high school dropout" 3 "HS grad/GED" 4 "some college" 5 "associate's degree" 6 "bachelor's degree" 7 "master's degree" 8 "doctorate/professional degree" 9 "unknown"
label define preg 1 "not pregnant in last year" 2 "pregnant at death" 3 "pregnant w/in 42 days of death" 4 "pregnant w/in 43-365 days of death" 9 "unknown/NA"
label define tob 0 "did not contribute" 1 "contributed" 2 "probably contributed" 9 "unknown"
label define disp 0 "buried" 1 "cremated" 9 "other/unknown"

foreach v of varlist * {
	cap label values `v' `v'
}

********************************************************************************
*creating a consolidated educaton variable 
*one and only one (main) education variable
assert !mi(educ89) | !mi(educ03), fast 
assert mi(educ89) | mi(educ03), fast 

*education flag mostly good but flawed
foreach v in 0 1 2 . {
	tab educ89 educ03 if educf == `v', m
}

*set all missing education to 9 or 99
replace educ89 = 99 if mi(educ89)
replace educ03 = 9 if mi(educ03)

*there is at most one non-missing education variable
assert educ89 == 99 if educ03 < 9, fast 
assert educ03 == 9 if educ89 < 99, fast
assert !(educ89 < 99 & educ03 < 9), fast 

*construct our own flag
drop educf
gen byte educf = .
replace educf = 0 if educ89 == 99 | educ03 == 9
replace educf = 1 if educ89 < 99
replace educf = 2 if educ03 < 9

label define educf 0 "no educ variable" 1 "1989 educ variable" 2 "2003 educ variable"
label values educf educf

*educ89 maps onto educr as indicated by docs 
assert inrange(educ89, 0, 8)   if educr == 1
assert inrange(educ89, 9, 11)  if educr == 2
assert educ89 == 12            if educr == 3
assert inrange(educ89, 13, 15) if educr == 4
assert inrange(educ89, 16, 17) if educr == 5
assert educ89 == 99            if educr == 9

*we will map educ89 onto educ03, mostly
gen byte education = educ03
*we group assoc. deg. w/some college
replace education = 4 if education == 5 
*we group phd/prof. deg. with MA
replace education = 7 if education == 8 

* grade 8 or less
replace education = 1 if educ89 <= 8 
* 1989 1-3 yrs hs = 2003 hs dropout
replace education = 2 if inrange(educ89, 9, 11) 
* 1989 4 yrs hs = 2003 hs grad / ged
replace education = 3 if educ89 == 12 
* 1989 1-3 yrs college = 2003 some college no degree
replace education = 4 if inrange(educ89, 13, 15) 
* 1989 4 yrs college = 2003 bachelor
replace education = 6 if educ89 == 16
* 1989 5 or more yrs college = 2003 masters 
replace education = 7 if educ89 == 17
* 1989 unknown = 2003 unknown
replace education = 9 if educ89 == 99 & educ03 == 9

* we don't want any skipped numbers, but we do want to keep unknown at 9 
assert education != 5, fast  
assert education != 8, fast 
replace education = education - 1 if inlist(education, 6, 7)
assert !mi(education)

label define education 1 "grade 8 or less" 2 "HS dropout" 3 "HS grad" 4 "some college" 5 "bachelor's" 6 "postgrad" 9 "unknown"
label values education education 
label variable education "consolidated education of decedent"

compress
gen long id = _n
save ../mid/micromaster, replace 
export using ../mid/micromaster.csv, delim(",") nolabel

/*
********************************************************************************
*assign education like case & deaton

*this is a table that holds cumulative education probabilities as columns 
*for each age-sex cell (rows)
preserve
gcollapse (count) deaths = month, by(sex year education) fast 
greshape wide deaths, i(year sex) j(education) fast 
drop deaths9
egen tot = rowtotal(deaths*)
gen cumshr0 = 0
forvalues i = 1 / 6 {
	egen t = rowtotal(deaths1-deaths`i')	
	gen cumshr`i' = t / tot
	drop t 
}
assert cumshr6 == 1
drop deaths* tot
order year sex cumshr0 cumshr1 cumshr2 cumshr3 cumshr4 cumshr5 cumshr6
local i = 1
foreach v of varlist * {
	label variable `v' "`v'"
	rename `v' probtab`i'
	local ++i 
}
mkmat *, matrix(probtab)
save mid/probtab_educimp, replace 
restore

*imputation of education the case-deaton way requires the use a random number generator
*hence we set a seed
set seed 60516 // zip code I grew up in

*case-deaton say 5% of white non-hispanics aged 45-54 in 1999-2013 are missing education
count if !race & !hisp & inrange(age, 45, 54) & inrange(year, 1999, 2013)
local n1 = r(N)
count if !race & !hisp & inrange(age, 45, 54) & inrange(year, 1999, 2013) & education == 9
local n2 = r(N)
di `n2' / `n1' // there it is
assert inrange(`n2' / `n1', 0.048, 0.052)
gen byte education2 = education if education != 9

preserve
keep if education != 9 
tempfile goodeduc
save `goodeduc'
restore

keep if education == 9
gen byte group = ((year - 1988) * 2) + sex - 1
assert inrange(group, 1, 58), fast
gen double rnd = runiform() 

parallel setclusters 24, force
hashsort group
parallel do educimp_pll.do, by(group)
append using `goodeduc'

label variable education2 "consolidated and allocated education of decedent"
label define education2 1 "grade 8 or less" 2 "HS dropout" 3 "HS grad" 4 "some college" 5 "bachelor's" 6 "postgrad" 9 "unknown"
label values education2 education
*/



