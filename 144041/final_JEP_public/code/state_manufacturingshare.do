clear all
pause on
set rmsg on 
set scheme s1color

* SAEMPS and SAEMPN tables from here: https://apps.bea.gov/itable/iTable.cfm?ReqID=70&step=1.  Select the NAICS for the 1998-2016 file, SIC for the prior years
import excel using ../raw/manufacturing1969-1997.xls, cellrange(A6) firstrow clear
drop if mi(LineCode)
destring GeoFips LineCode, replace
drop if GeoFips==0
drop GeoName Description
* bring into form that can be put into long format
rename (E-AK) population#, addnumber(1969)

* reshape, long, then wide
* This allows year to be a column, and to have separate variables for manufacturing and overall employment
greshape long population, i(GeoFips LineCode) j(year)
greshape wide population, i(GeoFips year) j(LineCode)

* divide manufacturing by nonfarm employment
gen manufacturingshare = population400/population80
drop population*
rename GeoFips fips
keep fips year manufacturingshare
replace fips = fips/1000
drop if fips>56

* there is an overlap but we use naics after 1998
drop if year>=1998
tempfile earlymanu
save `earlymanu'

import excel using ../raw/manufacturing1998-2018.xls, cellrange(A6) firstrow clear
drop if mi(LineCode)
destring GeoFips LineCode, replace
drop if GeoFips==0
drop GeoName Description
* identical to the above, except starting in 1998
rename (E-Y) population#, addnumber(1998)

* missing data is 0, not going to affect our overall results
replace population2002 = "" if population2002 == "(D)"
replace population2017 = "" if population2017 == "(D)"
destring population*, replace
greshape long population, i(GeoFips LineCode) j(year)
greshape wide population, i(GeoFips year) j(LineCode)

* slightly different codes for naics
gen manufacturingshare = population500/population80
drop population*
rename GeoFips fips
keep fips year manufacturingshare
replace fips = fips/1000
drop if fips>56

append using `earlymanu'
hashsort year fips
label var manufacturingshare "manufacturing share of non-farm employment"
save ../mid/statemanufacturingshare1969-2018, replace

