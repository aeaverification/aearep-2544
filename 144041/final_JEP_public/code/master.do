#delimit ;
**************************************************************************
* MASTER.DO program – This program can be run directly as-is in Stata without access to restricted-use microdata for a more detailed replication of our data. Two Python programs have been commented out. These programs cannot be run directly in Stata, but their position in this file indicates where they should be run in the workflow. The first Python program can be run with without access to confidential microdata, while the second requires these data. See the README file for details of the purpose and function of each program listed below.
**************************************************************************;
clear all;
pause off;
set rmsg on; 
set scheme s1color;

/* ***************************************************************************** */
* PRELIMINARY SECTION
* These files can all be run in Stata without access to restricted-use mortality data;

do download_raw;
do geo;
do weights;
do seer;
do pullcps; 
do pullbrfss;
* The next program (readin.do) reads in some raw data from CDC Wonder that have been included in the replication files. Consult the README file for the data-use restrictions that must be followed whenever CDC Wonder data is used;
do readin; 
do bea_income;
* The next program (le_berkeley.do) requires the HAVER data service to fully replicate. Accordingly, we save the files we get from Haver in ../mid/ so that le_berkeley.do can be run without having access to Haver;
do le_berkeley;

******
* This is the first two Python programs that cannot be run directly in Stata. It processes the compatibility ratios that bridge the change in disease classifications from ICD9 (which ends in 1998) to ICD10 (which begins in 1999). The outputs from this program are already present in the replication files. However, if you do choose to run this program, then you should run it at this point in the workflow;
* run comparability_ratio_process.py
/* END OF PRELIMINARY SECTION */



/* ****************************************************************************** */
* RESTRICTED-USE MICRODATA SECTION
* These programs cannot be run without access to the restricted-use mortality data. Uncomment them if you have access to these data and want to do a full replication;

*do pull;
*do convertidentifycauses;

******
* This is the second of two Python programs that cannot be run directly in Stata. It processes causes of death in the restricted-use microdata. If you run this program, make sure your computer has a lot of RAM (see README file for details);
* run microcausepd.py

* This Stata program does a small amount of clean-up and other tasks on the dataset created by the previous Python program;
*do microcause;
/* END OF RESTRICTED-USE MICRODATA SECTION */

/* *******************************************************************************/
* COVARIATE SECTION 
* These programs construct various state-level variables used in the analysis. They can be run without access to the confidential data;
do state_collegeshare;
do state_manufacturingshare;
do state_poverty;
do prescription_quality;
do state_covariates;
/* END OF COVARIATE SECTION */

/***********************************************************************************/
* RESTRICTED-USE IMPUTATION & FINAL DATASET SECTION
*This section cannot be run without having run the programs in the Restricted-Use Microdata Section.

*do simple_education_imputation;
*do state_dataset_out;
/* END OF RESTRICTED-USE IMPUTATION & FINAL DATASET SECTION */ 



/* *******************************************************************/
* ANALYSIS/SIMPLE-REPLICATION SECTION 
*These programs use the dataset final_dataset_redacted.dta, which has been included with the replication files. The programs also use some datasets with public-use or aggregated data that have been preloaded in the replication files. The first two programs are the only programs that need to be run for a simple replication of results. The third program checks some of the calculations that we cite in the main text. None of the three programs below require any of the previous programs in this master.do file to be run beforehand, because they use only data from final_dataset_redacted.dta and the pre-loaded public-use datasets;

do all_paper_figures;
do all_appendix_figures;
do paper_numbers;

/* END OF ANALYSIS/SIMPLE-REPLICATION SECTION */