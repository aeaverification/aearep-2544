#delimit ;
set scheme s1color;
pause on;

local file = "../final_dataset_redacted";

local prg JEP_numbers_check;
cap log close;

log using ../logs/`prg'.log, replace;

scalar createstartingpoint = 1;

if(createstartingpoint==1){;
use `file', clear;
cap drop _merge;

* midlife mortality;
keep if age_group == 3; 
drop age_group;

ren fipstate fips;
sort fips year;
gen lnmr_all       = ln(mr_all);
gen lnmr_NC        = ln(mr_all_NC);
gen lnmr_CD      = ln(mr_all_CD);
gen lnNC_CD_diff = lnmr_NC - lnmr_CD;
cap rename realpcincome ypph;
gen lnypph = ln(ypph);
gen lnpop = ln(pop);
sort fips year;
tsset fips year, y;
cap drop *diff*;

la var smoke "Smoke every day";
la var lnypph "Ln Real Per Capita Income";

gen lnpop_NC_all = ln(pop_NC);
gen lnpop_CD_all = ln(pop_CD);

* keep the relevant years and states
keep if year >= 1992 & year <= 2016;
drop if inlist(fips, 2, 11, 15) | fips > 56;
drop if inlist(fips, 13, 40, 44, 46);
hashsort fips year;
tempfile startingpoint;
save `startingpoint', replace;
};

* growth in CV across age groups;
use ../mid/readin_WONDER, clear;
* drop if fips == 2 | fips == 11 | fips == 15;
tab state, m;

collapse (sd)    sdcrude = cruderate  sdageadj = ageadjustedrate
        (mean) meancrude = cruderate meanageadj = ageadjustedrate [aw=population], by(age_group year);

gen cvcrude  =  sdcrude/meancrude;
gen cvageadj = sdageadj/meanageadj;
keep cvageadj age_group year;
keep if inlist(year, 1968, 2019);
greshape wide cv*, i(age_group) j(year);
gen diff = cvageadj2019 - cvageadj1968;
br diff cvageadj2019 cvageadj1968 age_group;
*pause;

* 2016 despair portion of deaths;
use `startingpoint', clear;
keep despair deaths year;
gcollapse (sum) despair deaths, by(year);
keep if year == 2016;
gen ratio = despair/deaths;
br ratio;
pause;

* relative increase in CV for all, non-despair deaths;
use `startingpoint', clear;
collapse (mean) mr_all mr_other mr_despair (sd) sd_mr_all = mr_all sd_mr_other = mr_other [aw = pop], by(year);
gen cv_mr_all = sd_mr_all/mr_all;
gen cv_mr_other = sd_mr_other/mr_other;
keep if year == 1992 | year == 2016;
keep year cv_mr_all cv_mr_other;
gen id = 1;
greshape wide cv_mr_*, i(id) j(year);
gen percentinc_all = ((cv_mr_all2016/cv_mr_all1992)-1)*100;
gen percentinc_other = ((cv_mr_other2016/cv_mr_other1992)-1)*100;
br percentinc_all percentinc_other;
pause;

* correlation between state-level growths;
use `startingpoint', clear;
keep mr_all mr_other mr_despair year fips;
keep if year == 1992 | year == 2016;
greshape wide mr_*, i(fips) j(year);
gen growthother = mr_other2016/mr_other1992;
gen growthdespair = mr_despair2016/mr_despair1992;
corr growthdespair growthother;
pause;

* regressions mortality on income;
use `startingpoint', clear;
preserve;
keep if year == 1992;
qui reg lnmr_all lnypph;
disp `e(r2)';
restore;

preserve;
keep if year == 2016;
qui reg lnmr_all lnypph;
disp `e(r2)';
restore;

pause;

* rankings of state mortality rates
use `startingpoint', clear;
keep mr_all mr_despair year fips state_abbrev;

keep if year == 2016;
hashsort mr_all;
* check ranking of Ohio, California;
br mr_all state_abbrev; 
pause;
* despair ranking of Ohio, California;
hashsort mr_despair;
br mr_despair state_abbrev;
pause;
drop state_abbrev;
greshape wide mr_all mr_despair, i(year) j(fips);
* ratio West Virginia to Minnesota;
gen ratiowv_minnesota = mr_all54/mr_all27;
br ratiowv_minnesota;
pause;

* growth of all cause mortality? confirming WV, KY went up, down for college;
use `startingpoint', clear;
keep mr_all mr_all_CD year fips state_abbrev;
keep if inlist(year, 1992, 2016);
greshape wide mr_all*, i(fips state_abbrev) j(year);
br state_abbrev fips mr_all_CD1992 mr_all_CD2016 mr_all2016 mr_all1992 if mr_all2016 > mr_all1992;

pause;


* California, Ohio in 1992, 2016;
use `startingpoint', clear;
keep mr_all mr_despair mr_other year fips state_abbrev;
keep if inlist(year, 1992, 2016) & inlist(fips, 6, 39);
br year fips mr_all mr_despair mr_other;
pause;


* Figure 4 dispersion numbers;
use `startingpoint', clear;
* repeating code to generate Figure 4 in paper;
scalar hidefigure4code = 1;
if(hidefigure4code==1){;
gen lnmr_diff = lnmr_NC - lnmr_CD;
gcollapse (mean) phi_et = lnmr_diff, by(year) fast merge;

gen double collshare2 = (mr_all - mr_all_NC)/(mr_all_CD - mr_all_NC);

*****************************************************
* Reshape the data into long form; 
keep fips year lnmr_CD lnmr_NC phi_et pop collshare2;
reshape long lnmr_, i(fips year) j(class) string;

replace phi_et = 0 if class == "CD";
gcollapse (mean) phi_t = lnmr_, by(year) fast merge;
gen myerror_ = lnmr_ - phi_t - phi_et;

gen thisshare = 0;
replace thisshare = collshare2   if class == "CD";
replace thisshare = 1-collshare2 if class == "NC";

save ../mid/`prg'_longdata, replace;

use ../mid/`prg'_longdata;

sort fips year class;
gen share90   = thisshare if year == 1992;
gen phi_t90  = phi_t    if year == 1992;
gen phi_et90  = phi_et    if year == 1992;
gen error90   = myerror_  if year == 1992;
gen error90CDonly = myerror_ if year == 1992 | class == "NC";
gen error90NConly = myerror_ if year == 1992 | class == "CD";

sort fips class year;
by fips class: carryforward share90 phi_t90 phi_et90 error90 error90CDonly error90NConly, replace;

* COUNTER1: Change college error before high school error;
gen counter1a = exp(phi_t90 + phi_et90 + error90);
gen counter1x = counter1a;
gen counter1b = exp(phi_t    + phi_et90   + error90);
gen counter1c = exp(phi_t    + phi_et   + error90);
gen counter1d = exp(phi_t    + phi_et   + error90NConly);
gen counter1e = exp(phi_t   + phi_et   + myerror_);

* COUNTER2: Change high school error before college error;
gen counter2a = exp(phi_t90 + phi_et90 + error90);
gen counter2x = counter2a;
gen counter2b = exp(phi_t    + phi_et90   + error90);
gen counter2c = exp(phi_t    + phi_et   + error90);
gen counter2d = exp(phi_t    + phi_et   + error90CDonly);
gen counter2e = exp(phi_t   + phi_et   + myerror_);

* COUNTER3: change phi_t, phi_et after, then college first;
gen counter3a = exp(phi_t90 + phi_et90 + error90);
gen counter3x = counter3a;
gen counter3b = exp(phi_t90    + phi_et90  + error90NConly);
gen counter3c = exp(phi_t90    + phi_et90   + myerror_);
gen counter3d = exp(phi_t    + phi_et90   + myerror_);
gen counter3e = exp(phi_t   + phi_et   + myerror_);

sort fips year class;
save ../mid/`prg'_counters_classes, replace;

***********************************
* Now figure state-level means of the counterfactuals 
* by collapsing education-specific classes
* using the appropriate college shares (either 1992 or actual);

use  ../mid/`prg'_counters_classes, clear;
collapse (mean) counter1x [aw=share90], by(fips year);
gen lncounter1x = ln(counter1x);
save ../mid/temp1, replace;

use ../mid/`prg'_counters_classes, clear;
collapse (mean) counter1a counter1b counter1c counter1d counter1e [aw=thisshare], by(fips year);
gen lncounter1a = ln(counter1a);
gen lncounter1b = ln(counter1b);
gen lncounter1c = ln(counter1c);
gen lncounter1d = ln(counter1d);
gen lncounter1e = ln(counter1e);

merge 1:1 fips year using ../mid/temp1, assert(3);
drop _merge;
erase ../mid/temp1.dta;

* Merge in selected state-wide variables from TWID2 so that we can check our work;
sort fips year;
merge 1:1 fips year using `startingpoint', keepusing(fips year lnypph mr_all lnmr_all mr_all_CD mr_all_NC pop);
keep if _merge == 3;
drop _merge;

save ../mid/`prg'_counters_states, replace;

*************************************
* Find standard deviations of the state-wide data and the various counterfactuals;

use ../mid/`prg'_counters_states, clear;
collapse (sd) lncounter1x lncounter1a lncounter1b lncounter1c lncounter1d lncounter1e
			  lnmr_all [aw = pop], by(year);
};

la var lncounter1x "1992 Variance";			
la var lncounter1a "Use Actual College Shares";
la var lncounter1b "Add Actual Year Effect";
la var lncounter1c "Add Actual Education Effect";
la var lncounter1d "Add College Error Term";
la var lncounter1e "Add High School Error Term";
la var lnmr_all    "Actual State-Level Data";
keep if year == 2016;
gen channelsab = (lncounter1c - lncounter1x)/(lncounter1e - lncounter1x);
gen channelsc = (lncounter1d - lncounter1c)/(lncounter1e - lncounter1x);
gen channelsd = (lncounter1e - lncounter1d)/(lncounter1e - lncounter1x);
br channelsab channelsc channelsd;
pause;


* code to generate year-by-year correlations;
scalar hidecorrelationcode = 1;

if(hidecorrelationcode == 1){;
clear;
* Calculate year-by-year correlations;
cap erase ../mid/`prg'_corrdata.dta;

* Set up new database to store year-by-year correlations. It will have 
* 26 years (reflecting 1990 through 2016);
set obs 28;
gen int year = 1991+_n;
gen double corr_resid = .;
gen double cov_resid = .;
gen double var_NC_resid = .;
gen double var_CD_resid = .;
gen double betaNC_lhs_resid = .;
gen double betaCD_lhs_resid= .;

save ../mid/`prg'_corrdata, replace;

use ../mid/`prg'_longdata, clear;

* make back wide;
drop thisshare collshare2 phi_et phi_t;
reshape wide lnmr_ myerror_, i(fips year) j(class) string;
tempfile startofloop;
save `startofloop', replace;

foreach y of numlist 1992(1)2016 {;

  * Raw correlations;
  use `startofloop', clear;
  corr myerror_NC myerror_CD if year == `y';
  local mycorr_resid  = r(rho);
  
  corr myerror_NC myerror_CD if year == `y', cov;
  local mycov_resid   = r(cov_12);
  local myV_NC_resid  = r(Var_1);
  local myV_CD_resid  = r(Var_2);
  
  use ../mid/`prg'_corrdata, clear;
 
  replace corr_resid   = `mycorr_resid' if year == `y';
  replace cov_resid    = `mycov_resid'  if year == `y';
  replace var_NC_resid = `myV_NC_resid' if year == `y';
  replace var_CD_resid = `myV_CD_resid' if year == `y';
  replace betaNC_lhs_resid = cov_resid/var_CD_resid;
  replace betaCD_lhs_resid = cov_resid/var_NC_resid;

  save ../mid/`prg'_corrdata, replace;
};
};

use ../mid/`prg'_corrdata, clear;
tsset year, y;
keep if year == 1993 | year == 2016;
keep corr_resid year;
disp corr_resid;
pause;

* low correlation 1968 1992, high 1992 2016;
use ../mid/readin_WONDER, clear;
keep fips year cruderate ageadjustedrate population age_group;
reshape wide cruderate ageadjustedrate population, i(fips year) j(age_group);

sort fips year;
merge 1:1 fips year using ../mid/bea_income, keep(3) nogenerate;
keep if year >= 1968 & year <= 2019;

gen ypph_1968x = ypph if year == 1968;
egen ypph_1968 = max(ypph_1968x), by(fips);
drop ypph_1968x;
gen ypph_1992x = ypph if year == 1968;
egen ypph_1992 = max(ypph_1992x), by(fips);
drop ypph_1992x;

* 4-panel plots;
drop if fips == 2 | fips == 11 | fips == 15;
drop if inlist(fips, 13, 40, 44, 46);


corr ageadjustedrate3 ypph_1968 if year == 1992;
corr ageadjustedrate3 ypph_1992 if year == 2019;
