clear all
pause on
set rmsg on 
set scheme s1color


*this file links state fips codes, names, and abbreviations
*this allows us to use the -statastates- module with -fmerge- 
*(since statastates uses regular -merge- and -fmerge is faster)
set obs 51
gen fipstate = .
local fips     1  2  4  5  6  8  9 10 11 12 13 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 44 45 46 47 48 49 50 51 53 54 55 56 
forvalues i = 1 / 51 {
	local f : word `i' of `fips'
	replace fipstate = `f' in `i'
}
statastates, fips("fipstate") nogen
rename (state_abbrev state_name) (abbrev name)
save ../mid/fipstate, replace

* regular county info, from https://www2.census.gov/geo/docs/maps-data/data/gazetteer/2019_Gazetteer/
* downloaded directly in download_raw
import delimited ../raw/2019_Gaz_counties_national.txt, delimiter(tab) clear
gen fipstate = int(geoid / 1000)
gen fipscty = geoid - (fipstate * 1000)
drop usps ansicode geoid
save ../mid/counties2019, replace 


*confips to hrr crosswalk, downloaded from https://mcdc.missouri.edu/applications/geocorr2018.html
* county to hospital referral region
* cannot download via code, must download manually
import delimited ../raw/geocorr2018, clear 
keep v1 v2 v6 v7
rename (v1 v2 v6 v7) (fips hrr pop afact)
foreach v of varlist * {
	assert mi(real(`v')) in 1/2
}
drop in 1/2
destring *, replace 

gen fipstate = int(fips / 1000)
gen oldfipscty = fips - (1000 * fipstate) 
drop if inlist(fipstate, 2, 15)
merge m:1 fipstate oldfipscty using ../confips/confips, assert(2 3) keep(3) noreport nogenerate keepusing(confips)
gcollapse (sum) pop, by(confips hrr) fast 
gcollapse (sum) popcty = pop, by(confips) fast merge
*allocation factor = share of population in a county going to a given hrr
gen afact = pop / popcty
drop pop*

compress
label data "confips to hrr crosswalk, afact derived from 2016 population shares"
save ../mid/confipstohrr, replace 

hist afact, start(0) width(0.05) fraction xlab(0(0.1)1) ///
xti("Share of ConFips going to HRR") yti("Share of ConFips-HRR Combos in Bin")

hashsort confips - afact
by confips : gen largest = _n == 1

hist afact if largest, start(0) width(0.05) fraction xlab(0(0.1)1) 
