#delimit ;
set scheme s1color;
pause on;

local file = "../final_dataset_redacted";

local prg JEP_appendix_figures_final;
cap log close;

log using ../logs/`prg'.log, replace;

scalar createstartingpoint = 1;
scalar createcrudestartingpoint = 1;
scalar figurea1 = 1;
scalar figurea2 = 1;
scalar figurea3 = 1;
scalar figurea4 = 1;
scalar figurea5 = 1;
scalar figurea6 = 1;
scalar tablea1 = 1;

* the below creates a starting point dataset that is used to generate a number of our figures;
if(createstartingpoint==1){;
use `file', clear;
cap drop _merge;

* keep only midlife mortality;
keep if age_group == 3; 
drop age_group;

ren fipstate fips;
sort fips year;
gen lnmr_all       = ln(mr_all);
gen lnmr_NC        = ln(mr_all_NC);
gen lnmr_CD      = ln(mr_all_CD);
gen lnNC_CD_diff = lnmr_NC - lnmr_CD;
cap rename realpcincome ypph;
gen lnypph = ln(ypph);
gen lnpop = ln(pop);
sort fips year;
tsset fips year, y;
cap drop *diff*;

la var smoke "Smoke every day";
la var lnypph "Ln Real Per Capita Income";

gen lnpop_NC_all = ln(pop_NC);
gen lnpop_CD_all = ln(pop_CD);

* keep the relevant years and states;
keep if year >= 1992 & year <= 2016;
drop if inlist(fips, 2, 11, 15) | fips > 56;
drop if inlist(fips, 13, 40, 44, 46);

hashsort fips year;
tempfile startingpoint;
save `startingpoint', replace;
};


* the below creates a crude starting point;
if(createcrudestartingpoint==1){;
use `file', clear;
cap drop _merge;

* keep only midlife mortality;
keep if age_group == 3; 
drop age_group;

ren fipstate fips;
sort fips year;
replace mr_all = mr_all_crude;
replace mr_all_NC = mr_all_crude_NC;
replace mr_all_CD = mr_all_crude_CD;
gen lnmr_all       = ln(mr_all);
gen lnmr_NC        = ln(mr_all_NC);
gen lnmr_CD      = ln(mr_all_CD);
gen lnNC_CD_diff = lnmr_NC - lnmr_CD;
cap rename realpcincome ypph;
gen lnypph = ln(ypph);
gen lnpop = ln(pop);
sort fips year;
tsset fips year, y;
cap drop *diff*;

la var smoke "Smoke every day";
la var lnypph "Ln Real Per Capita Income";

gen lnpop_NC_all = ln(pop_NC);
gen lnpop_CD_all = ln(pop_CD);

* keep the relevant years and states;
keep if year >= 1992 & year <= 2016;
drop if inlist(fips, 2, 11, 15) | fips > 56;
drop if inlist(fips, 13, 40, 44, 46);

hashsort fips year;
tempfile crudestartingpoint;
save `crudestartingpoint', replace;
};

* le by race;
if(figurea1 == 1){;
use ../mid/le_berkeley_mean_sds, clear;

la var USLE     "US Life Expectancy at Birth (Years, left scale)";
la var sdle_wtd "Std. Dev. of State-Level Life Expectancy (right scale)";
gen cv_wtd = sdle_wtd/USLE;
la var cv_wtd   "Coef. of Variation of State-Level Life Exp. (right scale)";   
la var USLEW   "White";
la var USLEB   "Black";
la var USLEWX  "White Non-Hispanic";
la var USLEBX  "Black Non-Hispanic";
la var USLEH   "Hispanic";

replace USLEW = . if USLEWX ~= .;
replace USLEB = . if USLEBX ~= .;

gr tw (line USLEW USLEWX USLEB USLEBX USLEH    year if year >= 1959, lc(navy navy cranberry cranberry dkgreen) lp(solid dash solid dash solid)) , 
  xlabel(1960(5)2020)
  xti("")
  yti("Years")
  leg(ring(0) pos(11) cols(1) region(lstyle(none)));

gr export ../paper_figures/figure_a1.pdf, as(pdf) replace;
};

* cv increase over time, black, hispanic;
if(figurea2==1){;
foreach stub in h b wnh {;
if("`stub'"=="b"){;
local title = "Black";
local pos = 11;
};
if("`stub'"=="h"){;
local title = "Hispanic";
local pos = 12;
};
if("`stub'"=="wnh"){;
local title = "White non-Hispanic";
local pos = 11;
};

use `startingpoint', clear;
*collapse (mean) mr_all_`stub' mr_other_`stub' (sd) sd_mr_all_`stub' = mr_all_`stub' sd_mr_other_`stub' = mr_other_`stub' [aw = pop], by(year);
*save ../mid/cv_over_time_`stub', replace;
use ../mid/cv_over_time_`stub', replace;
gen cv_mr_all_`stub' = sd_mr_all_`stub'/mr_all_`stub';
gen cv_mr_other_`stub' = sd_mr_other_`stub'/mr_other_`stub';
la var cv_mr_all_`stub'   "All-Cause Mortality Rate, `title'";
la var cv_mr_other_`stub'     "Omitting Deaths of Despair";

tsline cv_mr_all_`stub' cv_mr_other_`stub', 
  lp(solid dash)
  lc(navy cranberry)
  yti("Coefficient of Variation")
  leg(ring(0) pos(`pos') cols(1) region(lstyle(none)))
  xlabel(1992(3)2016)
  xti("")
  name(components_all, replace);
gr export ../paper_figures/figure_a2_`stub'.pdf, as(pdf) replace;
};
};

* figure a3: changing order of decomposition;
if(figurea3==1){;
use `startingpoint', clear;

* considering two equation system
* mr_it^(C) = phi_t + u_(it)^{C}
* mr_it^(H) = phi_t + phi_et + u_(it)^(H);

* to estimate phi_t, make data long, and then demean by year
* we will do this later
* we first estimate phi_et;

gen lnmr_diff = lnmr_NC - lnmr_CD;
gcollapse (mean) phi_et = lnmr_diff, by(year) fast merge;

*******************************************************************
* Now estimate college shares that force everything to be exact. 
* We do this by finding the CD and NC shares that will generate 
* the state-level mortality rate when these college shares are applied to the 
* education-specific rates for that state. Call this new share variable COLLSHARE2,
* to distinguish it from the existing COLLSHARE
* Note that the results are NOT significantly affected whichever choice is made;

gen double collshare2 = (mr_all - mr_all_NC)/(mr_all_CD - mr_all_NC);

gen double mycheck = collshare2*mr_all_CD + (1-collshare2)*mr_all_NC;
assert abs(mycheck - mr_all) < .000000000001 if !missing(mycheck) & !missing(mr_all);
drop mycheck;

*****************************************************
* Reshape the data into long form; 
keep fips year lnmr_CD lnmr_NC phi_et pop collshare2;
reshape long lnmr_, i(fips year) j(class) string;

replace phi_et = 0 if class == "CD";
gcollapse (mean) phi_t = lnmr_, by(year) fast merge;
gen myerror_ = lnmr_ - phi_t - phi_et;

gen thisshare = 0;
replace thisshare = collshare2   if class == "CD";
replace thisshare = 1-collshare2 if class == "NC";

save ../mid/`prg'_longdata, replace;

use ../mid/`prg'_longdata;

sort fips year class;
gen share92   = thisshare if year == 1992;
gen phi_t92  = phi_t    if year == 1992;
gen phi_et92  = phi_et    if year == 1992;
gen error92   = myerror_  if year == 1992;
gen error92CDonly = myerror_ if year == 1992 | class == "NC";
gen error92NConly = myerror_ if year == 1992 | class == "CD";

sort fips class year;
by fips class: carryforward share92 phi_t92 phi_et92 error92 error92CDonly error92NConly, replace;

* COUNTER1: Change college error before high school error;
gen counter1a = exp(phi_t92 + phi_et92 + error92);
gen counter1x = counter1a;
gen counter1b = exp(phi_t    + phi_et92   + error92);
gen counter1c = exp(phi_t    + phi_et   + error92);
gen counter1d = exp(phi_t    + phi_et   + error92NConly);
gen counter1e = exp(phi_t   + phi_et   + myerror_);

* COUNTER2: Change high school error before college error;
gen counter2a = exp(phi_t92 + phi_et92 + error92);
gen counter2x = counter2a;
gen counter2b = exp(phi_t    + phi_et92   + error92);
gen counter2c = exp(phi_t    + phi_et   + error92);
gen counter2d = exp(phi_t    + phi_et   + error92CDonly);
gen counter2e = exp(phi_t   + phi_et   + myerror_);

* COUNTER3: change phi_t, phi_et after, then college first;
gen counter3a = exp(phi_t92 + phi_et92 + error92);
gen counter3x = counter3a;
gen counter3b = exp(phi_t92    + phi_et92  + error92NConly);
gen counter3c = exp(phi_t92    + phi_et92   + myerror_);
gen counter3d = exp(phi_t    + phi_et92   + myerror_);
gen counter3e = exp(phi_t   + phi_et   + myerror_);

gen counter4a = exp(phi_t92 + phi_et92 + error92);
gen counter4b = exp(phi_t92    + phi_et92  + error92NConly);
gen counter4c = exp(phi_t92    + phi_et92   + myerror_);
gen counter4d = exp(phi_t    + phi_et92   + myerror_);
gen counter4e = exp(phi_t   + phi_et   + myerror_);
gen counter4x = counter4e;

sort fips year class;
save ../mid/`prg'_counters_classes, replace;

***********************************
* Now figure state-level means of the counterfactuals 
* by collapsing education-specific classes
* using the appropriate college shares (either 1992 or actual);

use  ../mid/`prg'_counters_classes, clear;
collapse (mean) counter1x counter2x counter3x counter4a counter4b counter4c counter4d counter4x [aw=share92], by(fips year);
gen lncounter1x = ln(counter1x);
gen lncounter2x = ln(counter2x);
gen lncounter3x = ln(counter3x);
gen lncounter4a = ln(counter4a);
gen lncounter4b = ln(counter4b);
gen lncounter4c = ln(counter4c);
gen lncounter4d = ln(counter4d);
gen lncounter4x = ln(counter4x);
save ../mid/temp1, replace;

use ../mid/`prg'_counters_classes, clear;
collapse (mean) counter1a counter1b counter1c counter1d counter1e 
                counter2a counter2b counter2c counter2d counter2e
		counter3a counter3b counter3c counter3d counter3e counter4e [aw=thisshare], by(fips year);
gen lncounter1a = ln(counter1a);
gen lncounter1b = ln(counter1b);
gen lncounter1c = ln(counter1c);
gen lncounter1d = ln(counter1d);
gen lncounter1e = ln(counter1e);

gen lncounter2a = ln(counter2a);
gen lncounter2b = ln(counter2b);
gen lncounter2c = ln(counter2c);
gen lncounter2d = ln(counter2d);
gen lncounter2e = ln(counter2e);

gen lncounter3a = ln(counter3a);
gen lncounter3b = ln(counter3b);
gen lncounter3c = ln(counter3c);
gen lncounter3d = ln(counter3d);
gen lncounter3e = ln(counter3e);

gen lncounter4e = ln(counter4e);

merge 1:1 fips year using ../mid/temp1, assert(3);
drop _merge;
erase ../mid/temp1.dta;

* Merge in selected state-wide variables from TWID2 so that we can check our work;
sort fips year;
merge 1:1 fips year using `startingpoint', keepusing(fips year lnypph mr_all lnmr_all mr_all_CD mr_all_NC pop);
keep if _merge == 3;
drop _merge;

save ../mid/`prg'_counters_states, replace;

*****************************
* Check class-specific and state-wide rates against data from TWID2;

use ../mid/`prg'_counters_classes, clear;
keep fips year counter1e counter2e counter3e counter4e class;
reshape wide counter1e counter2e counter3e counter4e, i(fips year) j(class) string;

order fips year *CD* *NC*;
merge 1:1 fips year using ../mid/`prg'_counters_states;
keep if _merge == 3;

* note that COUNTER1d and COUNTER2d use actual data throughout, so they should both
* match MR_ALL -- the state-wide mortality rate;
tsline mr_all counter1e counter2e counter3e counter4e if fips == 36, lp(solid dash) leg(cols(1)) name(ratecheck36, replace);
tsline mr_all counter1e counter2e counter3e counter4e if fips == 10, lp(solid dash) leg(cols(1)) name(ratecheck10, replace);

assert abs(mr_all - counter1e) < .001 if !missing(mr_all) & !missing(counter1e);
assert abs(mr_all - counter2e) < .001 if !missing(mr_all) & !missing(counter2e);
assert abs(mr_all - counter3e) < .001 if !missing(mr_all) & !missing(counter3e);
assert abs(mr_all - counter4e) < .001 if !missing(mr_all) & !missing(counter4e);


* same for the education-class specific rates;
assert abs(mr_all_NC - counter1eNC) < .001 if !missing(mr_all_NC) & !missing(counter1eNC);
assert abs(mr_all_NC - counter2eNC) < .001 if !missing(mr_all_NC) & !missing(counter2eNC);
assert abs(mr_all_NC - counter3eNC) < .001 if !missing(mr_all_NC) & !missing(counter3eNC);
assert abs(mr_all_NC - counter4eNC) < .001 if !missing(mr_all_NC) & !missing(counter4eNC);

assert abs(mr_all_CD - counter1eCD) < .001 if !missing(mr_all_CD) & !missing(counter1eCD);
assert abs(mr_all_CD - counter2eCD) < .001 if !missing(mr_all_CD) & !missing(counter2eCD);
assert abs(mr_all_CD - counter3eCD) < .001 if !missing(mr_all_CD) & !missing(counter3eCD);
assert abs(mr_all_CD - counter4eCD) < .001 if !missing(mr_all_CD) & !missing(counter4eCD);

save ../mid/`prg'_counters_checks, replace;

*************************************
* Find standard deviations of the state-wide data and the various counterfactuals;

use ../mid/`prg'_counters_states, clear;
collapse (sd) lncounter1x lncounter1a lncounter1b lncounter1c lncounter1d lncounter1e
              lncounter2x lncounter2a lncounter2b lncounter2c lncounter2d lncounter2e
	      lncounter3x lncounter3a lncounter3b lncounter3c lncounter3d lncounter3e
	      lncounter4a lncounter4b lncounter4c lncounter4d lncounter4x lncounter4e
			  lnmr_all [aw = pop], by(year);

* education only model, decomposition of variance
la var lncounter1x "1992 Baseline";			
la var lncounter1a "Add Actual College Shares";
la var lncounter1c "Add Actual Non-College Penalty";
la var lncounter1d "Add Actual College Residuals";
la var lncounter1e "Add Actual Non-College Residuals";

la var lncounter2x "1992 Baseline";			
la var lncounter2a "Add Actual College Shares";
la var lncounter2c "Add Actual Non-College Penalty";
la var lncounter2d "Add Actual Non-College Residuals";
la var lncounter2e "Add Actual College Residuals";

la var lncounter3x "1992 Baseline";			
la var lncounter3a "Add Actual College Shares";
la var lncounter3b "Add Actual College Residuals";
la var lncounter3c "Add Actual Non-College Residuals";
la var lncounter3e "Add Actual Non-College Penalty";

la var lncounter4a "1992 Baseline";
la var lncounter4b "Add Actual College Residuals";			
la var lncounter4c "Add Actual Non-College Residuals";
la var lncounter4x "Add Actual Non-College Penalty";
la var lncounter4e "Add Actual College Shares";

la var lnmr_all    "Total Standard Deviation";
  
tsline lncounter2x lncounter2a /*lncounter2b*/ lncounter2c lncounter2d lncounter2e lnmr_all,
  lc(navy cranberry brown cyan khaki pink)
  lp(solid dash long solid solid dash dash_dot) 
  ti("Changing Non-College Residuals Before College Residuals", size(medsmall))
  xlabel(1992(3)2016)
  ylabel(.10(.02).2)
  name(counter2, replace) xti("") leg(ring(0) pos(11) cols(1) region(lstyle(none)));

*gr save ../paper_figures/figure_a3_1.gph, replace;
gr export ../paper_figures/figure_a3_1.pdf, as(pdf) replace;

tsline lncounter3x lncounter3a lncounter3b lncounter3c /*lncounter3d*/ lncounter3e lnmr_all,
  lc(navy cranberry brown cyan khaki pink)
  lp(solid dash long solid solid dash dash_dot) 
  ti("Changing National Non-College Penalty After Residuals", size(medsmall))
  xlabel(1992(3)2016)
  ylabel(.10(.02).2)
  name(counter3, replace) xti("") leg(ring(0) pos(11) cols(1) region(lstyle(none)));
*gr save ../paper_figures/figure_a3_2.gph, replace;
gr export ../paper_figures/figure_a3_2.pdf, as(pdf) replace;

tsline lncounter4a lncounter4b lncounter4c /*lncounter4d*/ lncounter4x lncounter4e lnmr_all,
  lc(navy cranberry brown cyan khaki pink)
  lp(solid dash long solid solid dash dash_dot) 
  ti("Changing College Share and Non-College Penalty After Residuals", size(medsmall))
  xlabel(1992(3)2016)
  ylabel(.10(.02).2)
  name(counter4, replace) xti("") leg(ring(0) pos(11) cols(1) region(lstyle(none)));
*gr save ../paper_figures/figure_a3_3.gph, replace;
gr export ../paper_figures/figure_a3_3.pdf, as(pdf) replace;
clear;
};

* figure a4: place effect model;
if(figurea4==1){;
use `startingpoint', clear;
* Here is the "high-school minus college mortality rate
* regression" estimated in difference form (Non-college - college)
* One observation per state and year. MR here stands for "mortality rate", 
* CD = college degree and NC stands for non-college. The 
* year dummies here will identify the yearly "education effect".
* 
* This regression has one observation for each state and year;

gen lnmr_diff = lnmr_NC - lnmr_CD;
reg lnmr_diff i.year;

* Estimate phi_et's and phi_it's;
reg lnmr_diff i.year, cluster(fips);
predict phi_et, xb;
gen lnmr_NC_twid = lnmr_NC - phi_et;
gen phi_it = .5*(lnmr_NC_twid + lnmr_CD);

gen myerror_NC = lnmr_NC - phi_it - phi_et;
gen myerror_CD = lnmr_CD - phi_it;

*******************************************************************
* Now estimate college shares that force everything to be exact. 
* We do this by finding the CD and NC shares that will generate 
* the state-level mortality rate when these college shares are applied to the 
* education-specific rates for that state. Call this new share variable COLLSHARE2,
* to distinguish it from the existing COLLSHARE;
gen double collshare2 = (mr_all - mr_all_NC)/(mr_all_CD - mr_all_NC);

gen double mycheck = collshare2*mr_all_CD + (1-collshare2)*mr_all_NC;
assert abs(mycheck - mr_all) < .000000000001 if !missing(mycheck) & !missing(mr_all);
drop mycheck;

*****************************************************
* Reshape the data into long form; 
keep fips year lnmr_CD lnmr_NC myerror_CD myerror_NC phi_et phi_it pop collshare2;
reshape long lnmr_ myerror_, i(fips year) j(class) string;
replace phi_et = 0 if class == "CD";
gen thisshare = 0;
replace thisshare = collshare2   if class == "CD";
replace thisshare = 1-collshare2 if class == "NC";
save ../mid/placeeffect_longdata, replace;

**************************************************
* Counterfactual exercises ("counters");

use ../mid/placeeffect_longdata;

sort fips year class;
gen share92x   = thisshare if year == 1992;
gen phi_it92x  = phi_it    if year == 1992;
gen phi_et92x  = phi_et    if year == 1992;
gen error92x   = myerror_  if year == 1992;

sort fips class year;
by fips class: egen share92  = max(share92x);
by fips class: egen phi_it92 = max(phi_it92x);
by fips class: egen phi_et92 = max(phi_et92x);
by fips class: egen error92  = max(error92x);

drop share92x phi_it92x phi_et92x error92x;

* COUNTER1: Change phi_et before phi_it;
gen counter1a = exp(phi_it92 + phi_et92 + error92);
gen counter1x = counter1a;
gen counter1b = exp(phi_it92 + phi_et   + error92);
gen counter1c = exp(phi_it   + phi_et   + error92);
gen counter1d = exp(phi_it   + phi_et   + myerror_);

* COUNTER2: Change phi_it before phi_et;
gen counter2a = exp(phi_it92 + phi_et92 + error92);
gen counter2x = counter2a;
gen counter2b = exp(phi_it   + phi_et92 + error92);
gen counter2c = exp(phi_it   + phi_et   + error92);
gen counter2d = exp(phi_it   + phi_et   + myerror_);

sort fips year class;
save ../mid/placeeffect_counters_classes, replace;

***********************************
* Now figure state-level means of the counterfactuals 
* by collapsing education-specific classes
* using the appropriate college shares (either 1992 or actual);

use  ../mid/placeeffect_counters_classes, clear;
collapse (mean) counter1x counter2x [aw=share92], by(fips year);
gen lncounter1x = ln(counter1x);
gen lncounter2x = ln(counter2x);
save ../mid/temp2, replace;

use ../mid/placeeffect_counters_classes, clear;
collapse (mean) counter1a counter1b counter1c counter1d 
                counter2a counter2b counter2c counter2d [aw=thisshare], by(fips year);
gen lncounter1a = ln(counter1a);
gen lncounter1b = ln(counter1b);
gen lncounter1c = ln(counter1c);
gen lncounter1d = ln(counter1d);

gen lncounter2a = ln(counter2a);
gen lncounter2b = ln(counter2b);
gen lncounter2c = ln(counter2c);
gen lncounter2d = ln(counter2d);

merge 1:1 fips year using ../mid/temp2, assert(3);
drop _merge;
erase ../mid/temp2.dta;

* Merge in selected state-wide variables from TWID2 so that we can check our work;
sort fips year;
merge 1:1 fips year using `startingpoint', keepusing(fips year pop lnypph mr_all lnmr_all mr_all_CD mr_all_NC);
keep if _merge == 3;
drop _merge;

save ../mid/placeeffect_counters_states, replace;

*****************************
* Check class-specific and state-wide rates against data from TWID2;

use ../mid/placeeffect_counters_classes, clear;
keep fips year counter1d counter2d class;
reshape wide counter1d counter2d, i(fips year) j(class) string;

order fips year *CD* *NC*;
merge 1:1 fips year using ../mid/placeeffect_counters_states;
keep if _merge == 3;

* note that COUNTER1d and COUNTER2d use actual data throughout, so they should both
* match MR_ALL -- the state-wide mortality rate;
tsline mr_all counter1d counter2d if fips == 36, lp(solid dash) leg(cols(1)) name(ratecheck36, replace);
tsline mr_all counter1d counter2d if fips == 10, lp(solid dash) leg(cols(1)) name(ratecheck10, replace);

assert abs(mr_all - counter1d) < .001 if !missing(mr_all) & !missing(counter1d);
assert abs(mr_all - counter2d) < .001 if !missing(mr_all) & !missing(counter2d);

* same for the education-class specific rates;
assert abs(mr_all_NC - counter1dNC) < .001 if !missing(mr_all_NC) & !missing(counter1dNC);
assert abs(mr_all_NC - counter2dNC) < .001 if !missing(mr_all_NC) & !missing(counter2dNC);

assert abs(mr_all_CD - counter1dCD) < .001 if !missing(mr_all_CD) & !missing(counter1dCD);
assert abs(mr_all_CD - counter2dCD) < .001 if !missing(mr_all_CD) & !missing(counter2dCD);

save ../mid/placeeffect_counters_checks, replace;

*************************************
* Find standard deviations of the state-wide data and the various counterfactuals;

use ../mid/placeeffect_counters_states, clear;
collapse (sd) lncounter1x lncounter1a lncounter1b lncounter1c lncounter1d 
              lncounter2x lncounter2a lncounter2b lncounter2c lncounter2d 
			  lnmr_all [aw = pop], by(year);


la var lncounter1x "1992 Baseline";			
la var lncounter1a "Use Actual College Shares";
la var lncounter1b "Add Actual Non-College Penalty";
la var lncounter1c "Add Actual Place Effect";
la var lncounter1d "Add Actual Error Terms";

la var lncounter2x "1992 Baseline";			
la var lncounter2a "Use Actual College Shares";
la var lncounter2b "Add Actual Place Effect";
la var lncounter2c "Add Actual Non-College Penalty";
la var lncounter2d "Add Actual Error Terms";

la var lnmr_all    "Total Standard Deviation";

***** first graph: variance decomposition of place effects;
tsline lncounter1x lncounter1a lncounter1b lncounter1c lncounter1d lnmr_all,
  lc(navy dkgreen brown cyan khaki pink)
  lp(solid dash long solid solid dash) 
  ti("Adding Non-College Penalty Before Place Effect")
  xlabel(1992(3)2017)
  ylabel(.10(.02).2)
  name(counter1, replace) xti("") leg(ring(0) pos(11) cols(1) region(lstyle(none)));
*gr save ../paper_figures/figure_a4_1.gph, replace;

  gr export ../paper_figures/figure_a4_1.pdf, as(pdf) replace;
  
tsline lncounter2x lncounter2a lncounter2b lncounter2c lncounter2d lnmr_all,
  lc(navy dkgreen brown cyan khaki pink)
  lp(solid dash long solid solid dash) 
  ti("Adding Non-College Penalty After Place Effect")
  xlabel(1992(2)2016)
  ylabel(.10(.02).2)
  name(counter2, replace) xti("") leg(ring(0) pos(11) cols(1) region(lstyle(none)));
  *gr save ../paper_figures/figure_a4_2.gph, replace;

  gr export ../paper_figures/figure_a4_2.pdf, as(pdf) replace;

};

* figure a5: crude mortality rates;
if(figurea5==1){;
use `crudestartingpoint', clear;

* considering two equation system
* mr_it^(C) = phi_t + u_(it)^{C}
* mr_it^(H) = phi_t + phi_et + u_(it)^(H);

* to estimate phi_t, make data long, and then demean by year
* we will do this later
* we first estimate phi_et;

gen lnmr_diff = lnmr_NC - lnmr_CD;
gcollapse (mean) phi_et = lnmr_diff, by(year) fast merge;

*******************************************************************
* Now estimate college shares that force everything to be exact. 
* We do this by finding the CD and NC shares that will generate 
* the state-level mortality rate when these college shares are applied to the 
* education-specific rates for that state. Call this new share variable COLLSHARE2,
* to distinguish it from the existing COLLSHARE
* Note that the results are NOT significantly affected whichever choice is made;

gen double collshare2 = (mr_all - mr_all_NC)/(mr_all_CD - mr_all_NC);

gen double mycheck = collshare2*mr_all_CD + (1-collshare2)*mr_all_NC;
assert abs(mycheck - mr_all) < .000000000001 if !missing(mycheck) & !missing(mr_all);
drop mycheck;

*****************************************************
* Reshape the data into long form; 
keep fips year lnmr_CD lnmr_NC phi_et pop collshare2;
reshape long lnmr_, i(fips year) j(class) string;

replace phi_et = 0 if class == "CD";
gcollapse (mean) phi_t = lnmr_, by(year) fast merge;
gen myerror_ = lnmr_ - phi_t - phi_et;

gen thisshare = 0;
replace thisshare = collshare2   if class == "CD";
replace thisshare = 1-collshare2 if class == "NC";

save ../mid/`prg'_longdata, replace;

use ../mid/`prg'_longdata;

sort fips year class;
gen share92   = thisshare if year == 1992;
gen phi_t92  = phi_t    if year == 1992;
gen phi_et92  = phi_et    if year == 1992;
gen error92   = myerror_  if year == 1992;
gen error92CDonly = myerror_ if year == 1992 | class == "NC";
gen error92NConly = myerror_ if year == 1992 | class == "CD";

sort fips class year;
by fips class: carryforward share92 phi_t92 phi_et92 error92 error92CDonly error92NConly, replace;

* COUNTER1: Change college error before high school error;
gen counter1a = exp(phi_t92 + phi_et92 + error92);
gen counter1x = counter1a;
gen counter1b = exp(phi_t    + phi_et92   + error92);
gen counter1c = exp(phi_t    + phi_et   + error92);
gen counter1d = exp(phi_t    + phi_et   + error92NConly);
gen counter1e = exp(phi_t   + phi_et   + myerror_);

* COUNTER2: Change high school error before college error;
gen counter2a = exp(phi_t92 + phi_et92 + error92);
gen counter2x = counter2a;
gen counter2b = exp(phi_t    + phi_et92   + error92);
gen counter2c = exp(phi_t    + phi_et   + error92);
gen counter2d = exp(phi_t    + phi_et   + error92CDonly);
gen counter2e = exp(phi_t   + phi_et   + myerror_);

* COUNTER3: change phi_t, phi_et after, then college first;
gen counter3a = exp(phi_t92 + phi_et92 + error92);
gen counter3x = counter3a;
gen counter3b = exp(phi_t92    + phi_et92  + error92NConly);
gen counter3c = exp(phi_t92    + phi_et92   + myerror_);
gen counter3d = exp(phi_t    + phi_et92   + myerror_);
gen counter3e = exp(phi_t   + phi_et   + myerror_);

gen counter4a = exp(phi_t92 + phi_et92 + error92);
gen counter4b = exp(phi_t92    + phi_et92  + error92NConly);
gen counter4c = exp(phi_t92    + phi_et92   + myerror_);
gen counter4d = exp(phi_t    + phi_et92   + myerror_);
gen counter4e = exp(phi_t   + phi_et   + myerror_);
gen counter4x = counter4e;

sort fips year class;
save ../mid/`prg'_counters_classes, replace;

***********************************
* Now figure state-level means of the counterfactuals 
* by collapsing education-specific classes
* using the appropriate college shares (either 1992 or actual);

use  ../mid/`prg'_counters_classes, clear;
collapse (mean) counter1x counter2x counter3x counter4a counter4b counter4c counter4d counter4x [aw=share92], by(fips year);
gen lncounter1x = ln(counter1x);
gen lncounter2x = ln(counter2x);
gen lncounter3x = ln(counter3x);
gen lncounter4a = ln(counter4a);
gen lncounter4b = ln(counter4b);
gen lncounter4c = ln(counter4c);
gen lncounter4d = ln(counter4d);
gen lncounter4x = ln(counter4x);
save ../mid/temp1, replace;

use ../mid/`prg'_counters_classes, clear;
collapse (mean) counter1a counter1b counter1c counter1d counter1e 
                counter2a counter2b counter2c counter2d counter2e
		counter3a counter3b counter3c counter3d counter3e counter4e [aw=thisshare], by(fips year);
gen lncounter1a = ln(counter1a);
gen lncounter1b = ln(counter1b);
gen lncounter1c = ln(counter1c);
gen lncounter1d = ln(counter1d);
gen lncounter1e = ln(counter1e);

gen lncounter2a = ln(counter2a);
gen lncounter2b = ln(counter2b);
gen lncounter2c = ln(counter2c);
gen lncounter2d = ln(counter2d);
gen lncounter2e = ln(counter2e);

gen lncounter3a = ln(counter3a);
gen lncounter3b = ln(counter3b);
gen lncounter3c = ln(counter3c);
gen lncounter3d = ln(counter3d);
gen lncounter3e = ln(counter3e);

gen lncounter4e = ln(counter4e);

merge 1:1 fips year using ../mid/temp1, assert(3);
drop _merge;
erase ../mid/temp1.dta;

* Merge in selected state-wide variables from TWID2 so that we can check our work;
sort fips year;
merge 1:1 fips year using `crudestartingpoint', keepusing(fips year lnypph mr_all lnmr_all mr_all_CD mr_all_NC pop);
keep if _merge == 3;
drop _merge;

save ../mid/`prg'_counters_states, replace;

*****************************
* Check class-specific and state-wide rates against data from TWID2;

use ../mid/`prg'_counters_classes, clear;
keep fips year counter1e counter2e counter3e counter4e class;
reshape wide counter1e counter2e counter3e counter4e, i(fips year) j(class) string;

order fips year *CD* *NC*;
merge 1:1 fips year using ../mid/`prg'_counters_states;
keep if _merge == 3;

* note that COUNTER1d and COUNTER2d use actual data throughout, so they should both
* match MR_ALL -- the state-wide mortality rate;
tsline mr_all counter1e counter2e counter3e counter4e if fips == 36, lp(solid dash) leg(cols(1)) name(ratecheck36, replace);
tsline mr_all counter1e counter2e counter3e counter4e if fips == 10, lp(solid dash) leg(cols(1)) name(ratecheck10, replace);

assert abs(mr_all - counter1e) < .001 if !missing(mr_all) & !missing(counter1e);
assert abs(mr_all - counter2e) < .001 if !missing(mr_all) & !missing(counter2e);
assert abs(mr_all - counter3e) < .001 if !missing(mr_all) & !missing(counter3e);
assert abs(mr_all - counter4e) < .001 if !missing(mr_all) & !missing(counter4e);


* same for the education-class specific rates;
assert abs(mr_all_NC - counter1eNC) < .001 if !missing(mr_all_NC) & !missing(counter1eNC);
assert abs(mr_all_NC - counter2eNC) < .001 if !missing(mr_all_NC) & !missing(counter2eNC);
assert abs(mr_all_NC - counter3eNC) < .001 if !missing(mr_all_NC) & !missing(counter3eNC);
assert abs(mr_all_NC - counter4eNC) < .001 if !missing(mr_all_NC) & !missing(counter4eNC);

assert abs(mr_all_CD - counter1eCD) < .001 if !missing(mr_all_CD) & !missing(counter1eCD);
assert abs(mr_all_CD - counter2eCD) < .001 if !missing(mr_all_CD) & !missing(counter2eCD);
assert abs(mr_all_CD - counter3eCD) < .001 if !missing(mr_all_CD) & !missing(counter3eCD);
assert abs(mr_all_CD - counter4eCD) < .001 if !missing(mr_all_CD) & !missing(counter4eCD);

save ../mid/`prg'_counters_checks, replace;

*************************************
* Find standard deviations of the state-wide data and the various counterfactuals;

use ../mid/`prg'_counters_states, clear;
collapse (sd) lncounter1x lncounter1a lncounter1b lncounter1c lncounter1d lncounter1e
              lncounter2x lncounter2a lncounter2b lncounter2c lncounter2d lncounter2e
	      lncounter3x lncounter3a lncounter3b lncounter3c lncounter3d lncounter3e
	      lncounter4a lncounter4b lncounter4c lncounter4d lncounter4x lncounter4e
			  lnmr_all [aw = pop], by(year);

* education only model, decomposition of variance;
la var lncounter1x "1992 Baseline";			
la var lncounter1a "Add Actual College Shares";
la var lncounter1c "Add Actual Non-College Penalty";
la var lncounter1d "Add Actual College Residuals";
la var lncounter1e "Add Actual Non-College Residuals";

la var lncounter2x "1992 Baseline";			
la var lncounter2a "Add Actual College Shares";
la var lncounter2c "Add Actual Non-College Penalty";
la var lncounter2d "Add Actual Non-College Residuals";
la var lncounter2e "Add Actual College Residuals";

la var lncounter3x "1992 Baseline";			
la var lncounter3a "Add Actual College Shares";
la var lncounter3b "Add Actual College Residuals";
la var lncounter3c "Add Actual Non-College Residuals";
la var lncounter3e "Add Actual Non-College Penalty";

la var lncounter4a "1992 Baseline";
la var lncounter4b "Add Actual College Residuals";			
la var lncounter4c "Add Actual Non-College Residuals";
la var lncounter4x "Add Actual Non-College Penalty";
la var lncounter4e "Add Actual College Shares";

la var lnmr_all    "Total Standard Deviation";
 tsline lncounter1x lncounter1a /*lncounter2b*/ lncounter1c lncounter1d lncounter1e lnmr_all,
  lc(navy cranberry brown cyan khaki pink)
  lp(solid dash long solid solid dash dash_dot) 
  ti("Identical Order as in Figure 4, Crude Mortality Rates", size(medsmall))
  xlabel(1992(3)2016)
  ylabel(.14(.02).2)
  name(counter2, replace) xti("") leg(ring(0) pos(11) cols(1) region(lstyle(none)));
  *gr save ../paper_figures/figure_a5_1.gph, replace;
gr export ../paper_figures/figure_a5_1.pdf, as(pdf) replace;

tsline lncounter2x lncounter2a /*lncounter2b*/ lncounter2c lncounter2d lncounter2e lnmr_all,
  lc(navy cranberry brown cyan khaki pink)
  lp(solid dash long solid solid dash dash_dot) 
  ti("Changing Non-College Residuals Before College Residuals", size(medsmall))
  xlabel(1992(3)2016)
  ylabel(.14(.02).2)
  name(counter2, replace) xti("") leg(ring(0) pos(11) cols(1) region(lstyle(none)));

*gr save ../paper_figures/figure_a5_2.gph, replace;
gr export ../paper_figures/figure_a5_2.pdf, as(pdf) replace;

tsline lncounter3x lncounter3a lncounter3b lncounter3c /*lncounter3d*/ lncounter3e lnmr_all,
  lc(navy cranberry brown cyan khaki pink)
  lp(solid dash long solid solid dash dash_dot) 
  ti("Changing National Non-College Penalty After Residuals", size(medsmall))
  xlabel(1992(3)2016)
  ylabel(.14(.02).2)
  name(counter3, replace) xti("") leg(ring(0) pos(11) cols(1) region(lstyle(none)));
*gr save ../paper_figures/figure_a5_3.gph, replace;
gr export ../paper_figures/figure_a5_3.pdf, as(pdf) replace;

tsline lncounter4a lncounter4b lncounter4c /*lncounter4d*/ lncounter4x lncounter4e lnmr_all,
  lc(navy cranberry brown cyan khaki pink)
  lp(solid dash long solid solid dash dash_dot) 
  ti("Changing College Share and Non-College Penalty After Residuals", size(medsmall))
  xlabel(1992(3)2016)
  ylabel(.14(.02).2)
  name(counter4, replace) xti("") leg(ring(0) pos(11) cols(1) region(lstyle(none)));
*gr save ../paper_figures/figure_a5_4.gph, replace;
gr export ../paper_figures/figure_a5_4.pdf, as(pdf) replace;
clear;
};

* figure a6: place effect model, crude;
if(figurea6==1){;
use `crudestartingpoint', clear;
* Here is the "high-school minus college mortality rate
* regression" estimated in difference form (Non-college - college)
* One observation per state and year. MR here stands for "mortality rate", 
* CD = college degree and NC stands for non-college. The 
* year dummies here will identify the yearly "education effect".
* 
* This regression has one observation for each state and year;

gen lnmr_diff = lnmr_NC - lnmr_CD;
reg lnmr_diff i.year;

* Estimate phi_et's and phi_it's;
reg lnmr_diff i.year, cluster(fips);
predict phi_et, xb;
gen lnmr_NC_twid = lnmr_NC - phi_et;
gen phi_it = .5*(lnmr_NC_twid + lnmr_CD);

gen myerror_NC = lnmr_NC - phi_it - phi_et;
gen myerror_CD = lnmr_CD - phi_it;

*******************************************************************
* Now estimate college shares that force everything to be exact. 
* We do this by finding the CD and NC shares that will generate 
* the state-level mortality rate when these college shares are applied to the 
* education-specific rates for that state. Call this new share variable COLLSHARE2,
* to distinguish it from the existing COLLSHARE;
gen double collshare2 = (mr_all - mr_all_NC)/(mr_all_CD - mr_all_NC);

gen double mycheck = collshare2*mr_all_CD + (1-collshare2)*mr_all_NC;
assert abs(mycheck - mr_all) < .000000000001 if !missing(mycheck) & !missing(mr_all);
drop mycheck;

*****************************************************
* Reshape the data into long form; 
keep fips year lnmr_CD lnmr_NC myerror_CD myerror_NC phi_et phi_it pop collshare2;
reshape long lnmr_ myerror_, i(fips year) j(class) string;
replace phi_et = 0 if class == "CD";
gen thisshare = 0;
replace thisshare = collshare2   if class == "CD";
replace thisshare = 1-collshare2 if class == "NC";
save ../mid/placeeffect_longdata, replace;

**************************************************
* Counterfactual exercises ("counters");

use ../mid/placeeffect_longdata;

sort fips year class;
gen share92x   = thisshare if year == 1992;
gen phi_it92x  = phi_it    if year == 1992;
gen phi_et92x  = phi_et    if year == 1992;
gen error92x   = myerror_  if year == 1992;

sort fips class year;
by fips class: egen share92  = max(share92x);
by fips class: egen phi_it92 = max(phi_it92x);
by fips class: egen phi_et92 = max(phi_et92x);
by fips class: egen error92  = max(error92x);

drop share92x phi_it92x phi_et92x error92x;

* COUNTER1: Change phi_et before phi_it;
gen counter1a = exp(phi_it92 + phi_et92 + error92);
gen counter1x = counter1a;
gen counter1b = exp(phi_it92 + phi_et   + error92);
gen counter1c = exp(phi_it   + phi_et   + error92);
gen counter1d = exp(phi_it   + phi_et   + myerror_);

* COUNTER2: Change phi_it before phi_et;
gen counter2a = exp(phi_it92 + phi_et92 + error92);
gen counter2x = counter2a;
gen counter2b = exp(phi_it   + phi_et92 + error92);
gen counter2c = exp(phi_it   + phi_et   + error92);
gen counter2d = exp(phi_it   + phi_et   + myerror_);

sort fips year class;
save ../mid/placeeffect_counters_classes, replace;

***********************************
* Now figure state-level means of the counterfactuals 
* by collapsing education-specific classes
* using the appropriate college shares (either 1992 or actual);

use  ../mid/placeeffect_counters_classes, clear;
collapse (mean) counter1x counter2x [aw=share92], by(fips year);
gen lncounter1x = ln(counter1x);
gen lncounter2x = ln(counter2x);
save ../mid/temp2, replace;

use ../mid/placeeffect_counters_classes, clear;
collapse (mean) counter1a counter1b counter1c counter1d 
                counter2a counter2b counter2c counter2d [aw=thisshare], by(fips year);
gen lncounter1a = ln(counter1a);
gen lncounter1b = ln(counter1b);
gen lncounter1c = ln(counter1c);
gen lncounter1d = ln(counter1d);

gen lncounter2a = ln(counter2a);
gen lncounter2b = ln(counter2b);
gen lncounter2c = ln(counter2c);
gen lncounter2d = ln(counter2d);

merge 1:1 fips year using ../mid/temp2, assert(3);
drop _merge;
erase ../mid/temp2.dta;

* Merge in selected state-wide variables from TWID2 so that we can check our work;
sort fips year;
merge 1:1 fips year using `crudestartingpoint', keepusing(fips year pop lnypph mr_all lnmr_all mr_all_CD mr_all_NC);
keep if _merge == 3;
drop _merge;

save ../mid/placeeffect_counters_states, replace;

*****************************
* Check class-specific and state-wide rates against data from TWID2;

use ../mid/placeeffect_counters_classes, clear;
keep fips year counter1d counter2d class;
reshape wide counter1d counter2d, i(fips year) j(class) string;

order fips year *CD* *NC*;
merge 1:1 fips year using ../mid/placeeffect_counters_states;
keep if _merge == 3;

* note that COUNTER1d and COUNTER2d use actual data throughout, so they should both
* match MR_ALL -- the state-wide mortality rate;
tsline mr_all counter1d counter2d if fips == 36, lp(solid dash) leg(cols(1)) name(ratecheck36, replace);
tsline mr_all counter1d counter2d if fips == 10, lp(solid dash) leg(cols(1)) name(ratecheck10, replace);

assert abs(mr_all - counter1d) < .001 if !missing(mr_all) & !missing(counter1d);
assert abs(mr_all - counter2d) < .001 if !missing(mr_all) & !missing(counter2d);

* same for the education-class specific rates;
assert abs(mr_all_NC - counter1dNC) < .001 if !missing(mr_all_NC) & !missing(counter1dNC);
assert abs(mr_all_NC - counter2dNC) < .001 if !missing(mr_all_NC) & !missing(counter2dNC);

assert abs(mr_all_CD - counter1dCD) < .001 if !missing(mr_all_CD) & !missing(counter1dCD);
assert abs(mr_all_CD - counter2dCD) < .001 if !missing(mr_all_CD) & !missing(counter2dCD);

save ../mid/placeeffect_counters_checks, replace;

*************************************
* Find standard deviations of the state-wide data and the various counterfactuals;

use ../mid/placeeffect_counters_states, clear;
collapse (sd) lncounter1x lncounter1a lncounter1b lncounter1c lncounter1d 
              lncounter2x lncounter2a lncounter2b lncounter2c lncounter2d 
			  lnmr_all [aw = pop], by(year);


la var lncounter1x "1992 Baseline";			
la var lncounter1a "Use Actual College Shares";
la var lncounter1b "Add Actual Non-College Penalty";
la var lncounter1c "Add Actual Place Effect";
la var lncounter1d "Add Actual Error Terms";

la var lncounter2x "1992 Baseline";			
la var lncounter2a "Use Actual College Shares";
la var lncounter2b "Add Actual Place Effect";
la var lncounter2c "Add Actual Non-College Penalty";
la var lncounter2d "Add Actual Error Terms";

la var lnmr_all    "Total Standard Deviation";

***** first graph: variance decomposition of place effects;
tsline lncounter1x lncounter1a lncounter1b lncounter1c lncounter1d lnmr_all,
  lc(navy dkgreen brown cyan khaki pink)
  lp(solid dash long solid solid dash) 
  ti("Adding Non-College Penalty Before Place Effect")
  xlabel(1992(3)2017)
  ylabel(.14(.02).2)
  leg(size(small))
  name(counter1, replace) xti("") leg(ring(0) pos(11) cols(1) region(lstyle(none)));
*gr save ../paper_figures/figure_a6_1.gph, replace;

  gr export ../paper_figures/figure_a6_1.pdf, as(pdf) replace;
  
tsline lncounter2x lncounter2a lncounter2b lncounter2c lncounter2d lnmr_all,
  lc(navy dkgreen brown cyan khaki pink)
  lp(solid dash long solid solid dash) 
  ti("Adding Non-College Penalty After Place Effect")
  xlabel(1992(2)2016)
  ylabel(.14(.02).2)
  leg(size(small))
  name(counter2, replace) xti("") leg(ring(0) pos(11) cols(1) region(lstyle(none)));
  *gr save ../paper_figures/figure_a6_2.gph, replace;

  gr export ../paper_figures/figure_a6_2.pdf, as(pdf) replace;

};

* table a1: regressions of mortality on income and other covariates;
if(tablea1==1){;
use `startingpoint', clear;
merge m:1 fips using ../mid/prescription_quality, assert(2 3) keep(3);
* fill in obese, smoker ;
bys fips: mipolate obese year, generate(obeseip) e;
replace obese = obeseip if mi(obese) & inlist(fips, 5, 56);

bys fips: mipolate smoker year, generate(smokerip) e;
replace smoker = smokerip if mi(smoker) & inlist(fips, 5, 56);

gen lnmr_1992 = lnmr_all if year==1992;
gen lnmr_2000 = lnmr_all if year==2000;
gen lnmr_2016 = lnmr_all if year==2016;
rename lnypph lnrealpcincome;

** income alone;
reg lnmr_1992 lnrealpcincome if year==1992 /*[aw=pop]*/, cluster(fips);
estimates store reg1;

** add everything else;
reg lnmr_1992 lnrealpcincome shrpov smoker obese good_prescription risky_prescription manufacturingshare if year==1992 /*[aw=pop]*/, cluster(fips);
estimates store reg2;

** income alone;
reg lnmr_2000 lnrealpcincome if year==2000 /*[aw=pop]*/, cluster(fips);
estimates store reg3;

** add everything else;
reg lnmr_2000 lnrealpcincome shrpov smoker obese good_prescription risky_prescription manufacturingshare if year==2000 /*[aw=pop]*/, cluster(fips);
estimates store reg4;


** income alone;
reg lnmr_2016 lnrealpcincome if year==2016 /*[aw=pop]*/, cluster(fips);
estimates store reg5;

** add everything else;
reg lnmr_2016 lnrealpcincome shrpov smoker obese good_prescription risky_prescription manufacturingshare if year ==2016 /*[aw=pop]*/, cluster(fips);
estimates store reg6;




esttab reg1 reg2 reg3 reg4 reg5 reg6 using ../paper_figures/regressions_yearly.tex, replace
          b se obslast 
         sca(    "r2 R-squared") 
		 sfmt(%5.3f) keep(lnrealpcincome shrpov smoker obese manufacturingshare good_prescription risky_prescription) order(lnrealpcincome shrpov smoker obese manufacturingshare good_prescription risky_prescription);
};

gr close _all;
