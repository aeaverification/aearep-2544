clear all
pause on
set rmsg on 
set scheme s1color


scalar remaketemp = 1
* process the .xpt files to create temporary stata files, which we will start from
* all raw files are downloaded directly from https://www.cdc.gov/brfss/annual_data/annual_data.htm
if(remaketemp==1){
forvalues y = 2011 / 2019 {
	cap copy "../raw/brfss/LLCP`y'.XPT " "../raw/brfss/LLCP`y'.XPT", replace
	if !_rc erase "../raw/brfss/LLCP`y'.XPT "
}

forvalues y = 2011 / 2018 {
	confirm file ../raw/brfss/LLCP`y'.XPT
}

forvalues yy = 1 / 10 {
	if `yy' < 10 local yy 0`yy'
	cap confirm file ../raw/brfss/CDBRFS`yy'.XPT
	if _rc confirm file ../raw/brfss/cdbrfs`yy'.xpt
}

forvalues yy = 84 / 100 {
	if `yy' == 100 local yy 00
	cap confirm file ../raw/brfss/CDBRFS`yy'.XPT
	if _rc confirm file ../raw/brfss/cdbrfs`yy'.xpt
}

forvalues yy = 84 / 100 {
	local y = 19`yy'
	if `yy' == 100 {
		local yy 00
		local y 2000
	}
	cap import sasxport5 ../raw/brfss/CDBRFS`yy'.XPT, clear 
	if _rc import sasxport5 ../raw/brfss/cdbrfs`yy'.xpt, clear 
	foreach v of varlist _* {
		local s `=substr("`v'", 2, 32)'
		cap rename `v' `s'
	}
	save ../raw/brfss/temp/`y', replace	
}

forvalues yy = 1 / 10 {
	if `yy' < 10 local yy 0`yy'
	local y = 20`yy'
	cap import sasxport5 ../raw/brfss/CDBRFS`yy'.XPT, clear 
	if _rc import sasxport5 ../raw/brfss/cdbrfs`yy'.xpt, clear 
	foreach v of varlist _* {
		local s `=substr("`v'", 2, 32)'
		cap rename `v' `s'
	}
	save ../raw/brfss/temp/20`yy', replace	
}

forvalues y = 2011 / 2019 {
	import sasxport5 ../raw/brfss/LLCP`y'.XPT, clear 
	foreach v of varlist _* {
		local s `=substr("`v'", 2, 32)'
		cap rename `v' `s'
	}
	save ../raw/brfss/temp/`y', replace 
}
}


local wt finalwt

* note that we process to get the following:
* smoker: share who smoke every day
* binge: share who binge drink last month
* obese: share who are obese
* overweight: share who are  overweight or obese
* inactive: share with no exercise in the last 30 days

* 1984-1986
forvalues y = 1984 / 1986 {
	use ../raw/brfss/temp/`y', clear 
	
	rename smoker smoker3 // desired name already taken 
	gen byte smoker = smoker3 == 1
	replace smoker = . if smoker3 == . | smoker3 == 9	
	
	gen byte binge = .
	replace binge = 0 if drinkany == 2
	replace binge = 0 if drinkge5 == 88
	replace binge = 1 if inrange(drinkge5, 1, 76)
	
	* not doing obese, overweight for these years
	/*if `y' != 2000 {
		gen byte overweight = rfwhbmi == 2
		replace overweight = . if rfwhbmi == . | rfwhbmi == 9
	}
	else gen byte overweight = .*/
	
	*cap rename bmi2 bmi
	*gen byte obese = bmi >= 300
	*gen byte overweight = bmi >= 250
	*replace obese = . if bmi == 999 | mi(bmi)
	*replace overweight = . if bmi == 999 | mi(bmi)
	gen obese = .
	gen overweight = .
	
	gen inactive = exerany == 2
	replace inactive = . if exerany == . | exerany == 9 | exerany == 7
	
	keep if state <= 56 & !inlist(state, 2, 15)
	gcollapse (mean) smoker binge obese overweight inactive (rawsum) wt = `wt' [aw=`wt'], by(state) fast 
	replace wt = int(wt)

	gen year = `y'
	rename state fipstate 
	
	compress
	tempfile `y'
	save ``y''
}
forvalues y = 1987 / 2000 {
	use ../raw/brfss/temp/`y', clear 
	
	rename smoker smoker3 // desired name already taken 
	gen byte smoker = smoker3 == 1
	replace smoker = . if smoker3 == . | smoker3 == 9
	gen byte binge = .
	replace binge = 0 if drinkany == 2
	replace binge = 0 if drinkge5 == 88
	replace binge = 1 if inrange(drinkge5, 1, 76)
	
	/*if `y' != 2000 {
		gen byte overweight = rfwhbmi == 2
		replace overweight = . if rfwhbmi == . | rfwhbmi == 9
	}
	else gen byte overweight = .*/
	
	cap rename bmi2 bmi
	gen byte obese = bmi >= 300
	gen byte overweight = bmi >= 250
	replace obese = . if bmi == 999 | mi(bmi)
	replace overweight = . if bmi == 999 | mi(bmi)
	
	gen inactive = exerany == 2
	replace inactive = . if exerany == . | exerany == 9 | exerany == 7
	
	keep if state <= 56 & !inlist(state, 2, 15)
	gcollapse (mean) smoker binge obese overweight inactive (rawsum) wt = `wt' [aw=`wt'], by(state) fast 
	replace wt = int(wt)

	gen year = `y'
	rename state fipstate 
	compress
	tempfile `y'
	save ``y''
}


local wt finalwt
forvalues y = 2001 / 2010 {
	use ../raw/brfss/temp/`y', clear 
	
	cap rename smoker2 smoker3 
	cap rename bmi2cat bmi4cat 
	cap rename bmi3cat bmi4cat
	
	*despite name changes, this variable has 3 and only 3 valid values 
	assert inlist(bmi4cat, 1, 2, 3, 9), fast
	foreach v in 1 2 3 9 {
		qui count if bmi4cat == `v'
		assert r(N) > 0
	}
	
	gen byte smoker = smoker3 == 1
	replace smoker = . if smoker3 == . | smoker3 == 9
	
	cap rename drnk2ge5 drnk3ge5
	confirm variable drnk3ge5 
	*gen binge = 0
	gen binge = .
	replace binge = 0 if drnkany == 2
	replace binge = 0 if alcday == 888 
	replace binge = 0 if drnk3ge5 == 88	
	replace binge = 1 if inrange(drnk3ge5, 1, 76)
	count
	count if binge & !mi(binge)
	count if !binge
	count if mi(binge)
	
	sum binge [aw=finalwt]
	
	/*gen byte obese = bmi4cat == 3 
	replace obese = . if bmi4cat == . | bmi4cat == 9
	
	gen byte overweight = inlist(bmi4cat, 2, 3)
	replace overweight = . if bmi4cat == . | bmi4cat == 9*/
	
	cap rename bmi2 bmi
	cap rename bmi3 bmi
	cap rename bmi4 bmi
	
	if `y' == 2001 replace bmi = bmi / 100 // 2001 has 4 rather than 2 implied decimals
	gen byte obese = bmi >= 3000
	gen byte overweight = bmi >= 2500
	replace obese = . if bmi == 9999 | mi(bmi)
	replace overweight = . if bmi == 9999 | mi(bmi)
	
	gen byte inactive = totinda == 2
	replace inactive = . if totinda == . | totinda == 9	

	keep if state <= 56 & !inlist(state, 2, 15)
	gcollapse (mean) smoker binge obese overweight inactive (rawsum) wt = `wt' [aw=`wt'], by(state) fast 
	replace wt = int(wt)

	gen year = `y'
	rename state fipstate 
	
	compress
	tempfile `y'
	save ``y''
	assert !mi(binge) if year >= 2003 
}

* use the right brfss wt
local wt llcpwt
forvalues y = 2011 / 2019 {
	use ../raw/brfss/temp/`y', clear 

	gen byte smoker = smoker3 == 1
	replace smoker = . if smoker3 == . | smoker3 == 9

	*gen byte binge = 0
	gen byte binge = .
	replace binge = 0 if drnkany == 2
	replace binge = 0 if alcday == 888 
	replace binge = 0 if drnk3ge5 == 88
	replace binge = 1 if inrange(drnk3ge5, 1, 76)

	count
	count if binge & !mi(binge)
	count if !binge
	count if mi(binge)
	sum binge [aw=llcpwt]

	/*gen byte obese = bmi5cat == 4
	replace obese = . if bmi5cat == .

	gen byte overweight = inlist(bmi5cat, 3, 4)
	replace overweight = . if bmi5cat == . | bmi5cat == 9*/
	
	cap rename bmi5 bmi 
	gen byte obese = bmi >= 3000
	gen byte overweight = bmi >= 2500
	replace obese = . if bmi == 9999 | mi(bmi)
	replace overweight = . if bmi == 9999 | mi(bmi)
	
	gen byte inactive = totinda == 2
	replace inactive = . if totinda == 9

	keep if state <= 56 & !inlist(state, 2, 15)
	gcollapse (mean) smoker binge obese overweight inactive (rawsum) wt = `wt' [aw=`wt'], by(state) fast 
	replace wt = int(wt)

	gen year = `y'
	rename state fipstate 
	
	compress
	tempfile `y'
	save ``y''	
}

clear
forvalues y = 1984 / 2019 {
	append using ``y''
}

* make graphs of these over time to make sure they look smooth 
preserve
gcollapse (mean) smoker binge obese overweight inactive [aw=wt], by(year)
line smoker binge obese overweight inactive year, xti("") ///
lc(cranberry orange green navy purple) lp(solid dash solid dash solid) ///
legend(order(1 "share smoke every day" 2 "share binge drink last month" ///
3 "share obese" 4 "share overweight or obese" 5 "share no exercise last 30 days")) ///
ti("Key BRFSS Indicators") xlab(1990(5)2020)
graph export ../figures/brfss_indicators.pdf, replace
restore

statastates, fips(fipstate) 
drop if _merge == 2
assert _merge == 3
drop _merge 

label variable smoker "share smoke every day"
label variable binge "share binge drink last month"
label variable obese "share obese"
label variable overweight "share overweight or obese"
label variable inactive "share no exercise last 30 days"
label variable wt "sum of weights (BRFSS)"
compress
label data "key BRFSS indicators"
save ../mid/brfss_state, replace 

