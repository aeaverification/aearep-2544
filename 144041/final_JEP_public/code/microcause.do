clear all
pause on
set rmsg on 
set scheme s1color

* use the file generated now by our .py file
use ../mid/microcausepre, replace

* check that there is no overlap between chapters for underlying cause data
egen byte take = rowtotal(chap1-chap18)
assert inlist(take, 0, 1), fast

* reorder for later use
order poison_op* poison_unspec poison_notop* poison, before(cirrhosis)

* for now, we're dropping method 2 and 3 causes
drop *_2
drop *_3
save ../mid/microcause, replace
