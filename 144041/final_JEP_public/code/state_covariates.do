clear all
pause on
set rmsg on 
set scheme s1color

use ../mid/bea_income, clear 
rename fips fipstate
keep fipstate ypph year
label variable ypph "real per-capita income ($2012)"
tempfile income
save `income'

use ../mid/statemanufacturingshare1969-2018, clear
rename fips fipstate
tempfile manufacturing
save `manufacturing'

use ../mid/brfss_state, clear
drop if year == 2018 

* merge all of the covariates, make sure that the merges go generally as expected
* income has a few more years, keep those; income is the variable that is going into all of our analysis, so keep specifically for the years income is present
merge 1:1 year fipstate using `income', assert(2 3) noreport nogenerate

* keep only those years that are covered by income
merge 1:1 year fipstate using ../mid/state_poverty, keep(1 3) noreport nogenerate
*merge 1:1 year fipstate using ../mid/medicaid_expenditure, keep(1 3) noreport nogenerate
*merge 1:1 fipstate year using ../mid/statelevelozonepm25, assert(1 3) nogenerate
*merge 1:1 fipstate year using ../mid/statehealthinsurance, assert(1 3) nogenerate
merge 1:1 fipstate year using `manufacturing', assert(1 3) noreport nogenerate
*merge 1:1 fipstate year using ../mid/statepopdensity, assert(1 3) noreport nogenerate
merge 1:1 year fipstate using ../mid/statecollegeshareinterpolated, keep(1 3) noreport nogenerate
merge 1:1 year fipstate using ../mid/statecollegeshare, keep(1 3) noreport nogenerate
*merge 1:1 year fipstate using ../mid/frank_gini, keep(1 3) noreport nogenerate

label data "state-level covariates"
compress
hashsort year fipstate
save ../mid/state_covariates, replace 
