clear all
pause on
set rmsg on 
set scheme s1color

scalar simple_imputation = 1
if(simple_imputation==1){
*open microdata, merge county data, execute simple imputation for rarely missing vars 
use ../mid/microcause, clear
keep if inrange(year, 1990, 2017)

hashsort year fipstateocc confipsctyocc
compress

*define program for simple imputation - randomly assign obs to categories based
*on distribution of categorical var 
cap program drop simple_impute 
program define simple_impute
	syntax varlist, VALues(numlist) MISSval(integer)
	
	gen byte mi`varlist' = `varlist' == `missval'
	qui count if !mi`varlist'
	local N = r(N)
	replace `varlist' = . if mi`varlist'
	
	local sum = 0
	foreach i in `values' {
		local sumshr`i' = `sum' / `N'
		qui count if `varlist' == `i'	
		local sum = `sum' + r(N)
	}
	
	gen rnd = runiform()
	foreach i in `values' {
		replace `varlist' = `i' if rnd > `sumshr`i'' & mi`varlist'	
	}
	drop mi`varlist' rnd
end

simple_impute dow, val(1/7) miss(9)
simple_impute race, val(0/3) miss(9)
simple_impute marstat, val(1/4) miss(9)
simple_impute age, val(0/130) miss(999)

replace poison_op = . if poison_unspec
replace hisp = . if hisp == 9 

*determine which state-years will have educ89 or educ03 imputed
*if there are any deaths under 03 scheme then all missing in that state-year will have 03 imputed
*if there are any deaths under 89 scheme and NO deaths under 03 scheme then all missing in that state-year will have 89 imputed
gen byte deaths03 = cond(educ03 < 9, 1, 0)
gen byte deaths89 = cond(educ89 < 99, 1, 0)
gcollapse (sum) deaths*, by(year fipstateocc) fast merge replace 
gen byte use03 = deaths03 > 0
gen byte use89 = deaths89 > 0 
replace use89 = 0 if use03 // if both, use only 03
replace use03 = 1 if !(use03 | use89) // if neither, use 03
assert use89 | use03, fast 

replace educ89 = . if educ89 == 99 
replace educ03 = . if educ03 == 9 

keep id poison poison_op educ89 educ03 use89 use03 educf hisp fipstateocc year month dow age sex race marstat chap*
drop chap1 // default in regressions
compress
save ../mid/temp2, replace 
}

scalar beforemerge = 1
if(beforemerge==1){
* merge together imputed data
use ../mid/temp2, clear
keep dow hisp race age marstat id educ03 educ89
save ../mid/temp3, replace

use ../mid/microcause, clear
drop dow hisp race age marstat educ03 educ89
merge 1:1 id using ../mid/temp3
drop if inlist(fipstateres, 2, 11, 15)
drop if year == 1989
assert _merge == 3
rename confipsctyres oldfipscty
rename fipstateres fipstate
drop _merge

gen educmisscheck = educ89 == . & educ03 == .

* similar to education in pull, define two-level education.  we will impute educ2 non-randomly based on state-race-age4-sex cells
* we could do this with a random seed, but because our aim is just to construct the eventual mortality rates, we do not
label define educ2 1 "Less than college" 3 "College or more"
gen byte educ2 = .
replace educ2 = 1 if (educ89 <= 12 & !mi(educ89)) | (educ03 <= 3 & !mi(educ03))
replace educ2 = 1 if (inrange(educ89, 13, 15) & !mi(educ89)) | (inlist(educ03, 4, 5) & !mi(educ03))
replace educ2 = 2 if (educ89 >= 16 & !mi(educ89)) | (educ03 >= 6 & !mi(educ03))
gen educmiss = educ2 == .
assert educmiss == educmisscheck
drop educmisscheck
keep year fipstate educ2 educmiss age race hisp sex circulatory poison cirrhosis suicide diabetes cerebrovasc lowerresp cancer flupneum
save ../mid/priorstateimpute, replace
}

** imputation, all deaths 
use ../mid/priorstateimpute, clear
* states without education recorded
keep if year >=1992
drop if inlist(fipstate, 13, 40, 44, 46) 
* create two race categories (smaller races create too small cells)
gen race2 = 0
replace race2 = 1 if race>=1
drop race
gen count = 1
gen educ2count = 1 if educ2 == 2

* create four age categories for our 25-64 range
gen byte age4 = . 
replace age4 = 1 if inrange(age, 25, 34)
replace age4 = 2 if inrange(age, 35, 44)
replace age4 = 3 if inrange(age, 45, 54)
replace age4 = 4 if inrange(age, 55, 64)
drop if mi(age4)
drop age

* collapse down to cells, and get the percentage of college in each cell
gcollapse (sum) educmiss educ2count count, by(fipstate year age4 race2 sex)
gen how_much_missing  = educmiss/count
gen pctcolldeath = (educ2count)/(count - educmiss)
*replace pctcolldeath = . if mi(how_much_missing) | how_much_missing > 0.15
keep fipstate year age4 race2 sex how_much_missing pctcolldeath educmiss educ2count count
gen pctcolldeath_ref = pctcolldeath

* check that cells with a lot missing are very small
levelsof fips, local(fiplist)
assert count==1 if how_much_missing==1
replace pctcolldeath = 0 if how_much_missing == 1
assert !mi(pctcolldeath)

keep fipstate year age4 race2 sex pctcolldeath pctcolldeath_ref

drop pctcolldeath_ref
ren pctcolldeath pctcolldeath_racesex
tempfile cellsstateracesex
save `cellsstateracesex', replace

use ../mid/priorstateimpute, clear
* states without education recorded
keep if year >=1992
drop if inlist(fipstate, 13, 40, 44, 46) 
gen race2 = 0
replace race2 = 1 if race>=1
drop race
gen byte age4 = . 
replace age4 = 1 if inrange(age, 25, 34)
replace age4 = 2 if inrange(age, 35, 44)
replace age4 = 3 if inrange(age, 45, 54)
replace age4 = 4 if inrange(age, 55, 64)
drop if mi(age4)
drop age

* get only the number of deaths with missing education, in each cell
gcollapse (sum) educmiss, by(year fipstate age4 race2 sex)
fmerge 1:1 fipstate year age4 race2 sex using `cellsstateracesex'
expand 2
bys year fipstate age4 race2 sex: gen educ2 = _n
gen new_deaths = .
assert !mi(educmiss)

* calculate the number of new deaths that we should add to non-college, college
replace new_deaths = pctcolldeath * educmiss if educ2 == 2
replace new_deaths = (1-pctcolldeath) * educmiss if educ2 == 1
assert new_deaths >=0
keep year fipstate age4 race2 sex educ2 new_deaths
tempfile newdeaths
save `newdeaths', replace

use ../mid/priorstateimpute, clear
* states without education recorded
keep if year >=1992
drop if inlist(fipstate, 13, 40, 44, 46) 
gen race2 = 0
replace race2 = 1 if race>=1
drop race
gen count = 1
gen educ2count = 1 if educ2 == 2
gen byte age4 = . 
replace age4 = 1 if inrange(age, 25, 34)
replace age4 = 2 if inrange(age, 35, 44)
replace age4 = 3 if inrange(age, 45, 54)
replace age4 = 4 if inrange(age, 55, 64)
drop if mi(age4)
drop age
gen deaths = 1
gcollapse (sum) deaths, by(year fipstate age4 educ2 race2 sex)

* merge on the new deaths, then add them to the existing totals
fmerge 1:1 fipstate year age4 race2 educ2 sex using `newdeaths'
replace deaths = deaths + new_deaths
rename educ2 educ
drop if mi(educ)
drop new_deaths _merge
tempfile dataset
save `dataset', replace

* do the same process for all of the disparate causes of death
foreach cause in circulatory poison cirrhosis suicide diabetes cerebrovasc lowerresp cancer flupneum {

* note that the code in this section is the same as the above, except as noted in comments
use ../mid/priorstateimpute, clear
* states without education recorded
keep if year >=1992
drop if inlist(fipstate, 13, 40, 44, 46) 
keep if `cause'==1

gen count = 1
gen educ2count = 1 if educ2 == 2
gen byte age4 = . 
replace age4 = 1 if inrange(age, 25, 34)
replace age4 = 2 if inrange(age, 35, 44)
replace age4 = 3 if inrange(age, 45, 54)
replace age4 = 4 if inrange(age, 55, 64)
drop if mi(age4)
drop age
gen race2 = 0
replace race2 = 1 if race>=1
drop race

* note that we are looking at agexracexsex cells, NOT with state
* this is because some causes of death have too few deaths to break down this granularly
gcollapse (sum) educmiss educ2count count, by(year age4 race2 sex)
gen how_much_missing  = educmiss/count
gen pctcolldeath = (educ2count)/(count - educmiss)
*replace pctcolldeath = . if mi(how_much_missing) | how_much_missing > 0.15
keep year age4 race2 sex how_much_missing pctcolldeath educmiss educ2count count
disp "`cause'"
gen pctcolldeath_ref = pctcolldeath

* check how much is being imputed, can see that the worst cell is half
summ how_much_missing
replace pctcolldeath = 0 if how_much_missing == 1
assert !mi(pctcolldeath)

keep year age4 race2 sex pctcolldeath pctcolldeath_ref

drop pctcolldeath_ref
ren pctcolldeath pctcolldeath_racesex
tempfile cellsracesex
save `cellsracesex', replace

use ../mid/priorstateimpute, clear
* states without education recorded
keep if year >=1992
drop if inlist(fipstate, 13, 40, 44, 46) 
keep if `cause'==1
gen byte age4 = . 
replace age4 = 1 if inrange(age, 25, 34)
replace age4 = 2 if inrange(age, 35, 44)
replace age4 = 3 if inrange(age, 45, 54)
replace age4 = 4 if inrange(age, 55, 64)
drop if mi(age4)
drop age
gen race2 = 0
replace race2 = 1 if race>=1
drop race
gcollapse (sum) educmiss, by(year fipstate age4 race2 sex)
fmerge m:1 year age4 race2 sex using `cellsracesex'
expand 2
bys year fipstate age4 race2 sex: gen educ2 = _n
gen new_deaths = .
assert !mi(educmiss)
replace new_deaths = pctcolldeath * educmiss if educ2 == 2
replace new_deaths = (1-pctcolldeath) * educmiss if educ2 == 1
assert new_deaths >=0
keep year fipstate age4 race2 sex educ2 new_deaths
tempfile newdeaths
save `newdeaths', replace

use ../mid/priorstateimpute, clear
* states without education recorded
keep if year >=1992
drop if inlist(fipstate, 13, 40, 44, 46) 
gen byte age4 = . 
replace age4 = 1 if inrange(age, 25, 34)
replace age4 = 2 if inrange(age, 35, 44)
replace age4 = 3 if inrange(age, 45, 54)
replace age4 = 4 if inrange(age, 55, 64)
drop if mi(age4)
drop age
gen race2 = 0
replace race2 = 1 if race>=1
drop race
gcollapse (sum) `cause', by(year fipstate age4 educ2 race2 sex)

fmerge 1:1 fipstate year age4 race2 educ2 sex using `newdeaths'
replace `cause' = `cause' + new_deaths
rename educ2 educ
drop if mi(educ)
drop new_deaths _merge
tempfile dataset_`cause'
save `dataset_`cause'', replace

}

* merge all the imputed datasets together
use `dataset', clear
foreach cause in circulatory poison cirrhosis suicide diabetes cerebrovasc lowerresp cancer flupneum {
fmerge 1:1 fipstate year age4 race2 educ sex using `dataset_`cause'', nogenerate
}

save ../mid/imputeeducation, replace
