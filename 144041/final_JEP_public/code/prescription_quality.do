clear all
pause on
set rmsg on 
set scheme s1color

* from the Darmouth Atlas of Medicare Prescription Drug Use: https://www.rwjf.org/en/library/research/2013/10/the-dartmouth-atlas-of-medicare-prescription-drug-use.html
import excel using ../raw/DAP_prescription_drug_use_state_2010.xls, clear firstrow
keep StateName Percentofpatientsfillingbeta Percentofpatientsfillingstat Percentofdiabeticpatientsage Q
statastates, name(StateName)
drop if _merge == 1
gen good_prescription = (Percentofdiabeticpatientsage + Percentofpatientsfillingstat)/2
rename Q risky_prescription
rename state_fips fips
keep fips good_prescription risky_prescription
drop if inlist(fips, 2, 11, 15) | fips > 56
save ../mid/prescription_quality, replace
