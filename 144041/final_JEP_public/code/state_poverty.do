clear all
pause on
set rmsg on 
set scheme s1color

import excel using ../raw/hstpov21, cellrange(A5) firstrow clear
drop if mi(Total)
drop if STATE == "STATE"
gen year = 2019 - floor((_n-1)/51)
statastates, name(STATE)
drop if inlist(state_fips, 2, 11, 15) | state_fips > 56
drop if year < 1990 | year > 2017
rename (state_fips Percent) (fipstate shrpov)
keep fips shrpov year
destring shrpov, replace
label var shrpov "share in poverty"
save ../mid/state_poverty, replace
