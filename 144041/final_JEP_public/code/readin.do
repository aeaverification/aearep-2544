#delimit ;
clear all;

local prg readin_WONDER;
cap log close;
pause on;
log using ../logs/`prg'.log, replace;

* all of this data is downloaded directly from https://wonder.cdc.gov/mortSQL.html we download manually, downloading the relevant age groups specified below
* split out by state and year, downloading both crude and age-adjusted mortalities; 

**************************;
* 1968-1978;

import delimited using "../raw/WONDER/origdata/child (0-4)/Compressed Mortality, 1968-1978.txt", clear rowrange(1:614);
gen age_group = 1;
save ../mid/child1968_1978, replace;

import delimited using "../raw/WONDER/origdata/young person (5-24)/Compressed Mortality, 1968-1978.txt", clear rowrange(1:614);
gen age_group = 2;
save ../mid/youngperson1968_1978, replace;

import delimited using "../raw/WONDER/origdata/midlife (25-64)/Compressed Mortality, 1968-1978.txt", clear rowrange(1:614);
gen age_group = 3;
save ../mid/midlife1968_1978, replace;

import delimited using "../raw/WONDER/origdata/older (65+)/Compressed Mortality, 1968-1978.txt", clear rowrange(1:614);
gen age_group = 4;
save ../mid/older1968_1978, replace;

import delimited using "../raw/WONDER/origdata/allages/Compressed Mortality, 1968-1978.txt", clear rowrange(1:614);
gen age_group = 5;
save ../mid/allages1968_1978, replace;

**************************;
* 1979-1998;

import delimited using "../raw/WONDER/origdata/child (0-4)/Compressed Mortality, 1979-1998.txt", clear rowrange(1:1073);
gen age_group = 1; 
save ../mid/child1979_1998, replace;

import delimited using "../raw/WONDER/origdata/young person (5-24)/Compressed Mortality, 1979-1998.txt", clear rowrange(1:1073);
gen age_group = 2; 
save ../mid/youngperson1979_1998, replace;

import delimited using "../raw/WONDER/origdata/midlife (25-64)/Compressed Mortality, 1979-1998.txt", clear rowrange(1:1073);
gen age_group = 3; 
save ../mid/midlife1979_1998, replace;

import delimited using "../raw/WONDER/origdata/older (65+)/Compressed Mortality, 1979-1998.txt", clear rowrange(1:1073);
gen age_group = 4; 
save ../mid/older1979_1998, replace;

import delimited using "../raw/WONDER/origdata/allages/Compressed Mortality, 1979-1998.txt", clear rowrange(1:1073);
gen age_group = 5; 
save ../mid/allages1979_1998, replace;

**************************;
* 1999-2019;

import delimited using "../raw/WONDER/origdata/child (0-4)/Underlying Cause of Death, 1999-2019.txt", clear rowrange(1:1123);
gen age_group = 1;
save ../mid/child1999_2019, replace;

import delimited using "../raw/WONDER/origdata/young person (5-24)/Underlying Cause of Death, 1999-2019.txt", clear rowrange(1:1123);
gen age_group = 2;
save ../mid/youngperson1999_2019, replace;

import delimited using "../raw/WONDER/origdata/midlife (25-64)/Underlying Cause of Death, 1999-2019.txt", clear rowrange(1:1123);
gen age_group = 3;
save ../mid/midlife1999_2019, replace;

import delimited using "../raw/WONDER/origdata/older (65+)/Underlying Cause of Death, 1999-2019.txt", clear rowrange(1:1123);
gen age_group = 4;
save ../mid/older1999_2019, replace;

import delimited using "../raw/WONDER/origdata/allages/Underlying Cause of Death, 1999-2019.txt", clear rowrange(1:1123);
gen age_group = 5;
save ../mid/allages1999_2019, replace;

*******************************************;
clear all;

foreach a in child youngperson midlife older allages {;
  foreach y in 1968_1978 1979_1998 1999_2019 {;
  disp "`a'";
  disp "`y'";
  append using ../mid/`a'`y'.dta;
	     erase ../mid/`a'`y'.dta;
  };
};

assert yearcode == year;
drop yearcode;
assert notes == "Total" if year == .;
drop if year == . & notes == "Total";
assert notes == "";
drop notes;

* Rename state code to FIPS code, state to STATENAME and add two-letter postal abbreviations as STATE;
ren statecode fips;
ren state statename;
fips2state;

sort age_group fips year;
order age_group fips year state statename;

label define agegrp_lbl 
  1 "Child (0-4)"
  2 "Young Person (5-24)"
  3 "Midlife (25-64)"
  4 "Older (65+)"
  5 "All Ages";
label val age_group agegrp_lbl;

levelsof fips, local(fipslist);

* This command adds hyphen to adj-adjusted rate label;
la var ageadjustedrate "Age-Adjusted Rate";
save ../mid/`prg', replace;

log close;
exit, clear;

