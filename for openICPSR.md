- [REQUIRED] Please provide debugged code, addressing the issues identified in this report.

- [REQUIRED] For some data: Provide a copy of the data *you* downloaded as part of the ICPSR archive.

- [STRONGLY SUGGESTED] Code your programs to only download when the local file is not present. 

- [REQUIRED] For some data: Please reference the permanent deposit of these data.

- [REQUIRED] openICPSR should not have ZIP files visible. ZIP files should be uploaded to openICPSR via "Import from ZIP" instead of "Upload Files". Please delete the ZIP files, and re-upload using the "Import from ZIP" function.

- [REQUIRED] Please review the title of the openICPSR deposit as per our guidelines (below).

> Detailed guidance is at [https://aeadataeditor.github.io/aea-de-guidance/](https://aeadataeditor.github.io/aea-de-guidance/).

- [REQUIRED] Please amend README to contain complete requirements.

> The openICPSR submission process has changed. If you have not already done so, please "Change Status -> Submit to AEA" from your deposit Workspace.
> [NOTE] Since July 1, 2021, we will publish replication packages as soon as all requested changes to the deposit have been made. Please process any requested changes as soon as possible.



Details in the full report, which you will receive via ScholarOne shortly. Please provide your response to the items listed above via the openICPSR Project Communication log, specifying AEAREP-2544. Other items in the report may need to be addressed via ScholarOne.
