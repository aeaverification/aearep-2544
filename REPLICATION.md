# JEP-2020-1202 Rising Geographic Disparities in US Mortality Validation and Replication results

## SUMMARY

Thank you for your replication archive. Starting from the analysis file provided, all figures and the table were successfully reproduced with the exception of Figure 4. Some intermediate data sets were not reproduced. While we appreciate a script that non-interactively downloads all data inputs, we also need to consider the ability to reproduce the paper in its current state. Given the absence of curation of the downloaded data files, we ask that you provide downloaded copies (where feasible) or point to (in a few other instances) curated copies. Also,  data citations are missing, code has a few bugs, and the replication package needs to be reorganized (no ZIP files). In assessing compliance with our [Data and Code Availability Policy](https://www.aeaweb.org/journals/policies/data-code), we have identified the following issues, which we ask you to address.

**Conditional on making the requested changes to the manuscript and the openICPSR deposit prior to publication, the replication package is accepted.**

### Action Items (Manuscript)

- [REQUIRED] Please add data citations to the article. Guidance on how to cite data is provided in the [AEA Sample References](https://www.aeaweb.org/journals/policies/sample-references) and in [additional guidance](https://social-science-data-editors.github.io/guidance/addtl-data-citation-guidance.html).

### Action Items (OpenICPSR)

- [REQUIRED] Please provide debugged code, addressing the issues identified in this report.

- [REQUIRED] For some data: Provide a copy of the data *you* downloaded as part of the ICPSR archive.

- [STRONGLY SUGGESTED] Code your programs to only download when the local file is not present. 

- [REQUIRED] For some data: Please reference the permanent deposit of these data.

- [REQUIRED] openICPSR should not have ZIP files visible. ZIP files should be uploaded to openICPSR via "Import from ZIP" instead of "Upload Files". Please delete the ZIP files, and re-upload using the "Import from ZIP" function.

- [REQUIRED] Please review the title of the openICPSR deposit as per our guidelines (below).

> Detailed guidance is at [https://aeadataeditor.github.io/aea-de-guidance/](https://aeadataeditor.github.io/aea-de-guidance/).

- [REQUIRED] Please amend README to contain complete requirements.

> The openICPSR submission process has changed. If you have not already done so, please "Change Status -> Submit to AEA" from your deposit Workspace.
> [NOTE] Since July 1, 2021, we will publish replication packages as soon as all requested changes to the deposit have been made. Please process any requested changes as soon as possible.

## General

> [SUGGESTED] A recommended README template for replication packages in economics can be found on the [Social Science Data Editor Github site](https://social-science-data-editors.github.io/guidance/template-README.html).

## Data description

### Data Sources

Several datasets are downloaded in the file `download_raw.do`. While we appreciate and encourage such automatic downloads, they do not allow for robust reproduction of the current state of the project. For all files mentioned therein (and not explicitly mentioned below), please do the following:

> [REQUIRED] Please add data citations to the article. Guidance on how to cite data is provided in the [AEA Sample References](https://www.aeaweb.org/journals/policies/sample-references) and in [additional guidance](https://social-science-data-editors.github.io/guidance/addtl-data-citation-guidance.html).

That will usually contain the URL, satisfying the description of data provenance.

> [REQUIRED] Provide a copy of the data *you* downloaded as part of the ICPSR archive.

This ensures that future updates by the data provider do not impede reproducibility. 

> [STRONGLY SUGGESTED] Code your programs to only download when the local file is not present. 

Example:

```
capture confirm  file gnxl.txt
if _rc==0 {
    display "file gnxl.txt does not exist, downloading"
    /* download code here */
    }
```

#### National Vital Statistics System Mortality data

- Dataset is not provided. Link is provided.
- Access conditions are described in the online appendix. Access to the data is restricted to protect confidentiality. To access the data researches need to fill out an application.

- The data are not cited in the README nor the article.

> [REQUIRED] Please add data citations to the article. Guidance on how to cite data is provided in the [AEA Sample References](https://www.aeaweb.org/journals/policies/sample-references) and in [additional guidance](https://social-science-data-editors.github.io/guidance/addtl-data-citation-guidance.html).

#### United States Mortality DataBase

- Data are provided. Link is provided.
- The data are publicly available, but downloading requires users to register.
- Authors got the data through HAVER.
- The data are not cited in the README nor the article.

> [REQUIRED] Please add data citations to the article. Guidance on how to cite data is provided in the [AEA Sample References](https://www.aeaweb.org/journals/policies/sample-references) and in [additional guidance](https://social-science-data-editors.github.io/guidance/addtl-data-citation-guidance.html).

#### CDC Wonder Underlying Cause of Death Compressed Mortality Data

- Data are provided.
- Data are publicly available and may be distributed freely. Link is provided.
- The dataset is not cited in the README nor the article.

> [REQUIRED] Please add data citations to the article. Guidance on how to cite data is provided in the [AEA Sample References](https://www.aeaweb.org/journals/policies/sample-references) and in [additional guidance](https://social-science-data-editors.github.io/guidance/addtl-data-citation-guidance.html).

#### Income by state BEA

- Dataset is provided.
- data are publicly available. Link is provided.
- The dataset is not cited in the README nor the article.

> [REQUIRED] Please add data citations to the article. Guidance on how to cite data is provided in the [AEA Sample References](https://www.aeaweb.org/journals/policies/sample-references) and in [additional guidance](https://social-science-data-editors.github.io/guidance/addtl-data-citation-guidance.html).

#### SEER Population data

- data are not provided. Link is provided
- Data is downloaded by the do file `download_raw.do`
- The dataset is not cited in the README nor the article.

> [REQUIRED] Please add data citations to the article. Guidance on how to cite data is provided in the [AEA Sample References](https://www.aeaweb.org/journals/policies/sample-references) and in [additional guidance](https://social-science-data-editors.github.io/guidance/addtl-data-citation-guidance.html).

You should cite the location of the data that you use to download these data.

> [REQUIRED] Please reference the permanent deposit of these data.

The AEA has undertaken efforts to preserve these frequently used data, which unfortunately are not permanently preserved by the CDC (yet). Please amend README to cite the following curated copy of the data:

> NIH National Cancer Institute, and Vilhuber, Lars. NIH SEER U.S. Population Data - 1969-2019. Nashville, TN: American Economic Association [publisher], 2021. Ann Arbor, MI: Inter-university Consortium for Political and Social Research [distributor], 2021-08-13. https://doi.org/10.3886/E146341V1

#### Current Population Survey Microdata IPUMS

- Extract is provided.
- Data are publicly available. Link is provided.
- The dataset is not cited in the README nor the article.

> [REQUIRED] Please add data citations to the article. Guidance on how to cite data is provided in the [AEA Sample References](https://www.aeaweb.org/journals/policies/sample-references) and in [additional guidance](https://social-science-data-editors.github.io/guidance/addtl-data-citation-guidance.html).

#### Resident population by state Us census bureau

- Data are provided.
- Data are publicly available. Link is provided.
- The dataset is not cited in the README nor the article.

> [REQUIRED] Please add data citations to the article. Guidance on how to cite data is provided in the [AEA Sample References](https://www.aeaweb.org/journals/policies/sample-references) and in [additional guidance](https://social-science-data-editors.github.io/guidance/addtl-data-citation-guidance.html).

#### Behavioral Risk Factor Surveillance System (BRFSS)

- Data are not provided. Link is provided.
- data are publicly available.
- Data is downloaded by do file `donwload_raw.do`
- The data is cited in the manuscript

`Centers for Disease Control and Prevention (CDC). 2021. “Behavioral Risk Factor Surveillance System (BRFSS).” US Department of Health and Human Services, Centers for Disease Control and Prevention. https://www.cdc.gov/brfss/annual_data/annual_data.htm. (accessed January 1, 2021).`

> [REQUIRED] Please reference the permanent deposit of these data.

The AEA has undertaken efforts to preserve these frequently used data, which unfortunately are not permanently preserved by the CDC (yet). Please amend README to cite the following curated copy of the data:

> Centers for Disease Control and Prevention, National Center for Chronic Disease Prevention and Health Promotion, Division of Population Health. Behavioral Risk Factor Surveillance System (BRFSS) Annual Survey Data, 1999-2019. Nashville, TN: American Economic Association [publisher], 2021. Ann Arbor, MI: Inter-university Consortium for Political and Social Research [distributor], 2021-09-03. https://doi.org/10.3886/E146342V2

#### Census and ACS Educational Attainment tables

- Data are provided
- data are publicly available. Link is provided.
- The data are not cited in the manuscript or README

> [REQUIRED] Please add data citations to the article. Guidance on how to cite data is provided in the [AEA Sample References](https://www.aeaweb.org/journals/policies/sample-references) and in [additional guidance](https://social-science-data-editors.github.io/guidance/addtl-data-citation-guidance.html).

#### Poverty Tables

- Data are provided.
- data are publicly available. Link is provided.
- The data are not cited in the manuscript or README

> [REQUIRED] Please add data citations to the article. Guidance on how to cite data is provided in the [AEA Sample References](https://www.aeaweb.org/journals/policies/sample-references) and in [additional guidance](https://social-science-data-editors.github.io/guidance/addtl-data-citation-guidance.html).

#### Manufacturing share

- Data are provided.
- data are publicly available. Link is provided.
- The data are not cited in the manuscript or README

> [REQUIRED] Please add data citations to the article. Guidance on how to cite data is provided in the [AEA Sample References](https://www.aeaweb.org/journals/policies/sample-references) and in [additional guidance](https://social-science-data-editors.github.io/guidance/addtl-data-citation-guidance.html).

#### Prescription quality Dartmouth Atlas of Medicare

- Data are provided.
- data are publicly available.
- The data are not cited in the manuscript or README

> [REQUIRED] Please add data citations to the article. Guidance on how to cite data is provided in the [AEA Sample References](https://www.aeaweb.org/journals/policies/sample-references) and in [additional guidance](https://social-science-data-editors.github.io/guidance/addtl-data-citation-guidance.html).

### Analysis Data Files

- [ ] No analysis data file mentioned
- [ ] Analysis data files mentioned, not provided (explain reasons below)
- [x] Analysis data files mentioned, provided. File names listed below.

`final_dataset_redacted.dta`

## Data deposit

### Requirements

- [x] README is in TXT, MD, PDF format
- [ ] openICPSR deposit has no ZIP files
- [ ] Title conforms to guidance (starts with "Data and Code for:" or "Code for:", is properly capitalized)
- [x] Authors (with affiliations) are listed in the same order as on the paper

> [REQUIRED] openICPSR should not have ZIP files visible. ZIP files should be uploaded to openICPSR via "Import from ZIP" instead of "Upload Files". Please delete the ZIP files, and re-upload using the "Import from ZIP" function.

> [REQUIRED] Please review the title of the openICPSR deposit as per our guidelines (below).

> Detailed guidance is at [https://aeadataeditor.github.io/aea-de-guidance/](https://aeadataeditor.github.io/aea-de-guidance/).

### Deposit Metadata

- [x] JEL Classification (required)
- [x] Manuscript Number (required)
- [x] Subject Terms (highly recommended)
- [ ] Geographic coverage (highly recommended)
- [x] Time period(s) (highly recommended)
- [ ] Collection date(s) (suggested)
- [x] Universe (suggested)
- [x] Data Type(s) (suggested)
- [ ] Data Source (suggested)
- [ ] Units of Observation (suggested)

- [NOTE] openICPSR metadata is sufficient.

For additional guidance, see [https://aeadataeditor.github.io/aea-de-guidance/data-deposit-aea-guidance.html](https://aeadataeditor.github.io/aea-de-guidance/data-deposit-aea-guidance.html).

## Data checks

Data sets are provided in .dta format or .txt. Except for Ipums extract that is in .dat All files can be read.
Analysis data `final_dataset_redacted.dta` can be read and has variable labels.

## Code description

There are 23 do files, including a "master.do" and 2 python scripts.

## Stated Requirements

- [ ] No requirements specified
- [x] Software Requirements specified as follows:
  - Stata
  - Python
- [ ] Computational Requirements specified as follows:
  - Cluster size, etc.
- [ ] Time Requirements specified as follows:
  - Length of necessary computation (hours, weeks, etc.)

- [ ] Requirements are complete.

## Missing Requirements

- [x] Software Requirements
  - [x] Stata
    - [x] Packages: `gtools`, `ftools`, `estout`. `grc1leg`
  - [x] Python
    - [x] Version
    - [x] Packages: `xlrd, openpyxl, gc`
  - [x] Linux/Bash shell
    - [x] Parts of the code rely on `shell` command running `gunzip`
- [x] Computational Requirements specified as follows:
  - Cluster size, disk size, memory size, etc.
- [x] Time Requirements
  - Length of necessary computation (hours, weeks, etc.)

> [REQUIRED] Please amend README to contain complete requirements.

## Computing Environment of the Replicator

- CISER Shared Windows Server 2019, 256GB, Intel Xeon E5-4669 v3 @ 2.10Ghz (2 processors, 36 cores)
- Stata/MP 17

## Replication steps

1. Downloaded code from URL provided and data from OpenICPSR
2. Run master.do
3. Run into error on do file `download_raw.do`

 ```
 `file not found
    Trailing char < > at index 12: LLCP2014.XPT 
 ```

4. Unzipped table LLCP2014.XPT manually, continue to run code.
5. Code from lines 52 to 56 of `download_raw.do` requires gzip. Didn't run lines 53 `shell gunzip us.1990_2019.19ages.adjusted.txt.gz`
and 55 `shell gunzip us.1990_2019.singleages.adjusted.txt.gz`. Instead unzipped files using git bash in the folder ./raw/brfss
6. run into an error: `file ../raw/us.1990_2019.singleages.adjusted.txt not found`. Changed line 11 of `seer.do` for `byte sex 16 byte age 17-18 pop 19-26 using ../raw/brfss/us.1990_2019.singleages.adjusted.txt, clear`. Continue to run code.
7. run into error `r(601)` when running `pullbrfss.do`. It seems that a file is not found. By inspecting the code, it seems that `download_raw.do` doesn't get all the brfss
8. Run into error when packages `gtools` and `ftools` were not found. Installed packages and resumed running code.
9. While running `state_covariates.do` encounter error: `file ../mid/brfss_state.dta not found`
10. Run into error command `grc1leg` is unrecognized. Installed `grc1leg` and continued running the code.

```
net install grc1leg, from ("http://www.stata.com/users/vwiggins")
```

11. While running the code for figure 4, run into error `r(199)`. Run code for the rest of the figures.
12. Run into error `command esttab is unrecognized`. Installed package `estout` and finish running code successfully.
13. Run python script `comparability_ratio_process.py` run into error:

```
File "<stdin>", line 1, in <module>
IndentationError: unexpected indent
>>> ModuleNotFoundError: No module named 'xlrd'
```
14. Re-ran python code with Docker image (anaconda 4.10, python 3.8.8), wqrked.

```
docker pull continuumio/anaconda3:latest
docker run -it --rm -v $(pwd):/project -w /project/code continuumio/anaconda3 python comparability_ratio_process.py
```

> [REQUIRED] Please provide debugged code, addressing the issues identified in this report.

> [REQUIRED] Please specify all requirements.

## Findings

From the analysis file all figures and the table were successfully reproduced with the exception of table 4. Some intermediate data sets were not reproduced, either because the required confidential data or some error in the code.

| Data Preparation Program       | Dataset created                                                                                                                                                       |  | Reproduced? |
|--------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------|--|-------------|
| geo.do                         | mid/fipstate.dta, mid/counties2019.dta, mid/confipstohrr.dta                                                                                                          |  | yes         |
| weight.do:                     | mid/agestd_weights_nvsr.dta, mid/agestd_weights_nvsr_educ.dta                                                                                                         |  | yes         |
| seer.do:                       | ../mid/seerconfips_singleage.dta, mid/seerconfips.dta, ../mid/seer_cohort.dta                                                                                         |  | yes         |
| pullcps.do                     | mid/statecollegeshare.dta, mid/educstate.dta, mid/educstaterace.dta                                                                                                   |  | yes         |
| pullbrfss.do                   | ../figures/brfss_indicators.pdf, ../mid/brfss_state.dta                                                                                                               |  | no          |
| readin.do                      | mid/readin_WONDER.dta, mid/readin_WONDER_causes.dta,                                                                                                                  |  | yes         |
| comparability_ratio_process.py | aw/comparability_ratios_top_causes_age.xlsx                                                                                                                           |  | yes         |
| bea_income.do                 | mid/bea_income.dta                                                                                                                                                    |  | yes         |
| le_berkeley.do                | mid/le_berkeley.dta                                                                                                                                                   |  | no          |
| state_collegeshare.do          | mid/statecollegesharenotinterpolated.dta,                                                                                                                             |  | yes         |
| state_manufacturingshare.do   | mid/statemanufacturingshare1969-2018.dta                                                                                                                              |  | yes         |
| state_poverty.do               | mid/state_poverty.dta                                                                                                                                                 |  | yes         |
| prescription_quality.do       | mid/prescription_quality.dta                                                                                                                                          |  | yes         |
| state_covariates.do            | mid/state_covariates.dta                                                                                                                                              |  | no          |
| pull.do                       | mid/micromaster.dta, mid/micromaster.csv                                                                                                                              |  | no          |
| convertidentifycauses.do:      | mid/icd9toicd10.dta, mid/icd10toicd9.dta, mid/icd9codes.dta, mid/icd10codes.dta, mid/icd9causes.dta, mid/icd10causes.dta, mid/icd9chapters.dta, mid/icd10chapters.dta |  | no          |
| microcausepd.py                | mid/microcausepre.dta                                                                                                                                                 |  | no          |
| microcause.do                  | mid/microcause.dta                                                                                                                                                    |  | no          |
| simple_education_imputation.do | mid/priorstateimpute.dta, mid/imputeeducation.dta                                                                                                                     |  | no          |
| state_dataset_out.do           | final_dataset_redacted.dta                                                                                                                                            |  | no          |

### Figures/Tables

| Figure/Table # | Program                 | Line Number | Reproduced? |
|----------------|-------------------------|-------------|-------------|
| Figure 1       | all_paper_figures.do    | 60          | yes         |
| Figure 2       | all_paper_figures.do    | 82          | yes         |
| Figure 3       | all_paper_figures.do    | 133         | yes         |
| Figure 4       | all_paper_figures.do    | 233         | no          |
| Figure 5       | all_paper_figures.do    | 391         | yes         |
| Figure 6       | all_paper_figures.do    | 442         | yes         |
| Figure 7       | all_paper_figures.do    | 462         | yes         |
| Figure A.1     | all_appendix_figures.do | 103         | yes         |
| Figure A.2     | all_appendix_figures.do | 129         | yes         |
| Figure A.3     | all_appendix_figures.do | 166         | yes         |
| Figure A.4     | all_appendix_figures.do | 415         | yes         |
| Figure A.5     | all_appendix_figures.do | 8-Jun       | yes         |
| Figure A.6     | all_appendix_figures.do | 866         | yes         |
| table A.1      | all_appendix_figures.do | 1060        | yes         |

## Classification

- [ ] full reproduction
- [ ] full reproduction with minor issues
- [x] partial reproduction (see above)
- [ ] not able to reproduce most or all of the results (reasons see above)

### Reason for incomplete reproducibility

- [ ] `Discrepancy in output` (either figures or numbers in tables or text differ)
- [x] `Bugs in code`  that  were fixable by the replicator (but should be fixed in the final deposit)
- [ ] `Code missing`, in particular if it  prevented the replicator from completing the reproducibility check
  - [ ] `Data preparation code missing` should be checked if the code missing seems to be data preparation code
- [ ] `Code not functional` is more severe than a simple bug: it  prevented the replicator from completing the reproducibility check
- [ ] `Software not available to replicator`  may happen for a variety of reasons, but in particular (a) when the software is commercial, and the replicator does not have access to a licensed copy, or (b) the software is open-source, but a specific version required to conduct the reproducibility check is not available.
- [ ] `Insufficient time available to replicator` is applicable when (a) running the code would take weeks or more (b) running the code might take less time if sufficient compute resources were to be brought to bear, but no such resources can be accessed in a timely fashion (c) the replication package is very complex, and following all (manual and scripted) steps would take too long.
- [ ] `Data missing` is marked when data *should* be available, but was erroneously not provided, or is not accessible via the procedures described in the replication package
- [x] `Data not available` is marked when data requires additional access steps, for instance purchase or application procedure.
